\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\usepackage{graphicx}
\usepackage{../present}
\usepackage{../stats}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=red} 

% \usepackage[english]{babel}
% or whatever

% \usepackage[latin1]{inputenc}
% or whatever


\title[Sampling] % (optional, use only with long paper titles)
{Sampling}
\subtitle{}
\author{J Hef{}feron}
\institute{
  Mathematics\\
  Saint Michael's College\\
  Colchester, VT\\[1ex]
  \texttt{jhefferon@smcvt.edu.edu}
}
\date{}

\subject{Sampling}
% This is only inserted into the PDF information catalog. Can be left
% out. 


%\setlength{\parskip}{1ex}



\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................
\begin{frame}{Population vs sample}
The \alert{population} is the group that we want information about.
With a census we observe each member of the population.
With a sample we observe a small subset, 
and use that to infer about the population as a whole.

We have seen that usually we must not attempt a census but instead must
take a sample.


\exm
We make hundreds of widgets each day.
For quality testing, every day we take off fifteen at random and test them.
Our goal is to use the fifteen to get information about the hundreds. 
  
\exm
We have $35$ patients with a certain condition and we will give them either 
the standard operation, or a newly-developed one.
Our goal is to get information about what would happen if 
we gave this treatment 
to every person in the world with this condition.

\pause
\smallskip
\fbox{\parbox{0.9\textwidth}{\color{red}
  We do not care about the sample.
  We care about the population.
  We only use the sample to infer things about the
  population.}}
\end{frame}





\begin{frame}
\frametitle{Samples vary}

Samples have a quality that we must engineer around:~samples vary.

Flip a coin a hundred times.
Sometimes you report $\hat{p}=0.53$, sometimes
$\hat{p}=0.46$,
and sometimes you may even report $\hat{p}=0.50$.

We have earlier discussed design of experiments and  
minimizing bias.
That's not what we are talking about here.
These people didn't flip the coin wrong.
Instead, there
is routine sample-to-sample variation, and we are worried that
what we are interpreting as an outcome is only a fluctuation.

\pause
\exm
You ask a random sample of 
fifteen SMC students to rate the president on a $1$--$10$ scale,
and report the average,~$\bar{x}$.
Different samples return somewhat different $\bar{x}$'s.

\exm
You want to know if missing breakfast affects elementary school boys more
than girls.
You select students at random for the treatment and control groups for each
sex, and then you measure their scores on a math test.
Different selections of who is in the treatment groups would result in 
somewhat different scores on the test. 
\end{frame}





\begin{frame}{Why it matters}

Suppose that you are testing a cancer drug. 
After doing an experiment
with a sample of patients and running the numbers, you
conclude the drug doubles the chance that people live to five years.
Great!
What keeps you awake at night?

\pause
What worries you is that
you may, by sheer happenstance, have gotten a way-off sample. 
Not because you did anything wrong. 
Instead,
you just happened to get a sample that came out on the long-lived side.
\end{frame}




\begin{frame}{Way-off-ness happens}
When you do a study, at the end you are holding a  
sample number, maybe an~$\bar{x}$ or a~$\hat{p}$. 
You think that probably it is close to the true number.
But what worries you is the possiblity that you may have gotten,
by happenstance, a way-off sample.

When way-off samples appear there are two factors possibly at work.
\begin{asparadesc}
\pause\item[Bias]
There could be some systematic problem with the way that
you ran your study. 

\pause\item[Luck]
If thousands of people flip a coin $100$ times, 
then some of them will get $58$ heads, just by
happenstance.
If you are standing there holding a sample statistic, a $\hat{p}$ or $\bar{x}$,
is there a realistic chance that you are one of these people?
\end{asparadesc}

\pause\medskip
When we hear of a study that proved to come to the wrong conclusion, 
there is a natural human tendency to
go back to the study and examine it for some bias.
But the second possibility is absolutely a worry: things with only 
a one percent chance of happening
will in fact happen, one percent of the time.  
\end{frame}








\section{Sampling distributions}

\begin{frame}{The distribution of samples}
To know the chance of a way-off sample we need to know how samples are
distributed.

\medskip
We have a paper bag with fifteen slips of paper, 
labeled
  5,5,5,5,5,\,4,4,4,4,\,3,3,3,\,2,2,\,1.
This is the population.
We will take samples from it.

\begin{center}
  \includegraphics[height=0.2\textheight]{paper-bag.jpg}
\end{center}

Everyone mixes up the papers as well as possible.
They draw a sample of five papers, 
and report the average $\bar{x}$ of their sample.
\pause
We make a graph showing how the samples are distributed.
This illustrates the \alert{sampling distribution}.
\end{frame}

\begin{frame}{Automate the process}
\textit{StatKey} can simulate this.
Under \textit{Sampling Distributions}, select 
\textit{Mean}.  
Then edit the data to list the population of the bag.
\begin{center}
  \includegraphics[height=0.40\textheight]{paper_bag_edit.png}
\end{center}
Set the sample size to $n=5$.
\end{frame}
\begin{frame}
Before we take any samples, note that
for this population average, the census of the entire set of numbers,
is $\mu=3.67$.
\begin{center}
  \includegraphics[height=0.65\textheight]{paper_bag_ready.png}
\end{center}
\end{frame}
\begin{frame}
A person takes a single sample by
pulling out~$5$ slips of paper and reporting their
sample average,~$\bar{x}$.
They do this to estimate the population average,~$\mu$.

Here the computer generated ten such $\bar{x}$'s,
simulating ten people each pulling a sample.
\begin{center}
  \includegraphics[height=0.55\textheight]{paper_bag_ten.png}
\end{center}
In the big graph on the left we see that
some of people got $\bar{x}$'s that are close to the 
population number~$\mu=3.667$, while some $\bar{x}$'s are further off.
\end{frame}

\begin{frame}
Here the computer generated a hundred sample~$\bar{x}$'s.
\centergraphic[height=.55\textheight]{sampling_dist.png}
It starts to show a pattern and to 
give us a better idea of the chance of a way-off sample. 

\pause\medskip
A \alert{sampling distribution} is the distribution of a
  sample statistic, here the distribution of the $\bar{x}$'s.
\pause
The \alert{standard error} is the standard deviation of that 
distribution. 
Here it is the standard deviation
of the sample $\bar{x}$'s.
\end{frame}


\begin{frame}{Randomness}
In these simulations, the computer is using randomness to pick the samples.
So when you follow along and try it on your own, your pictures will look
slightly different than mine.
But they will be close.

In particular, despite the minor differences, what you generate and
what I generated both help a person understand what a sampling
distribution is about.
\end{frame}


\begin{frame}{Population distribution and sampling distribution}
Here the computer generated a thousand.
\centergraphic[height=.5\textheight]{paper_bag_thousand.png}
There are two distributions pictured.
The distribution of the population\Dash the fifteen pieces of paper in the 
paper bag\Dash is the right upper plot.
The distribution of size-five samples, of the $\bar{x}$'s, is on the left. 

Notice that their means are essentially the same, $3.667$
and $3.662$.
\end{frame}

\begin{frame}{What's the chance of a way-off sample?}
With a thousand dots we have a pretty good idea of the sampling distribution.
What's the chance of any one person reporting a sample~$\bar{x}$ that
is $2.5$ or less?
Click on \textit{Left Tail}, then click on the number that appears
on the bottom and change it to $2.5$.
\centergraphic[height=.5\textheight]{paper_bag_way_off.png}
So: the chance of a sample~$\bar{x}$ being this way-off from the true 
population mean~$\mu$ is about $0.013$, 
about $1.3$~percent.
\end{frame}



\begin{frame}{Sample proportions also vary}
In grade school they tell you that 
if you flip a coin a hundred times then you will get fifty heads.
In middle school they tell you that will get some number close to fifty.
Finally, in high school you learn the whole story:~you are likely 
to get a number close to fifty heads.  
but there is still some chance of getting a sample that is not close to fifty.
You could, in theory, get $85$ heads.

If you flip a coin then 
your proportion of heads~$\hat{p}$ 
is your single-number sample estimate of this coin's true proportion,
of its population parameter~$p$. 
\end{frame}



\begin{frame}{Distribution of sample proportions}
We can do distributions of $\hat{p}$'s in the same way.
In \textit{StatKey} click on \textit{CI For a Proportion}, then
edit the data.
Click on \textit{Edit proportion} and enter $0.50$,
\centergraphic[height=.5\textheight]{coin_flip_edit.png}
and also enter \textit{Sample size} of $n=100$.
\end{frame}



\begin{frame}
When you hit \textit{OK} then
\textit{StatKey} looks like this.
\centergraphic[height=.75\textheight]{coin_flip_ready.png}
\end{frame}


\begin{frame}
The computer can simulate a thousand samples.
It is as though there were a thousand people, and  
each flipped a coin $100$ times.
Each person called out their percentage of heads,~$\hat{p}$,
and we recorded them as one thousand dots.
\centergraphic[height=.65\textheight]{coin_flip_thousand.png}
\end{frame}


\begin{frame}{Chance of a way-off sample?}
What is the chance of a sample $\hat{p}$ of $0.62$ or more?
Click on \textit{Right Tail}, then click on the number that appears and
change it to $\text{cutoff}=0.62$. 
(Don't enter $62$, enter $0.62$.)
\centergraphic[height=.55\textheight]{coin_flip_way_off.png}
The computer counts the dots to estimate a 
chance of $0.0060$, a little less than one percent.
\end{frame}




\begin{frame}{Chance of a correct sample?}
What is the chance that a person gets a sample of $\hat{p}=0.50$?
Change the right tail to $\text{cutoff}=0.51$.
Also click on \textit{Left Tail} and set it to $\text{cutoff}=0.49$ 
\centergraphic[height=.55\textheight]{coin_flip_correct.png}
The chance of a ``correct'' sample is actually quite small, in this
simulation between nine and ten percent.
\end{frame}




\begin{frame}
\frametitle{Summary: the challenge of sampling}

Samples vary.
If you have done an experiment and are
holding a sample number then you may worry, 
``I understand that my sample number is probably not the true number,
and I'm worried that it may be way off from the true number.'' 

But, we know from everyday experience that
even in the face of varying samples, we can sometimes make conclusions.
If someone flips a coin a hundred time and they get $54$ heads, 
you figure that probably the coin is good but they got a number that
is a little high.
However, if instead they get $87$ heads then you
have a good idea that the coin is fishy.
So, even though samples vary, sometimes the 
evidence is strong enough for us to be confident of our conclusions.
\end{frame}




% ===========================================


\section{Inference}
\begin{frame}
\frametitle{Inferring things from statistics}

\begin{itemize}
\item
\alert{Descriptive statistics} is the  
straightforward presentation of facts, 
in which modeling decisions have had minimal influence. 
So far, we have mostly been describing.

\item
\alert{Statistical inference} is the set of 
techniques used to make conclusions in the face of
data that is subject to random variation, for example, 
routine sample-to-sample variation.
\end{itemize}

\smallskip
A complete statistical analysis typically includes both 
descriptive statistics and statistical inference. 
Often, it will progress in steps, where the emphasis 
moves gradually from description to inference.
\end{frame}



\begin{frame}{Where we are, and the two paths forward}
In a statistical study, we want to know the
true number, the population parameter, $\mu$ or~$p$. 
But what we actually have is a sample number, $\overline{x}$ or~$\hat{p}$. 

\pause
To make conclusions, we must engineer around the fact that samples vary,
that there are a spread of possible $\overline{x}$'s or~$\hat{p}$'s.
We will have two approaches.

\begin{enumerate}
\item
Using the sample number,
we may compute an interval and say, 
``We are pretty confident the true number is in this interval.''
This is a \alert{confidence interval}.

\item
We may use the sample number to answer yes/no questions.
So we may say something like,
``While we don't know the population number exactly, 
nonetheless from the sample number we are quite confident that
the new treatment is better than the old.''
This is a \alert{hypothesis test}.
\end{enumerate}
\end{frame}

\begin{frame}
\exm
Your friend flips a penny 100 times and gets 43~heads.
The sample proportion is $\hat{p}=0.43$.
Here are the two approaches.
\begin{enumerate}
\item 
We don't necessarily think that 
were we to flip this coin a million times we would find that
the true proportion of heads, the population parameter, 
is~$p=0.43$.
Instead we think that $p$ is close to $0.43$.

How close? 
We will see formulas to compute an interval and say, 
``We are pretty confident that for this penny, 
were we to flip it millions of times, would exhibit a 
true proportion, a population percentage~$p$, in the following range \ldots''

\item
Alternatively, maybe
we don't care about this coin's true proportion~$p$; we just want to know
whether $p<0.50$.

We will see formulas (and arguments) that allow us to say, 
``Based on the sample number,
we are pretty darn sure that the true number~$p$, whatever it is,
is below fifty percent.''
\end{enumerate}
\end{frame}



% =======================================================

\section{Confidence intervals}


\begin{frame}
\frametitle{Statistical inference terms}

A \alert{parameter} is a number that describes some aspect of a population.
A \alert{statistic} is a number that is computed from a sample.

\pause
\exm
What is the average age of the US population?
We can't do a census to find $\mu$.
We take take a sample, and use the $\overline{x}$ from the sample to
estimate the true value.

The parameter is~$\mu$.
The sample statistic is~$\bar{x}$.

\pause
\exm
Are graduating SMC students are proficient at a
second language?
The population parameter~$p$ 
is the percentage of all seniors that are proficient.
We pick a sample of thirty SMC seniors at random and give them an exam.
We find the sample statistic $\hat{p}$, as a way of 
estimating $p$.

\pause
\smallskip
The parameter is the platonic ideal.
It is what we want to know.
We make an 
estimate based on the sample statistic, 
which are single numbers.
Thus, $\overline{x}$ and $\hat{p}$ are 
\alert{point estimates} of parameters.
\end{frame}



\begin{frame}{Interval estimates}
  Some calculations (that you are not responsible for) show that
  if we flip a coin a hundred times then the chance of getting
  a sample number of $\hat{p}=0.50$ is only about $8$\%.

  This illustrates that when we take a sample then 
  the 
  $\bar{x}$ or $\hat{p}$ is probably not equal to the true number,
  the population parameter $\mu$ or~$p$.
  Instead, we think that probably the sample is close to the population number.
  
An \alert{interval estimate} gives an interval of values in which the
population parameter could lie.
When we get it, the formula will look like this.
The ``some number'' is the \alert{margin of error}.
\begin{equation*}
  \overline{x}-\text{some number}\;\ldots\;\overline{x}+\text{that number}
\end{equation*}
The point of this interval is that probably
the census population number $\mu$ or $p$ lies 
in that interval.

(``Error'' not a good word.  
There is no error here; 
we did not take the sample wrong.
It should be called ``the margin for routine sample-to-sample variation.''
But it isn't.)
\end{frame}



\begin{frame}{Plus and minus}
We write the interval estimate in this way.
\begin{equation*}
  \overline{x}\pm \text{margin of error}
  \qquad\text{or}\qquad
  \hat{p}\pm \text{margin of error}
\end{equation*}

The $\pm$ symbol may be unfamiliar. 
For the interval $1.2\pm 3.4$ we have that $\overline{x}=1.2$ and the 
margin of error is $3.4$. 
The interval is this.
\begin{equation*}
  1.2-3.4\;\ldots\; 1.2+3.4
  \quad\text{which is the interval}\quad
  -2.2\; \ldots\; 4.6
\end{equation*}
\end{frame}


\begin{frame}{Confidence interval}
  Imagine that you run an experiment,
  and now you have a sample statistic $\bar{x}$ or $\hat{p}$.
  
  You don't think that is the true population number.
  But, you are reasonably confident that you are close to the true number.
  How close?
  Plus or minus some margin of error.
  How confident?
  Maybe ninety percent, or ninety five percent, or ninety nine percent.
  
  \pause
  A \alert{confidence interval} for a parameter,
  at a \alert{success level} or \alert{confidence level}~$C$,
  is an interval that will contain the population
  parameter with probability~$C$.

  \pause\smallskip
  The key point: a confidence interval is about the true number,
  the census number, the population parameter.
  Don't get confused:~while it is calculated using a sample statistic such as 
  $\bar{x}$ or~$\hat{p}$, it is nonetheless really about
  the population number, $\mu$ or~$p$.
\end{frame}

  



\begin{frame}{Rule of thumb for $95\%$ confidence intervals}

We often work with a confidence level of $C=0.95$.
That is, it is traditional in many fields to use $95\%$ confidence intervals.

To compute any confidence interval we have seen this formula.
\begin{equation*}
  \text{sample statistic}\pm \text{margin of error}
\end{equation*}
The sample statistic is either $\bar{x}$ or $\hat{p}$, depending on our
experiment.
But what to use for the margin of error?

In this course, we will see two answers.
The first is rough, but is good for developing conceptual understanding,
and illustrating simulation.
The second is an exact formula that we will see later in the semester,
in a couple of weeks.
\end{frame}


\begin{frame} %{Estimating the confidence interval given the SE}
Recall that \textit{StatKey} used simulation to show us a 
distribution of samples.
Here also,
given an experiment's sample, 
we will use \textit{StatKey} to show us (approximations of) the
sampling distribution.
This is called ``bootstrapping'' and we will see how to do it 
in a couple of slides.

Once we know how to do that we will then have the \alert{standard error},
the standard deviation of the samples. 
With that number (assuming that the sampling 
distribution is relatively symmetric and bell-shaped),
this formula gives a
good estimate of the $95\%$ confidence interval.
\begin{equation*}
  \text{sample statistic}\pm 2\cdot\text{standard error}
\end{equation*}
\end{frame}

\begin{frame}{Formula and examples}
This is a
good estimate of the $95\%$ confidence interval.
\begin{equation*}
  \text{sample statistic}\pm 2\cdot\text{standard error}
\end{equation*}
(where he sampling 
distribution is relatively symmetric and bell-shaped), 
\smallskip
\exm
We are doing a study in which we figure that the 
standard error is $\text{SE}=4.12$.
(Agian, we will estimate standard errors in the next section.)
If we take a sample and report that the average proficiency score is
$\overline{x}=54.0$, then the formula below is a 
good approximation of the $95\%$ confidence interval.
\begin{equation*}
  54.0\pm 2\cdot 4.12
  \qquad
  54.0-8.24\;\ldots\;54.0+8.24
\end{equation*}
\textcolor{red}{Again, the point of a confidence interval}:~we 
are pretty sure that the true number, 
the population parameter $\mu$,
the average proficiency level if we did a census,
lies between $45.76$ and~$62.24$.
\end{frame}


\begin{frame}
\exm
In a study of five-year recurrence rates we take sample. 
We report $\hat{p}=0.43$ and also estimate the standard error to be $0.11$.
Our $95\%$ confidence interval is
$0.43\pm 2\cdot 0.11$.
\begin{equation*}
  0.32\;\ldots\;0.54
\end{equation*}
We are confident that the interval contains~$p$, that if millions of 
patients got this proceedure so that we knew the true recurrence rate,
then it would be in this interval.

\smallskip
\exm
We do a study in which we estimate the standard error to be $12.41$.
We take a sample and report $\overline{x}=121.49$.
Our $95\%$ confidence interval is 
$121.49\pm 2\cdot 12.41$.
\begin{equation*}
  96.67\;\ldots\;146.31
\end{equation*}
We are pretty sure that $\mu$ lies in this interval.
\end{frame}




\begin{frame}{Caution: don't think these}

In one of the studies above, from the sample we calculated
a $95\%$ confidence interval as $54.0\pm 2\cdot 4.12$, which is
the interval from $45.76\;\ldots\;62.24$.

Here are two misinterpretations of that result.
These are \textit{wrong}.
If you are thinking one of these then you need to adjust your thinking.
\begin{enumerate}
\item \textit{$95\%$ of the population lies between $45.76$ and $62.24$}

No, the confidence interval only tells us about the single number $\mu$.
It does not tell us about the rest of the population.

\pause\item
\textit{If I take a sample then I am $95\%$ sure of reporting $\overline{x}$
between $45.76$ and $62.24$}

No, the confidence interval doesn't tell us about samples.
Instead, it only estimates the
the population parameter,~$\mu$.
\end{enumerate}

What's right is that
we are pretty sure that the true number, the census number,
the population number,~$\mu$, lies between $45.76$ and $62.24$.

How sure?
$95\%$ sure.
\end{frame}

\begin{frame}{What does it mean to be $95\%$ sure?}
\exm
Remember the paper bag experiment?
There we knew the population average of the numbers in a bag:~$\mu=3.67$.

Suppose that we bring a thousand people. 
Each person takes out their five piece of paper sample, 
reports their $\overline{x}$,
and calculates a $95\%$ confidence interval.
At the end we say, ``Raise your hand if your interval contains $3.67$.''
Ninety five percent of the people raise their hand.

You are ninety five percent sure of something if you do it lots of times
and ninety five percent of those times it happens the way that you 
were looking for.
\end{frame}




% =========================================
\section{Bootstrapping}


\begin{frame}{Recall: the distribution of samples}
In the paper bag experiment we simulated taking samples from a population.
Into a paper bag we put
a population with five papers labeled~$5$, four labeled~$4$, etc.
\pause
A sample is a person taking out five pieces and reporting 
their average,~$\bar{x}$.
Different people report different~$\bar{x}$'s.
\textit{StatKey} simulated of a thousand samples.
\centergraphic[height=.5\textheight]{paper_bag_thousand.png}
This is the \alert{sampling distribution}.
\end{frame}




\begin{frame}{What affects spreadedness of the sampling distribution?}
The standard deviation of the sampling distribution is the
\alert{standard error}.
It  depends on two things.

\begin{enumerate}
\pause\item \textit{The size of each individual sample}
Larger samples are better estimates.
If in science class we roll a ball down an inclined
plane once, then we could get a crazy number.
But if we perform the experiment twenty times and report the 
average time $\overline{x}$,
then it is unlikely to be way off.

\pause\item
\textit{The spreadedness of the population being sampled}
Compare two studies:
the first asks people for their income last year, while 
the second asks how many times they went skiing last year.
For the first, there are people making \$0 and there are people making
\$100,000.
But for the second there is nobody who last year went skiing 100,000 times.
We expect that samples from the first population can be more spread
because the underlying population is more spread.
\end{enumerate}
\end{frame}


\begin{frame}{Aside: for sample sizes, bigger is better but not much}
To try to reduce the standard error we can enlarge the sample size.
But we run into two factors.
\begin{itemize}
\item \textit{Bigger samples cost more} 
  They cost more both in time and money.
\item \textit{A sample that is twice as big does not have $1/2$ the spreadedness} 
  This is \alert{diminshing returns}.
  (The potential error falls with the square root of the sample size.
  Thus, to halve the error you need $4$ times the sample size.
  That's not a big problem when your sample is size~$10$, but when
  your sample is size~$1000$, it calls for a lot more 
  effort.)
\end{itemize}
\end{frame}



\begin{frame}{Getting population information from the sample}
We want to know the standard error, 
the spreadedness of the sampling distribution.
Often we have no real idea of this number; 
how to estimate it?

\exm
In the paper bag simulation the underlying population is this.
\begin{center}
  5,5,5,5,5,\,4,4,4,4,\,3,3,3,\,2,2,\,1
\end{center}
In a typical study we do not know the parameter mean~$\mu$.
Imagine that we pull a size five sample and get this.
\begin{center}
  5,5,3,2,1
\end{center}
Our sample mean is $\bar{x}=3.2$.
But how to get information about the sampling distribution?
We only have the one sample.
\end{frame}


\begin{frame}{Bootstrapping: approximating the sampling distribution}
Our only information about the population is the sample.
We will use it to estimate the sampling distribution.

\exm We use the sample as a surrogate for the population.
\begin{enumerate}
\item
Consider again the size-$n=5$ sample:
$5$, $5$, $3$, $2$, $1$.
\item Turn those five papers over so you can't see the numbers, mix them up,
and pick one at random.
Write that number down,
put the paper back (this is called \alert{replacement}), 
mix the papers,
and pick again.
Repeat until you have picked five numbers 
(because the original sample has five).
Report the average,~$\bar{x}$.
\item Repeat the prior step, picking size~$5$ samples with replacement, 
many times.
The distribution of $\bar{x}$'s 
is your approximation of
sampling from the paper bag, from the population.
\end{enumerate}
\end{frame}

\begin{frame}{Bootstrapping in general}
\begin{enumerate}
\item Start with a sample from
  the underlying population.
  Suppose it has size~$n$.
\item 
  Using that, sample with replacement until you have $n$ numbers.
  This set of numbers is one \alert{bootstrap sample}.
  Report the statistic that you want, 
  the \alert{bootstrap statistic}.
  For us this will be the sample average, $\bar{x}$.
\item
  Repeat the prior step many times.
  That gives a \alert{bootstrap distribution}.
\end{enumerate}
\end{frame}


\begin{frame}
\textit{StatKey} lets us do this quickly.
Enter the sample $5$, $5$, $3$, $2$, $1$.
\begin{center}
  % \hspace*{-2em}
  \includegraphics[height=0.75\textheight]{bootstrapci_edit.png}
\end{center}
\end{frame}
\begin{frame}
This is from generating $1000$ $\bar{x}$'s, using sampling with replacement.
\centergraphic[height=0.625\textheight]{bootstrap1.png}  
Estimate the standard error of the paper bag sampling distribution
as $\text{std.\ error}=0.742$, shown on the upper right of the graph.
This is the $95\%$ confidence interval:
$  \bar{x} \pm 2\cdot \text{standard error}  
  =3.2     \pm 2\cdot 0.742                   
  =3.2     \pm 1.484        
$.
It is the interval $1.716\;\ldots\;4.684$.
We are pretty sure $\mu$ lies in there.
\end{frame}



\begin{frame}{Proportions}
We can do the same thing with a sample $\hat{p}=48/100$.
\begin{center}
  % \hspace*{-2em}
  \includegraphics[height=0.75\textheight]{bootstrapciprop_edit.png}
\end{center}
\end{frame}
\begin{frame}
This is from generating a thousand bootstrap samples.
\centergraphic[height=0.65\textheight]{bootstrap_prop.png}  
We get this $95\%$ confidence interval:
$
  \hat{p} \pm 2\cdot \text{standard error}  =
  0.48    \pm 2\cdot 0.050                  = 
  0.48    \pm 0.100 
$, which is the interval
$0.38\;\ldots\;0.58$.
We are pretty sure $p$ lies in there.
\end{frame}

\begin{frame}
Alternatively, \textit{StatKey} will find the interval for us.
Click on ``Two-tail'' in the upper left.
\begin{center}
  % \hspace*{-2em}
  \includegraphics[height=0.7\textheight]{bootstrap_prop_int.png}
\end{center}
\textit{StatKey} just counts, 
coloring red $2.5$\% of the dots on each side. 
\end{frame}




\begin{frame}{Confidences other than $95\%$}
  We can get the computer to make any confidence level we want.
  Click on the ``$95$'' and enter $0.90$
  \centergraphic[height=0.65\textheight]{bootstrap_prop_int2.png}  
Again \textit{StatKey} just counts the dots, 
coloring red $5$\% of the dots on each side. 
\end{frame}




\begin{frame}{Caution}
  For this to give accurate results you must look and check that
  the bootstrap distribution is reasonably bell-shaped and
  is also reasonably symmertic around the original statistic,
  here $\bar{x}$ or $\hat{p}$.

  If it is not, then you must use methods that lie outside our
  scope.
  Consult an expert.
\end{frame}

\end{document}




