\documentclass{article}
\usepackage{overhead}

\begin{document}
\begin{slide}
  \slidetitle{Gathering Data}

\begin{slidepoints}
  \item[How might an statistical investigation arise?]
    \begin{description}
      \item[A prior investigation; exploratory data analysis]
        The question can be a next step of some research program.  
        \textit{A researcher who has established an effect on test scores 
          in elementary school
          students who do not receive breakfast may decide to study if 
          students with such a history can have their grades improved by a
          school breakfast program.}
      \item[A controversy]
        A person may want to settle some hot question.
        \textit{Are radio waves linked to cancer?}
      \item[Anecdotal evidence]
        An investigator wonders if there is a larger pattern in a 
        small set of occurrences.
        \textit{A doctor investigates headaches among people 
          taking the new cold pill.} 
      \item[Ordered up]
        An investigator gets money to do a study from someone 
        (company, political activist, lawyer) who 
        believes the outcome strengthens their position.
        \textit{A drug company, about to market a new pain pill,
          underwrites a study on side effects of the current treatment.}
    \end{description}
  \item[Deciding whether to go ahead]
    \begin{description}
      \item[Is the question worth asking?]
        is it already known?
        Is it important---is is worth spending some fraction of your
        life on?
      \item[Can what we want to know be found out?]
        What data will be collected?
        What will be measured?   How?  When?
      \item[About what population are we asking?]
        Can we identify members of that population?
        Is it too big to survey, and so we must sample instead?
        How to sample?         
      \item[Cost?]
        How will we pay for it, in time and money?
    \end{description}

Remember: most studies are not neutral---we usually gather statistics to 
support some position.

There are two kinds of statistical investigations.
\begin{slidepoints}
  \item[Observational studies]
    The investigator observes the variation of interest, but does nothing
    active.
    \textit{We compare times in the one hundred yard dash 
    between a the students in the class who smoke and those who don't.}
  \item[Experiment]
    The investigator institutes controls; a treatment is deliberately 
    imposed on some individuals and witheld from others.
    The investigator acts in the desire that the two groups differ only
    in whether or not the treatment is applied
    (various strategies are employed to keep all other differences constant).
    \textit{We get a hundred people with arthritis and give half of 
    them a new pill, while the other half get a similar-looking, but
    inactive pill.} 
\end{slidepoints}
\end{slide}


\begin{slide}
  \slidetitle{Observational Studies}

In an \emph{observational study} the investigator observes the variation 
but does not act to sharpen the data.

\begin{example}
Jane Goodall
goes with the chimp troop, waits until she believes that they are used to her,
and then records the diet that she sees them eating.
\end{example}

\begin{example}
There are five known fossils of Archypterox (a proto-bird).
To judge if they are more bird or reptile, we measure the
lengths of some bones and compare the ratios with living animals.   
\end{example}

\begin{example}
We are interested in whether having an older mother is, in part, 
a cause of troubled pregnancies.
We look at the files of every birth in the local hospital over the past year
and draw up a table whose rows give the age of the mother and whose columns 
list the percentage of pregnacies that are `routine', `mildly troubled',
or `quite troubled'.
\end{example}

\begin{example}
To help doctors choose which of two treatments for blocked arteries is better,
a study looks at 250 patients who have choosen one method or the other, and 
sees how they have turned out. 
\end{example}

\begin{example}
A doctor trying out a new pill that the drug company rep gave out gives it 
to all the patients who came in this week with mild pain.
A week later, the patients are called and $60\%$ say that they feel better.   
\end{example}

Problems with the last case.
\begin{itemize}
  \item How do we know that there is any effect at all?
    Maybe without treatment $60\%$ get better
    (maybe $70\%$; remember bloodletting?).
  \item How do we know the people even took the pills?
  \item Maybe people say they feel better because the doctor's office took
     the trouble to call back.
  \item Even if the pill does have an effect, maybe it is not as good, or much
     more expensive, thatn the current treatment.
  \item Maybe some aspect of this doctor's patients predisposes them to 
     do well with this pill (older, etc; maybe these patients are mostly
     working class and this pill is good at body aches).
  \item Maybe, just by happenstance, this doctor got a high sample.
     Perhaps next week's patients would give give a low sample.
\end{itemize}
\end{slide}



\begin{slide}
  \slidetitle{Experiment}
In an \emph{experiment} the observer acts to make the data as useful as
possible.
The investigator imposes some change in order to see its effect.

\begin{example}
Polio hit the US in 1916 and until the 1950's killed many people.
The vaccine developed by Salk was known to cause the production of antibodies
agains polio; did it prevent the disease?
If the treatment were simply given to all children in the country, a drop in 
the disease rate might be because of the vaccine.
But it might be simply because the rate varied from year to year
(e.g., the 1953 rate was half of the 1952 rate).

In one study, researchers took children (whose parents had given permission 
for them to be in the experiment), and split them into two groups.
Both groups got shots; the first group got the vaccine while the second got 
a shot of salt water.
\begin{center}
  \textit{results of Salk 
    trial}\footnote{\emph{An evaluation of the 1954 poliomyelitis 
    vaccine trial}, Thomas Francis, Jr., American Journal of Public 
    Health, 1954, p.~1-53}  \\
  \begin{tabular}{rl}
    group      &rate of polio per 100,000 \\
    \hline
    vaccine    &28                        \\
    salt water &71
  \end{tabular}
\end{center}
\end{example}

\end{slide}


\begin{example}
  A study of young children found that those with more body fat tended to have
  more ``controlling'' mothers.\footnote{\emph{Parents and Children's Adioposity and Eating Style}, S.L.~Johnson, L.L.~Birch, Pediatrics, 1994, p.~1150-1155} 
\end{example}

\begin{example}
California evaluates a new program to rehabilitate prisoners before their 
release.
They get several months of ``boot camp'', military-style training with 
strict discipline.
Admission to the program is voluntary (most prisoners are promised a
reduction in time served in return for volunteering).
A prison spokesman says, ``Those who complete boot camp are less likely
to return to prison than other inmates.''\footnote{hypothetical, from
David Freedman, et.\ al.}  
\end{example}


\begin{example}
Many observational studies have found that people who get many extra
vitamins by eating five or more 
servings of fresh fruit and vegtables each day (especially those of a type
including broccoli) have much lower death rates from colon cancer and 
lung cancer.
Some large randomized controlled experiments were done;\footnote{\emph{A 
clinical trial of antioxidant vitamins to prevent colorectal adenoma}, 
E.R.~Greenburg et.\ al., New England Journal of Medicine, 1994, p.~141-147, 
and \emph{Effect of Vitamin E and Beta Carotene on the Incidence of Lung 
Cancer and Other Cancers in Male Smokers}, O.P.~Heinonen, New England 
Journal of Medicine, 1994, p.1029-1035} 
a treatment group go extra vitamins and the control group ate their usual diet.
There was found to be no difference between the treatment and 
control groups.   
\end{example}


\begin{example}
According to a study at Kaiser Permanente, users of oral contraceptive have
a higher rate of cervical cancer than nonusers (even after adjusting for
factors such as age, education, and marital status).
They conclude that the Pill causes cancer.\footnote{\emph{The Incidence of 
Cervical Cancer and Duration of Oral Contraceptive Use}, E.~Peritz et.\ al.,
American Journal of Epidemiology, 1977, p.~462-469}
\end{example}


\begin{example}
Researchers in several different countries found that cervical cancer was 
quite rare among Jews.
It was also found to be rare among Moslems.
In the 1950's several investigators announced that they were satisfied 
that circumcision of males was the protective factor.\footnote{\emph{A study
of environmental factors in carcinoma of the cervix}, E.L.~Wynder et.\ al.,
American Journal of Obstetrics and Gynecology, 1954, p.~1016-1052}
But there are other differencs between Jews and Moslems, and other well-studied
groups.
Current research suggests that cervical cancer is a sexually transmitted 
disease (the human papiloma virus is implicated); early researchers did
not pay attention to this confounding variable (in part because cancer takes
so long to develop, and so sexual behavior in the 1930's and 1940's is the
variable at issue).\footnote{c.f., \emph{A Review of Problems of Bias and 
Confounding in Epidemologic Studies of Cervical Neoplasia and Oral 
Contraceptive Use}, S.H.~Swan and D.B.~Pettiti, American Journal of 
Epidemology, 1982, p.~10-18}  
\end{example}


\begin{example}
The disease condition Pellagra was first observed in Euurope in the
1700's, especially in the peoples of northern Italy.
It spread in the 1800's into southwestern France, Austria, Romania,
and parts of the Turkish Empire.
It was recognized in Egypt, South Africa, and was prevelant in the south 
of the US.
It seemed to hit some villages more than others, and even some households more
than others.
One blood-sucking fly was had roughly the same geographical range as
the area of greatest disease, and further, the fly was most active in the
spring which was when the disease was most prevalent.
Many epidemiologists figured that this was another insect-borne disease
like malaria (or Lyme). 

Around 1914, the American epidemiologist Joseph Goldberger showed
(in a combination of observational studies and experiments) that pellagra
is caused by bad diet, not an insect.
Milk, meat, and eggs contain a nutritional component that prevents 
pallegra but corn contains little of this component.
Poor people ate corn, mostly.
Since 1940 most of the flour sold in the US is enriched with a vitamin
which prevents the disease (niacin). 
\end{example}

\begin{example}
With cirrhosis of the liver, a patient may hemorrhage.
One treatment is to redirect the blood through a `portacaval shunt'.
This summarizes the studies on the effectiveness of this 
treatment.\footnote{\textit{The Present Status of Shunts for Portal 
Hypertension in Cirrhosis}, N.D.~Grace, et.\ al., Gastroenterology, 1966, 
p.~684-691}
\begin{center}
  \begin{tabular}{l|rrr|r}
                    &\multcolumn{3}{|ccc|}{\textit{degree of enthusiasm}} & \\
     \textit{methodology} 
         &\multcolumn{1}{|c}{\textit{strong}}  
         &\multcolumn{1}{c}{\textit{moderate}}  
         &\multcolumn{1}{c|}{\textit{none}}
         &\textit{percentage strong}                        \\
      \hline
      No controls               &$24$      &$7$    &$1$  &$75\%$  \\
      Controls, not randomized  &$10$      &$3$    &$2$  &$67\%$  \\
      Randomized controls       &$0$       &$1$    &$3$  &$0\%$   \\
      \hline
  \end{tabular}
\end{center}
This table illustrates the same bias; here are the three-year survival rates.
\begin{center}
  \begin{tabular}{l||r|r|}
         &\textit{Randomized control}  &\textit{Historical control}  \\
      \hline
    Treatment (surgery) group   &$60\%$  &$60\%$  \\
    Control group               &$60\%$  &$45\%$
  \end{tabular}
\end{center}
\end{example}

\begin{example}
Coronary bypass surgery is widely used.
Does it help people live longer?
These are the three-year survival rates reported in two types 
of studies. 
The first type randomly assigns patients to control or treatment group
(there were six studies totaling $,9290$ patients).
The second type uses historical controls (nine studies with $18,861
patients).\footnote{\textit{Randomized versus Historical Controls for 
Clinical Trials}, H.~Sacks, et.\ al., American Journal of Medicine, 
1982, p.~233-240}
\begin{center}
  \begin{tabular}{l||r|r|}
         &\textit{Randomized control}  &\textit{Historical control}  \\
      \hline
    Treatment (surgery) group   &$87.6\%$  &$90.9\%$  \\
    Control group               &$83.2\%$  &$71.1\%$
  \end{tabular}
\end{center}
A few studies did not report three-year rates.
Overall, the summary is this. 
\begin{center}
  \begin{tabular}{l||rr|rr|}
                       &\multicolumn{4}{c}{\textit{Type of control}}  \\
     \cline{2-5}                                                     \\
                       &\multicolumn{2}{c}{\textit{randomized}}
                       &\multicolumn{2}{|c}{\textit{historical}}     \\
     \cline{2-5}                

    \textit{Therapy}   &\textit{num positive} &\textit{num negative}
                       &\textit{num positive} &\textit{num negative} \\
    \hline
    Coronary bypass    &$1$  &$7$   &$16$  &$5$   \\
    \hline
  \end{tabular}
\end{center}
The less-carefully done studies have a greater tendency to be positive.
\end{example}

\begin{example}
Here is the same effect with three other treatments.  
\begin{center}
  \begin{tabular}{l||rr|rr|}
                       &\multicolumn{4}{c}{\textit{Type of control}}  \\
     \cline{2-5}                                                     \\
                       &\multicolumn{2}{c}{\textit{randomized}}
                       &\multicolumn{2}{|c}{\textit{historical}}     \\
     \cline{2-5}                

    \textit{Therapy}   &\textit{num positive} &\textit{num negative}
                       &\textit{num positive} &\textit{num negative} \\
    \hline
     5-FU   &$0$  &$5$   &$2$  &$0$   \\
     BCG   &$2$  &$2$   &$4$  &$0$   \\
     DES   &$0$  &$3$   &$5$  &$0$   \\
    \hline
  \end{tabular}
\end{center}
The first treatment is used in chemotherapy for colon cancer.
The second is used for melanoma.

The third DES is an artificial hormone used to prevent spontaneous abortion.
The randomized controlled experiments seemed to persuade few doctors to
be cautious.
Even in the late 1960's, fifty thousand women a year were being given the 
drug.
We now know that if administered to the mother during pregnancy, the drug can 
cause a daughter to develop, twenty years later, a cancer (clear-cell
adencarcinoma of the vagina); it was banned on pregnant women in 1971.
\end{example}


\end{document}

