\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
% \usepackage{present,jh}
\usepackage{present}
\usepackage{../stats}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=red} 

% \usepackage[english]{babel}
% or whatever

% \usepackage[latin1]{inputenc}
% or whatever


\title[ANOVA] % (optional, use only with long paper titles)
{Analysis of Variance}
\subtitle{}
\author{J Hef{}feron}
\institute{
  Mathematics\\
  Saint Michael's College\\
  Colchester, VT\\[1ex]
  \texttt{jhefferon@smcvt.edu.edu}
}
\date{}

\subject{ANOVA}
% This is only inserted into the PDF information catalog. Can be left
% out. 


%\setlength{\parskip}{1ex}



\begin{document}
\begin{frame}
  \titlepage
\end{frame}

\section{One-way ANOVA}
\begin{frame}{Analysis of Variance}
In the prior chapter we went from doing hypothesis tests like this.
\begin{center}
  \begin{tabular}{l}
    $H_0:$ $p_1=p_2$ \\
    $H_a:$ $p_1<p_2$ 
  \end{tabular}
  \quad or\quad
  \begin{tabular}{l}
    $H_0:$ $p_1=p_2$ \\
    $H_a:$ $p_1>p_2$ 
  \end{tabular}
  \quad or\quad
  \begin{tabular}{l}
    $H_0:$ $p_1=p_2$ \\
    $H_a:$ $p_1\neq p_2$ 
  \end{tabular}
\end{center}
to doing tests like this with Chi-square.
\begin{center}
  \begin{tabular}{l}
    $H_0:$ $p_1=p_2=p_3$ \\
    $H_a:$ not all of these proportions are equal
  \end{tabular}
\end{center}
\pause
We now do the same with means: we go from 
\begin{center}
  \begin{tabular}{l}
    $H_0:$ $\mu_1=\mu_2$ \\
    $H_a:$ $\mu_1<\mu_2$ 
  \end{tabular}
  \quad or\quad
  \begin{tabular}{l}
    $H_0:$ $\mu_1=\mu_2$ \\
    $H_a:$ $\mu_1>\mu_2$ 
  \end{tabular}
  \quad or\quad
  \begin{tabular}{l}
    $H_0:$ $\mu_1=\mu_2$ \\
    $H_a:$ $\mu_1\neq \mu_2$ 
  \end{tabular}
\end{center}
to doing tests like this with ANOVA.
\begin{center}
  \begin{tabular}{l}
    $H_0:$ $\mu_1=\mu_2=\mu_3$ \\
    $H_a:$ not all of these means are equal
  \end{tabular}
\end{center}
\end{frame}


\begin{frame}{Definition}
\alert{Analysis of Variance (ANOVA)} is a statistical method used
to compare the means of two or more groups.

\exm We might test three dosages of medicine.
The dependent variable is the pain level.
\begin{center} \small
  \begin{tabular}{r|l}
    \multicolumn{1}{c}{\textit{Dosage}}
       &\multicolumn{1}{c}{\textit{Reported pain level out of ten}}  \\
    \hline
    None~$0$mg   &$9,8,7,8,8,9,8$  \\
    Low~$50$mg   &$7,6,6,7,8,7,6$  \\
    High~$100mg$ &$4,3,2,3,4,3,2$
  \end{tabular}
\end{center}
\pause
In ANOVA the independent variables are \alert{factors}.
There is one factor here, dosage. 
\pause
Each factor will have two or more \alert{groups} (or \alert{levels},
these are categorical variables).
The factor of dosage has three groups. 
\pause
The \alert{degrees of freedom} for a factor is the number of groups minus one.
\end{frame}


\begin{frame}{Intuition}
Contrast these three scenarios.
\begin{center}
  \includegraphics{asy/anova00.pdf}  
  \hspace{2em plus 0.5fil}
  \includegraphics{asy/anova01.pdf}  
  \hspace{2em plus 0.5fil}
  \includegraphics{asy/anova02.pdf}
\end{center}
In $A$ the means are not very separated, and the data is pretty spread.
It looks like the null hypothesis could be true.
\pause
In $B$, while the means are still close, because the spreadedness is so 
small, it looks like we should reject the null hypothesis.
\pause
Data set $C$ is spread out but the means differ so much that 
it again looks like we should reject the null hypothesis.

Thus, whether to reject or not reject depends both on the variation between the
groups and on the variation within each group.
\end{frame}



\begin{frame}
The variation between the groups is called \alert{$SSG$}.
The variation within each group is called \alert{$SSE$}.
Add them to get \alert{$SSTotal$}.

\pause
There are $n$~pieces of data so 
the \alert{degrees of freedom of SSTotal} is $n-1$.
There are $k$~groups so the \alert{degrees of freedom of $SSG$} is $k-1$.
Likewise, the \alert{degrees of freedom for $SSE$} is  $n-k$.

\pause
These are the \alert{Mean Square} statistics.
\begin{equation*}
  MSG = \frac{SSG}{k-1}
  \qquad
  MSE = \frac{SSE}{n-k}
\end{equation*}
The \alert{F-statistic} is the ratio $MSG/MSE$.
This is like the chi-square statistic, or the $t$~statistic, or the
$z$~statistic, in that you use it as a scale to find the $p$~value
from the curve.

\medskip
\rem
\begin{itemize}
\item The ``$SS$'' stands for for ``sum of squares.''
  We use this because of the distance formula, the Pythagorean formula.
\item
The ``$E$'' stands for ``error.'' 
We have mentioned before that this historically-used word survives 
even though it is misleading in that it does not imply some kind of mistake
happened. 
\end{itemize}
\end{frame}


\begin{frame}{The assumptions for ANOVA}
\begin{itemize}
\item The populations have the same, or roughly the same standard deviations
  (within a factor of $2$).
\item The populations are normally distributed.
\item Each value is sampled independently from each other value. 
  This assumption requires that each subject provide only one value. 
  If a subject provides two scores, then the values are not independent. 
\end{itemize}

With those assumptions, the $p$-value is the upper tail of $F_{k-1,n-k}$.
\end{frame}



\begin{frame}{One way ANOVA example}
Recall the example of testing three dosage levels.
\begin{center} \small
  \begin{tabular}{cr|l}
    \multicolumn{1}{c}{\textit{Case}}
       &\multicolumn{1}{c}{\textit{Dosage}}
       &\multicolumn{1}{c}{\textit{Reported pain level out of ten}}  \\
    \hline
    1 &None~$0$mg   &$9,8,7,8,8,9,8$  \\
    2 &Low~$50$mg   &$7,6,6,7,8,7,6$  \\
    3 &High~$100mg$ &$4,3,2,3,4,3,2$
  \end{tabular}
\end{center}
Here is the side-by-side boxplot picture.
Looks like maybe $\mu_1$ is different.
\begin{center} \small
  \vcenteredhbox{\includegraphics{asy/anova03.pdf}}  
  \hspace*{2em plus 0.5fil}
  \begin{tabular}{l}
    \multicolumn{1}{c}{\textit{Five number summaries}} \\  \hline
      $7$, $8$, $8$, $8.5$, $9$  \\
      $6$, $6$, $7$, $7$, $8$    \\
      $2$, $2.5$, $3$, $3.5$, $4$
  \end{tabular}
\end{center}

Is there sufficient evidence of a difference between at least one of the
means and the others?
\end{frame}

\begin{frame}
We state the hypotheses.  We use $\alpha=0.05$.
\begin{center}
  \begin{tabular}{l}
    $H_0:$ $\mu_1=\mu_2=\mu_3$  \\
    $H_a:$ at least one mean differs from the others
  \end{tabular}
\end{center}
\pause This is the ANOVA table.
\begin{center}
\begin{tabular}{r|r*{2}{r@{$.$}l}|r@{$.$}l}
   \multicolumn{1}{c}{\ }
     &\multicolumn{1}{c}{df}
     &\multicolumn{2}{c}{SS}
     &\multicolumn{2}{c}{MS}
     &\multicolumn{2}{c}{F}  \\
     \cline{2-6}
     Groups &$2$   &$98$  &$67$  &$49$ &$333$  &$86$ &$333$  \\
     Error  &$18$  &$10$  &$29$  &$0$ &\multicolumn{1}{@{}l|}{$571$} \\ 
     \cline{2-6}
     \multicolumn{1}{r}{Total}  &$20$  &$108$ &$95$  
\end{tabular}  
\end{center}
\end{frame}


\begin{frame}{Topic: formulas}
Here is where those numbers come from.
\begin{enumerate}
\item The overall average is $\bar{x}=5.95$. 
\item $SSTotal = (9-5.95)^2+(7-5.95)^2+(4-5.95)^2+(8-5.95)^2+\cdots = 108.95$
\item An algebraically-equivalent formula is $SSTotal=(n-1)\cdot s^2$ where 
  $s$ is the standard deviation of the entire data set.
\item $SSG = 7\cdot (8.1-5.95)^2+7\cdot(6.7-5.75)^2+7\cdot(3.0-5.75)^2 = 98.67$
\item $SSE = SSTotal-SSG$
\end{enumerate}
We will let the computer do the calculations.
\end{frame}


\begin{frame}
To do the calculation, start by entering the data.
\begin{center}
  \includegraphics[height=0.5\textheight]{pain.png}
\end{center}
\end{frame}
\begin{frame}
You get summary statistics.
\begin{center}
  \includegraphics[height=0.38\textheight]{pain1.png}
\end{center}
You can also get an ANOVA table.
\begin{center}
  \includegraphics[height=0.38\textheight]{pain2.png}
\end{center}
\end{frame}

\begin{frame}
To get some idea of what is going on you can do a simulation, 
under the Null Hypothesis.  
\begin{center}
  \includegraphics[height=0.38\textheight]{pain3.png}
\end{center}
You could find some simulation answers here but we will go to the F-table.
\end{frame}

\begin{frame}
Enter the degrees of freedom.
\begin{center}
  \includegraphics[height=0.38\textheight]{pain4.png}
\end{center}
\pause Here is the correct version of the F-table.
\begin{center}
  \includegraphics[height=0.38\textheight]{pain5.png}
\end{center}
\end{frame}

\begin{frame}
Compute the $p$~value.
\begin{center}
  \includegraphics[height=0.5\textheight]{pain6.png}
\end{center}
There is basically zero chance, assuming the Null Hypothesis,
of observing what we observed.
We reject $H_0$.

You may see this written ``The three groups differ significantly
$F(2,18)=86.333$, $p<0.05$.''
\end{frame}



\begin{frame}
\exm 
We think that students will learn best if while they are studying
there is a constant sound
(such as a waterfall loop) rather than complete silence, or 
ambient sounds.
Here are the hypotheses (the means are the average scores). 
\begin{center}
  \begin{tabular}{l}
    $H_0:$ $\mu_1=\mu_2=\mu_3$  \\
    $H_a:$ at least one mean differs from the others
  \end{tabular}
\end{center}
\pause
We gather data: students study for thirty minutes and then take a quiz.
\begin{center} \small
  \begin{tabular}{r|l}
    \multicolumn{1}{c}{\textit{Condition}} 
      &\multicolumn{1}{c}{\textit{Scores, out of ten}} \\ \hline
    \textit{Constant sound} &$7,4,6,8,6,6,2,9$   \\
    \textit{Ambient sound}  &$5,5,3,4,4,7,2,2$   \\
    \textit{No sound}       &$2,4,7,1,2,1,5,5$   \\
  \end{tabular}
\end{center}
We will test with $\alpha=0.01$.
\end{frame}


\begin{frame}
Enter the data into \textit{StatKey}.  
\begin{center}
  \includegraphics[height=0.45\textheight]{sound.png}  
\end{center}
\pause
\begin{center}
  \includegraphics[height=0.4\textheight]{sound1.png}  
\end{center}
\end{frame}
\begin{frame}
\textit{StatKey} gives this ANOVA table.
\begin{center}
  \includegraphics[height=0.45\textheight]{sound2.png}  
\end{center}
\begin{center}
\begin{tabular}{r|r*{2}{r@{$.$}l}|r@{$.$}l}
   \multicolumn{1}{c}{\ }
     &\multicolumn{1}{c}{df}
     &\multicolumn{2}{c}{SS}
     &\multicolumn{2}{c}{MS}
     &\multicolumn{2}{c}{F}  \\
     \cline{2-6}
     Groups &$2$   &$38$  &$08$  &$15$ &$042$  &$3$ &$595$  \\
     Error  &$21$  &$87$  &$88$  &$4$ &\multicolumn{1}{@{}l|}{$185$} \\ 
     \cline{2-6}
     \multicolumn{1}{r}{Total}  &$23$  &$117$ &$96$  
\end{tabular}  
\end{center}
\end{frame}

\begin{frame}
Go to the $F$~curve, entering the degrees of freedom.  
\begin{center}
  \includegraphics[height=0.45\textheight]{sound3.png}  
\end{center}
\end{frame}

\begin{frame}
On that curve, enter $F=3.595$.  
\begin{center}
  \includegraphics[height=0.55\textheight]{sound4.png}  
\end{center}
We get a $p$~value of $0.045$.
That is not less than $\alpha=0.01$, so we fail to reject.
\end{frame}
\end{document}



