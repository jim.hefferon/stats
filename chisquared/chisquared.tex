\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
% \usepackage{present,jh}
\usepackage{../present}
\usepackage{../stats}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=red} 

% \usepackage[english]{babel}
% or whatever

% \usepackage[latin1]{inputenc}
% or whatever


\title[Chi-square] % (optional, use only with long paper titles)
{Chi-square}
\subtitle{}
\author{J Hef{}feron}
\institute{
  Mathematics\\
  Saint Michael's College\\
  Colchester, VT\\[1ex]
  \texttt{jhefferon@smcvt.edu.edu}
}
\date{}

\subject{Chi-square}
% This is only inserted into the PDF information catalog. Can be left
% out. 


%\setlength{\parskip}{1ex}



\begin{document}
\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Proportions}
In the prior chapter
we looked at tests for a single proportion.

For example, 
we might test for the effect of a person's sex on 
the chance they get an allergic reaction to our drug. 
We might take a sample and get numbers.
\begin{center} \small
  \begin{tabular}{r|rr}
    \multicolumn{1}{r}{\ }  
       &\multicolumn{1}{c}{\textit{Male}} 
       &\multicolumn{1}{c}{\textit{Female}}     \\ \cline{2-3} 
      \textit{Reaction frequency}   &$0.45$  &$0.55$   \\
  \end{tabular}
\end{center}
With those, we might find a confidence interval using $\hat{p}=0.45$.
Or we might run a hypothesis test of $H_0:\;p=0.50$ against $H_a:\;p<0.50$.

The point is that if, among the people who have a allergic reaction,
if we know the percentage who are men, $p$, then we know the 
percentage who are women, $1-p$.

Similarly, a resturant is considering adding to their menu either 
fried chicken or fried fish then we could ask a sample of people which
they would prefer and report the percentage of chicken as $\hat{p}$,
then knowing that the percentage of fish is $1-\hat{p}$.
\end{frame}


\begin{frame}
What if the variable has more than two categories?
What if the resturant is considering offering one, and only one, of 
fried chicken, fried fish, fried ham, or fried tofu?
We want to know whether they have different percentages, or
whether they are all the same. 

Another example is that we might test a drug by breaking sample subjects
into three groups and giving one group a whole dose, 
one a half dose, and
one no dose.  
We'd like to know whether the three $p$'s are the same or not.
\end{frame}



\section{Chi-square; goodness of fit for a single categorical variable}
\begin{frame}{When there are more than two categories}
\exm
In Rock-Paper-Scissors there are two players.
Each plays either ``rock'' or ``paper'' or ``scissors.''
\begin{center}
  \includegraphics[height=0.3\textheight]{rps.png}
\end{center}
Scissors cuts paper, that is, if one plays scissors and the other plays
paper then scissors wins.
Similarly, paper wraps rock and rock blunts scissors.
\end{frame}

\begin{frame}
You will have the upper hand if you can guess what your opponent will play.
This study reported the first play for $119$ players.
\begin{center} \small
  \begin{tabular}{r|ccc}
    \multicolumn{1}{r}{\ }  %\textit{Option}}
      &\textit{Rock}  &\textit{Paper}  &\textit{Scissors}  \\  \cline{2-4}
    \textit{Frequency}
      &$66$  &$39$  &$14$  
  \end{tabular}
\end{center}
Is the answer ``Rock?''
Or maybe this is just a sample thing and really in the long run
each of the three categories has a $1/3$~chance?

This is the setup of a hypothesis test, but with three proportions.
\begin{center}
    \begin{tabular}{l}
      $H_0:$ $p_{\text{rock}}=1/3$ and $p_{\text{paper}}=1/3$ and $p_{\text{scissors}}=1/3$  \\
      $H_a:$ some $p$ is not as specified in the prior line 
    \end{tabular}
\end{center}
\end{frame}


\begin{frame}{Conducting a Chi-square test: expected counts}
In the data table\begin{center} \small
  \begin{tabular}{r|ccc}
    \multicolumn{1}{r}{\ }  %\textit{Option}}
      &\textit{Rock}  &\textit{Paper}  &\textit{Scissors}  \\  \cline{2-4}
    \textit{Frequency}
      &$66$  &$39$  &$14$  
  \end{tabular}
\end{center}
there are three \alert{cells}.

A hypothesis test starts by assuming $H_0$.
Here, that means that each category has a  $1/3$ chance. 
We get the \alert{expected counts}, $n\cdot p$.
\begin{center} \small
  \begin{tabular}{r|ccc}
    \multicolumn{1}{r}{\ }  %\textit{Option}}
      &Rock  &Paper  &Scissors  \\  \cline{2-4}
    \textit{Frequency}
      &$66$  &$39$  &$14$  \\
    \textit{Expected count}
      &$119\cdot 0.33=39.7$  &$119\cdot 0.33=39.7$  &$119\cdot 0.33=39.7$  
  \end{tabular}
\end{center}
\medskip
In some problems the chances differ from category to category.
That happens in the example after this one.

(The test we are developing works well if each expected count is at least~$5$.)
\end{frame}

\begin{frame}{Definition}
\dfn
The \alert{chi-square statistic $\chi^2$} comes from calculating 
this 
\begin{equation*}
 (\text{observed}-\text{expected})^2/\text{expected}
\end{equation*}
for each cell, and then adding them together.

The \textit{Contribution} line below contains the numbers 
$(\text{observed}-\text{expected})^2/\text{expected}$.
\begin{center} \small
  \begin{tabular}{r|ccc}
    \multicolumn{1}{r}{\ }  %\textit{Option}}
      &\textit{Rock}  &\textit{Paper}  &\textit{Scissors}  \\  \cline{2-4}
    \textit{Frequency}
      &$66$  &$39$  &$14$  \\
    \textit{Expected count}
      &$119\cdot 0.33=39.7$  &$119\cdot 0.33=39.7$  &$119\cdot 0.33=39.7$  \\  
    \textit{Contribution}
      &$17.482$  &$0.011$  &$16.608$  
  \end{tabular}
\end{center}
The total is $\chi^2=34.101$.
\end{frame}


\begin{frame}{Finding $\chi^2$ inside \textit{StatKey}}
At the front page click on \textit{Chi-Squared Goodness of Fit}, 
and click on \textit{Edit Data}.
\begin{center}
  \includegraphics[width=0.9\textwidth]{rps_edit.png}
\end{center}
\end{frame}

\begin{frame}
After you entered the data, click on \textit{Show Details}.
\begin{center}
  \includegraphics[width=0.99\textwidth]{rps_details.png}
\end{center}
It tells you the count, the expected count (in green), 
and the contribution (in blue).
\end{frame}

\begin{frame}{Doing the hypothesis test}
Recall the hypotheses.
\begin{center}
    \begin{tabular}{l}
      $H_0:$ $p_{\text{rock}}=1/3$ and $p_{\text{paper}}=1/3$ and $p_{\text{scissors}}=1/3$  \\
      $H_a:$ some $p$ is not as specified in the prior line 
    \end{tabular}
\end{center}
We want a $P$-value. 
There are three categories, Rock, Paper, and Scissors, so the
$\text{\alert{degrees of freedom}}=\text{number of categories}-1$
is~$2$ for this problem.
\end{frame}


\begin{frame}{Finding the $P$-value with \textit{StatKey}}
Use \textit{StatKey} to show the $\chi^2$ graph, under Theoretical
Distributions.
Remember that $\text{df}=2$.
Click on \textit{Right Tail} and enter the value of $34.101$. 
\begin{center}
  \includegraphics[width=0.99\textwidth]{rps2.png}    
\end{center}
The $P$-value of $3.9\text{e}-8$ is scientific notation for seven $0$'s
followed by a $3$ and a $9$.
Essentially zero.
We reject~$H_0$.
\end{frame}


\begin{frame}{Comments}
\begin{itemize}
\item
  We started by discussing 
  men and women reacting to a drug, with two categories.
  If we did a $\chi^2$ test we would get exactly the same $p$-value.
\item 
  This case had three proportions.  
  When we say that $p_{\text{rock}}=1/3$ and $p_{\text{paper}}=1/3$, we have
  then determined the other one, $p_{\text{scissors}}=1/3$,
  since the three have to add to $1$.
  This is where ``degrees of freedom'' comes from:
  we are free to choose all but the final proportion. 
\end{itemize}
\end{frame}

\begin{frame}{Unequal proportions}
\exm
A company designs a pay increase plan with three categories:
merit raise, inflation-only raise, and no raise.
Their targets are for $25\%$ of employees to get merit raises,
for $65\%$ to get inflation-only raises, and for the 
remaining $10\%$ to get no raise.
At the end of the year they check what actually happened to see if they
came close to the targets.

\pause
Because of sample year-to-sample~year variation they do not think they 
will hit the target exactly.
But could the variation between this year's numbers and the targets
reasonably be ascribed to sample-to-sample stuff?
Or is the observed data just too different for that to be credible?
Use $\alpha=0.01$.
 
\begin{center}
  \begin{tabular}{l}
    $H_0:$ $p_m=0.25$, $p_i=0.65$, $p_n=0.10$ \\
    $H_a:$ some proportion differs
  \end{tabular}
\end{center}
\end{frame}


\begin{frame}
This year's data is here.
\begin{center} \small
  \begin{tabular}{r|ccc}
    \multicolumn{1}{r}{\ }
      &\textit{Merit} &\textit{Inflation} &\textit{None} \\ \cline{2-4}
    \textit{Observed}
      &$193$           &$365$              &$42$         \\
    \textit{Expected}
      &$600\cdot 0.25=150$ &$600\cdot 0.65=390$  &$600\cdot 0.10=60$ 
  \end{tabular}
\end{center}
\pause
Again from \textit{StatKey}'s front page select 
\textit{Chi-square Goodness of Fit}.
Click on \textit{Edit Data}.
\vspace*{-1ex}\begin{center}
  \includegraphics[width=0.9\textwidth]{uneq_edit.png}
\end{center}
\end{frame}


\begin{frame}
Click on \textit{Null Hypothesis} to enter those numbers.
\begin{center}
  \includegraphics[width=0.95\textwidth]{uneq_nulls.png}
\end{center}
\end{frame}


\begin{frame}
The \textit{Show Details} button shows the numbers.
\begin{center}
  \includegraphics[width=0.95\textwidth]{uneq1.png}    
\end{center}
Note the test statistic of $\chi^2=19.330$.
\end{frame}


\begin{frame}{Run the test}
From \textit{StatKey}'s front page again select $\chi^2$.
There are three categories, so $\text{df}=3-1=2$.
Click on \textit{Right Tail}, and enter $19.330$.
\begin{center}
  \includegraphics[height=0.45\textheight]{uneq2.png}    
\end{center}
The $P$-value is $0.000\,063$.  
With $\alpha=0.01$ we reject~$H_0$.  
\end{frame}


% \begin{frame}{Benford's Law}
% In many naturally occurring data sets,
% the first digits are not distributed evenly.
% Instead, the digit~$1$ occurs
% more often than $2$, and so on down to~$9$.

% Take street addresses.
% You can have a street address whose first digit is `$8$' because you live
% at an address of $83$, or because your address is $814$, or because
% your address is $8$.
% But when you ask people for the first digit of their address, it is 
% hardly ever $8$.

% \pause
% \begin{center} \small
%   \makebox[0em][c]{%
%   \begin{tabular}{r|ccccccccc@{}}
%     \textit{digit} &$1$ &$2$ &$3$ &$4$ &$5$ &$6$ &$7$ &$8$ &$9$ \\ \cline{2-10}
%     \textit{freq}  &$0.301$ &$0.176$ &$0.125$ &$0.097$ &$0.079$ &$0.067$ &$0.058$ &$0.051$ &$0.046$
%   \end{tabular}}
% \end{center}
% (The formula is that digit~$d$ occurs with frequency $\log(d+1)-\log(d)$.
% A good, reasonably non-technical, 
% talk is \url{https://www.youtube.com/watch?v=4iz4EHriYz0}.)

% We can examine the first digit of the street address of each 
% person in the room and check if it matches this distribution.
% \end{frame}


% =========================================

\section{Chi-square; test for association or independence
between two categorical variables}

\begin{frame}{The question we want to answer}

Suppose that I told you that I propose to 
predict people's final grade in the class
from where their name falls in my grade book.
That is, suppose that I told you that I'm guessing that
people whose name starts with `A' will do well
and as we move through the alphabet to 
people whose name starts with `Z', they will do more poorly.
Wouldn't you say, ``No, there is no connection.
I think that the categorical variable 
defined by the first letter of a person's last name
does not have anything to do with the categorical variable
defined by whether their class grade is $A$, or $B$, etc.''?
 
\pause
Now suppose I claim that the handedness is related to mathematical ability.
That is, I propose to ask people to self-identify into left-\hbox{}
right-handed or neither.
I also give them a math test, and put them into categories of
mathematically able, and less-able.
Are the two categorical variables of handedness and abilty related?

\pause
We want to find whether two categorical variables are associated in some
way, or independent.
\end{frame}


\begin{frame}{Definition of independent}
\exm
I will have a categorical variable of flipping a coin.
There are two categories: $H$ and~$T$.
Each has probability $1/2$.

I will also have a categorical variable of rolling a dice.
There are six categories: $1$, $2$, \ldots{}, $6$.
Each has probability $1/6$.

\pause
I will roll a dice and flip a coin at the same time.
There are twelve outcomes: $H$ and~$1$, $H$ and~$2$, \ldots{}\,.
We perceive that what you get from flipping is independent of
what you get from rolling.
 
Here is the table of joint probabilities, of the expected numbers.
\begin{center} \small
  \begin{tabular}{r|cccccc}
    \multicolumn{1}{r}{\ }
       &$1$ &$2$ &$3$ &$4$ &$5$ &$6$ \\ \cline{2-7}
    $H$ &$1/12$ &$1/12$ &$1/12$ &$1/12$ &$1/12$ &$1/12$ \\
    $T$ &$1/12$ &$1/12$ &$1/12$ &$1/12$ &$1/12$ &$1/12$ 
  \end{tabular}
\end{center}

\dfn
Two classifications are \alert{independent} if 
the probability of their combination is the product of
their separate probabilities.
Otherwise they are \alert{associated}.
\end{frame}



\begin{frame}{Testing for association}
\exm
We ask $150$~men and $150$~women to identify products 
advertised on TV.
We get this two-way table.
\begin{center} \small
  \begin{tabular}{r|rr|r}
    \multicolumn{1}{r}{\ }
       &\multicolumn{1}{c}{\textit{Male}}
       &\multicolumn{1}{c}{\textit{Female}}
       &\multicolumn{1}{c}{\textit{Total}}   \\ \cline{2-3}
    \textit{Could identify} &$95$  &$41$  &$136$ \\
    \textit{Could not}      &$55$  &$109$ &$164$ \\  \cline{2-4}
    \multicolumn{1}{r}{\textit{Total}}  &$150$ &$150$ &$300$
  \end{tabular}
\end{center}
\pause
We want to run a hypothesis test.
\begin{center}
  \begin{tabular}{l}
    $H_0:$ Ability to identify is not associated with gender \\
    $H_a:$ It is associated
  \end{tabular}
\end{center}
\end{frame}


\begin{frame}{Getting the expected counts}
Look at the overall percentages.
These are called \alert{marginal percentages} (because they are 
in the table's margin).
\begin{center} \small
  \begin{tabular}{r|rr|rr}
    \multicolumn{1}{r}{\ }
       &\multicolumn{1}{c}{\textit{Male}}
       &\multicolumn{1}{c}{\textit{Female}}
       &\multicolumn{1}{c}{\textit{Total}}   
       &\multicolumn{1}{c}{\textit{Percentage}}   
          \\ \cline{2-3}
    \textit{Could identify} &$95$  &$41$  &$136$ &$136/300=0.453$  \\
    \textit{Could not}      &$55$  &$109$ &$164$ &$164/300=0.547$ \\ \cline{2-5}
    \multicolumn{1}{r}{\textit{Total}}  &$150$ &$150$ &$300$  \\
    % \multicolumn{1}{r}{\textit{Percentage}}  &$0.50$ &$0.50$ &  \\
  \end{tabular}
\end{center}
\pause
We take these percentages to be $H_0$'s true numbers.
Now assuming there is no association between ability to identify and 
gender, we multiply to get the {\color{blue}expected cell contents}.
\begin{equation*}
  \text{column total}\cdot\text{marginal probability}
  =
  \frac{\text{column total}\cdot\text{row count}}{\text{sample size}}
\end{equation*}
\begin{center} \small
  \begin{tabular}{r|rr|rr}
    \multicolumn{1}{r}{\ }
       &\multicolumn{1}{c}{\textit{Male}}
       &\multicolumn{1}{c}{\textit{Female}}
       &\multicolumn{1}{c}{\textit{Total}}   
       &\multicolumn{1}{c}{\textit{Percentage}}   
          \\ \cline{2-3}
    \textit{Could identify} &$95$ {\color{blue}($68$)}  &$41$ {\color{blue}($68$)}  &$136$ &$136/300=0.453$  \\
    \textit{Could not}      &$55$ {\color{blue}($82$)} &$109$ {\color{blue}($82$)}  &$164$ &$164/300=0.547$ \\ \cline{2-5}
    \multicolumn{1}{r}{\textit{Total}}  &$150$ &$150$ &$300$  \\
    % \multicolumn{1}{r}{\textit{Percentage}}  &$0.50$ &$0.50$ &  \\
  \end{tabular}
\end{center}
\end{frame}


\begin{frame}{Doing it in \textit{StatKey}}
On \textit{StatKey}'s front page, select 
\textit{$\chi^2$ Test for Association}.
Click on \textit{Edit Data}.
\begin{center}
  \includegraphics[width=0.99\textwidth]{identify.png}    
\end{center}
\end{frame}
\begin{frame}
Click on \textit{Show Details} to see all the numbers.
\begin{center}
  \includegraphics[width=0.99\textwidth]{identify2.png}    
\end{center}
\end{frame}

\begin{frame}
\begin{center}
  \includegraphics[height=0.45\textheight]{identify1.png}    
\end{center}
It says $\chi^2=39.222$.

We will look up the $P$-value using the $\chi^2$ curve in \textit{StatKey}.
Use this formula for the degrees of freedom.
\begin{equation*}
  \text{df}
    =(\text{number of rows}-1)\cdot (\text{number of columns}-1)
\end{equation*}
We get $1\cdot 1=1$.
\end{frame}



\begin{frame}{Running the test, drawing a conclusion}
\begin{center}
  \includegraphics[width=0.9\textwidth]{identify3.png}    
\end{center}
With degrees of freedom of $1$ and $\chi^2=39.222$,
we get a $P$-value of essentially zero.
(It is less than $0.000$.)
We reject $H_0$.
\end{frame}



\begin{frame}{A second example}
\exm
We ask men and women for political affiliation.
We get this two-way table.
\begin{center} \small
  \begin{tabular}{r|rrr|r}
    \multicolumn{1}{r}{\ }
       &\multicolumn{1}{c}{\textit{Republican}}
       &\multicolumn{1}{c}{\textit{Democrat}}
       &\multicolumn{1}{c}{\textit{Other}}
       &\multicolumn{1}{c}{\textit{Total}}   \\ \cline{2-4}
    \textit{Male}   &$200$  &$150$  &$50$    &$400$ \\
    \textit{Female} &$250$  &$300$  &$50$    &$600$    \\  \cline{2-5}
    \multicolumn{1}{r}{\textit{Total}}  &$450$ &$450$ &$100$  &$1000$
  \end{tabular}
\end{center}
\pause
We want to run this hypothesis test at $\alpha=0.05$.
\begin{center}
  \begin{tabular}{l}
    $H_0:$ Gender and voting preferences are independent \\
    $H_a:$ They are associated
  \end{tabular}
\end{center}
\end{frame}



\begin{frame}
Here are the marginal percentages
\begin{center} \small
  \begin{tabular}{r|rrr|rr}
    \multicolumn{1}{r}{\ }
       &\multicolumn{1}{c}{\textit{Republican}}
       &\multicolumn{1}{c}{\textit{Democrat}}
       &\multicolumn{1}{c}{\textit{Other}}
       &\multicolumn{1}{c}{\textit{Total}}   \\ \cline{2-4}
    \textit{Male}   &$200$  &$150$  &$50$    &$400$ $400/1000=0.4$ \\
    \textit{Female} &$250$  &$300$  &$50$    &$600$ $600/1000=0.6$ \\  \cline{2-5}
    \multicolumn{1}{r}{\textit{Total}}  &$450$ &$450$ &$100$  &\multicolumn{1}{l}{$1000$}
  \end{tabular}
\end{center}
and the expected cell counts.
\begin{center} \small
  \begin{tabular}{r|rrr|rr}
    \multicolumn{1}{r}{\ }
       &\multicolumn{1}{c}{\textit{Republican}}
       &\multicolumn{1}{c}{\textit{Democrat}}
       &\multicolumn{1}{c}{\textit{Other}}
       &\multicolumn{1}{c}{\textit{Total}}   \\ \cline{2-4}
    \textit{Male}   &$200$ ($180$)  &$150$ ($180$)  &$50$ ($40$)    &$400$ $400/1000=0.4$ \\
    \textit{Female} &$250$ ($270$) &$300$ ($270$)  &$50$ ($60$)    &$600$ $600/1000=0.6$ \\  \cline{2-5}
    \multicolumn{1}{r}{\textit{Total}}  &$450$ &$450$ &$100$  &\multicolumn{1}{l}{$1000$}
  \end{tabular}
\end{center}
We get $\chi^2=16.203$.
The next slides show how to get all these numbers in \textit{StatKey}.
\end{frame}


\begin{frame}
From \textit{StatKey}'s front page select 
\textit{$\chi^2$ Test for Association}.
Then hit \textit{Edit Data}.
\begin{center}
  \includegraphics[width=0.95\textwidth]{politics.png}    
\end{center}
\end{frame}
\begin{frame}
You see all the numbers if you hit \textit{Show Details}.
\begin{center}
  \includegraphics[width=0.95\textwidth]{politics1.png}    
\end{center}
In particular, $\chi^2=16.203$. 
\end{frame}

\begin{frame}
With $\text{degrees of freedom}=(3-1)(2-1)=2$, and $\chi^2=16.203$,
we find that the $P$-value is $0.00030$.
\begin{center}
  \includegraphics[width=0.95\textwidth]{politics3.png}    
\end{center}
That is much less than $\alpha$. 
We reject~$H_0$.  
\end{frame}

\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 




