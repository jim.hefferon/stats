\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
% \usepackage{present,jh}
\usepackage{../present}
\usepackage{../stats}


% \usepackage{tikz}
% % \usetikzlibrary{arrows,backgrounds,snakes}
% \usepackage{pgfplots}
% \pgfplotsset{width=5cm}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=red} 

% \usepackage[english]{babel}
% or whatever
\usepackage{jh}

% \usepackage{pgfplots}
% \pgfplotsset{width=10cm,compat=1.9}

% \usepackage[latin1]{inputenc}
% or whatever


\title[Data] % (optional, use only with long paper titles)
{Data}

\author{J Hef{}feron}
\institute{
  Mathematics\\
  Saint Michael's College\\
  Colchester, VT\\[1ex]
  \texttt{jhefferon@smcvt.edu.edu}
}
\date{}

\subject{Data}
% This is only inserted into the PDF information catalog. Can be left
% out. 


%\setlength{\parskip}{1ex}



\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................

\begin{frame}
\frametitle{Vocabulary}

Here are six people and two dimensions.
\begin{center}\footnotesize
  \begin{tabular}{r|cc}
    \multicolumn{1}{c}{\ } 
          &Height &\multicolumn{1}{c}{In-state} \\
    \cline{2-3}
    John  &$65$   &T      \\
    Mary  &$68$   &F      \\    
    Sue   &$69$   &T      \\    
    Bob   &$65$   &T      \\    
    Dave  &$71$   &F      \\    
    Allie &$60$   &T      \\    
  \end{tabular}
\end{center}
There are six \alert{cases} 
(sometimes instead called \alert{subjects} or \alert{units}).
There are two \alert{random variables}.
Each entry in the table, that is, each piece of data, is an \alert{observation}.

A random variable is \alert{quantitative} 
if it makes sense to average it; otherwise
it is \alert{categorical}.
Above, the variable Height is quantitative, while 
In-state is categorical.

\smallskip
Warning:~a variable can be numbers without being quantitative.
If part of your data is  Area Code then that is categorical, 
because it doesn't make sense to average people's area codes.
\end{frame}



\begin{frame}
\frametitle{Data distribution}  
This is a database query for 
SMC students.
There is randomness,
but there are also patterns.
\begin{center}\footnotesize
  \begin{tabular}{rr|rr|rr}
    age &\multicolumn{1}{c}{num}
    &age &\multicolumn{1}{c}{num}
    &age &\multicolumn{1}{c}{num} \\ \hline
    -- &5   &27  &1  &42  &3  \\
    16 &1   &29  &1  &43  &1  \\
    17 &1   &30  &3  &44  &3  \\
    18 &342 &31  &3  &45  &1  \\
    19 &486 &32  &3  &46  &1  \\
    20 &469 &33  &1  &48  &1  \\
    21 &431 &34  &1  &49  &3  \\
    22 &196 &35  &1  &50  &1  \\
    23 &41  &38  &2  &52  &2  \\
    24 &18  &39  &2  &55  &1  \\
    25 &6   &40  &2  &120 &1  \\
    26 &4   &41  &2  
  \end{tabular}
\end{center}
The \alert{distribution} describes the pattern of variation in a variable.

This data has quality problems,
but data always has issues.
The question:~is the data good enough to draw conclusions on the
topic of interest?
\end{frame}




\begin{frame}
\frametitle{Data: be suspicious}

\begin{itemize}
\item \textbf{What is the data for?}
  Can you answer your questions from the data that you've gathered?

  \inlineexample{The IT support team user survey reports say that the most
    common complaint is the length of time it takes to resolve the issue.
    So your boss starts a push. 
    After a month the 
    response ticket database says the resolution time has fallen from
    36 to 12 hours.  Success?}

\pause\item \textbf{What cases are described?}
  How many?  
  How do you find them?
  In what ways are they alike, and in what ways are they different?

  \inlineexample{Researchers searching for autism clusters in hopes of 
  finding an environmental cause for the disorder have identified ten clusters 
  around the state. 
  But investigation shows that they are be centered on regional 
  child developmental services centers.}
\end{itemize}
\end{frame}


\begin{frame}
\begin{itemize}
\item \textbf{What are the variables?}
  How many are there?
  Can they be accurately measured?
  Is there reason to mistrust some? 

  \inlineexample{Student evaluation of classes at SMC and other places
    is a problem.
    One reason is a low response rate; are the people who respond different
    than the average person?
    Another reason is that people often give the same response to each
    question; for instance, students who mark low scores on other items 
    might continue that on ``Instructors
    return quizzes promptly'' even if the instructor 
    has a perfect record on that.
    And, in the end, we should ask what the numbers mean:~a study at the 
    Air Force Academy that had very good design found that 
    instructors who did the best on how their students did in later classes
    had significantly below average student evaluation averages.}
\end{itemize}
\end{frame}




% \begin{frame}
%   \frametitle{Data sets of one variable}
% We first look at one variable at a time. 
% We will later look at the relationship between 
% two variables, such as height and weight.

% With one variable we will display the pattern of variation, and summarize
% it.
% \end{frame}




% ===============================================
\section{Categorical data}


\begin{frame}{Categorical variables}
The \alert{proportion} in some category is the number of cases in that
category, divided by the total number of cases.
Denote the proportion for a population with~\alert{$p$} and the proportion for
a sample with \alert{$\hat{p}$}.

We always list proportions as decimals; we might say a 
baseball batter's chance of a hit is 
$0.213$ rather than $23.1\%$.
Basically, if we always do it the same way then there is less chance of
getting mixed up about what way to do it.

\smallskip
\exm
At St Michael's the population proportion of women is $p=0.55$.
We might take a sample of one hundred SMC students and report 
that the proportion of women in the sample is $\hat{p}=0.48$.

% \pause\bigskip
% In a table giving percentage data, 
% the numbers should add to $1.00$, that is, $100\%$.
% But sometimes they don't because of rounding.
% \begin{center} \small
%   \begin{tabular}{r|cc}
%    \multicolumn{1}{c}{\ }
%     &\textit{number} &\textit{percentage $\hat{p}$} \\
%     \cline{2-3}
%     \textit{left handed}   &$20$  &$.33$ \\
%     \textit{right handed}  &$20$  &$.33$ \\
%     \textit{neither}       &$20$  &$.33$ \\
%      \cline{2-3}
%     \multicolumn{1}{r}{\textit{total}}         &$60$  &$1.00$ 
%   \end{tabular}
% \end{center}


\end{frame}



\begin{frame}
\frametitle{Displaying categorical data: bar chart}
Categorical data charts come in two types.
Data lies on an \alert{ordinal scale} if the categories have a
natural order
\centergraphic[height=1in]{graph.png}
and on a \alert{nominal scale} otherwise.
\centergraphic[height=1in]{bar-chart.png}
\end{frame}


% \begin{frame}
% \frametitle{Displaying categorical data: pie charts}

% Pie charts show proportions.
% \centergraphic[height=0.9in]{pie_chart.png}
% Disadvantages are: pie charts take a lot of space,
% are not useful when there are many categories,
% and comparing across categories is hard when the 
% comparisons are close.

% Really, pie charts are not very good at displaying data.
% \end{frame}


\begin{frame}{Two categorical variables: two-way tables}
This shows the numbers of cases in a sample of twenty year olds. 
  \begin{center}
    \begin{tabular}{r|cc|r}
      \multicolumn{1}{c}{\ }
      &\textit{male}  &\multicolumn{1}{c}{\textit{female}} 
      &\textit{total}                                    \\
      \cline{2-4}
     \textit{smoker} &$8$  &$3$  &$11$  \\
     \textit{former} &$5$  &$3$  &$8$   \\
     \textit{never}  &$42$ &$39$ &$81$  \\
       \cline{2-4}
     \multicolumn{1}{r}{\textit{total}}        &$55$ &$45$ &$100$
    \end{tabular}
  \end{center}
\end{frame}




% \begin{frame}{Bar charts}
% Use bar charts to compare between variables, usually
% between categorical variables.
% This compares the average times visitors spent at each of a museum's
% exhibits.
% \begin{center}
%   \includegraphics[width=0.2\textwidth]{barchart.jpg}
% \end{center}
% Because the bars are equal widths, comparing the areas means
% comparing the heights.
% We can rearrange the items on the horizontal axis as we like; typically
% arranging them by height is best.

% Use bars not lines; readers take them to mean a left to right trend, 
% so this is wrong. 
% \centergraphic[height=0.30in]{bad_graph.png}
% \end{frame}





% ==============================================
\section{Quantitative data}


\begin{frame}
  \frametitle{Quantitative variable distribution}
Our database query for SMC students by age has a lot of data.
\begin{center}\footnotesize
  \begin{tabular}{rr|rr|rr}
    age &\multicolumn{1}{c}{num}
    &age &\multicolumn{1}{c}{num}
    &age &\multicolumn{1}{c}{num} \\ \hline
    -- &5   &27  &1  &42  &3  \\
    16 &1   &29  &1  &43  &1  \\
    17 &1   &30  &3  &44  &3  \\
    18 &342 &31  &3  &45  &1  \\
    19 &486 &32  &3  &46  &1  \\
    20 &469 &33  &1  &48  &1  \\
    21 &431 &34  &1  &49  &3  \\
    22 &196 &35  &1  &50  &1  \\
    23 &41  &38  &2  &52  &2  \\
    24 &18  &39  &2  &55  &1  \\
    25 &6   &40  &2  &120 &1  \\
    26 &4   &41  &2  
  \end{tabular}
\end{center}
A graphical summary can help.
\end{frame}

\begin{frame}
Here we have a bar for each age.
\begin{center}
  \includegraphics[width=2.5in]{asy/histogram03.pdf}    
\end{center}
The shape of a distribution can be one of these three: 
\alert{skewed to the right} (or \alert{right-skewed}, or 
\alert{positively skewed}) 
if it has a long right tail, or
\alert{skewed to the left}, 
or \alert{symmetric} if it has no pronounced skew.
A common distribution shape is \alert{bell-shaped},
which is a symmetric kind.

An \alert{outlier} is a value that is notably distinct from the others.
\end{frame}



\begin{frame}
\frametitle{Displaying quantitative data: Stem and leaf plots}

Sometimes we get numerical data and just want a first look.
For that we often group the numbers by common \alert{stems}.
For instance,  consider this data.
\begin{center}
  $44$ $46$ $47$ $49$ $63$ $64$ $66$ $68$ $68$ $72$ $72$ $75$ 
                            $76$ $81$ $84$ $88$ $106$
\end{center}
We can grouping together numbers in the $40$'s, the $50$'s, etc.
\pause
\begin{center}\small
\begin{tabular}{r|l}
 \multicolumn{1}{c}{\textit{stem}}  
   &\multicolumn{1}{c}{\textit{leaves}}  \\
 \hline
 $4$ &$4$ $6$ $7$ $9$ \\
 $5$ &  \\
 $6$ &$3$ $4$ $6$ $8$ $8$ \\
 $7$ &$2$ $2$ $5$ $6$ \\
 $8$ &$1$ $4$ $8$ \\
 $9$ & \\  
$10$ &$6$
\end{tabular}
\end{center}
Inside each stem line, the \alert{leaves}, the final digits, come in
ascending order, with repeat digits shown. 
Note that even if there are no leaves, we still write the stem.
\end{frame}

\begin{frame}
If there is a lot of data then
we \alert{split stems}, putting the leaves $0$-$4$ and $5$-$9$
on separate lines.
\begin{center}\small
\begin{tabular}{r|l}
 \hline
 $4$ &$4$ \\
 $4$ &$6$ $7$ $9$ \\
 $5$ &  \\
 $5$ &  \\
 $6$ &$3$ $4$ \\
 $6$ &$6$ $8$ $8$ \\
 $7$ &$2$ $2$  \\
 $7$ &$5$ $6$ \\
 $8$ &$1$ \\
 $8$ &$4$ $8$ \\
 $9$ & \\  
 $9$ & \\  
$10$ &  \\
$10$ &$6$
\end{tabular}
\end{center}
Again, stem plots are most useful for getting a quick first look at the
data.
\end{frame}


% \begin{frame}{Histograms}
% Here is the SMC student age data.
% In the left there is one bar for each age.
% On the right we've grouped together all cases less than $18$
% and also all cases above~$29$.
% \begin{center}
%   \vcenteredhbox{\includegraphics[height=1.25in]{asy/histogram03.pdf}}    
%   \quad
%   \vcenteredhbox{\includegraphics[height=1in]{asy/histogram04.pdf}}    
% \end{center}
% \end{frame}
  

% \begin{frame}
% \frametitle{What if the data comes in uneven bins?}
% Data can come in intervals of differing lengths.
% \begin{center}\small
% \begin{tabular}{r|r}
%   \multicolumn{1}{r}{\textit{price}}  &\textit{number available}    \\ 
%   \hline
%   $0$-$0.99$    &$10$    \\
%   $1$-$1.99$    &$10$    \\
%   $2$-$2.99$  &$10$    \\
%   $3$-$3.99$  &$10$    \\
%   $4$-$9.99$  &$10$    
% \end{tabular} 
% \end{center}
% How many are in the range $4$-$4.99$?
% $5$-$5.99$?

% \pause
% To correctly show the distribution we have to adjust the bars.
% \begin{center}\small
% \begin{tabular}{r|rr}
%   \multicolumn{1}{r}{\textit{price}}  &\textit{num available}  &\textit{num/dollar}  \\ 
%   \hline
%   $0$-$0.99$    &$10$     &$10$  \\
%   $1$-$1.99$    &$10$     &$10$ \\
%   $2$-$2.99$  &$10$       &$10$ \\
%   $3$-$3.99$  &$10$       &$10$ \\
%   $4$-$9.99$  &$10$       &$10/6=1.67$ 
% \end{tabular} 
% \end{center}
% \end{frame}


% \begin{frame}{Drawing a histogram}
% \begin{enumerate}
% \item Draw an $x$-axis with the intervals having proper width.
% \item Erect rectangles.
% Each rectangle must have an \emph{area} proportional to the frequency 
% of the interval.
% This makes the height above
% each dollar-wide interval be the relative chance of falling in that 
% interval.
% \begin{center}\small
% \begin{tabular}{r|rr}
%   \multicolumn{1}{r}{\textit{price}}  
%     &\multicolumn{1}{c}{\textit{number}}  
%     &\multicolumn{1}{c}{\textit{number/dollar}}  \\ 
%   \hline
%   $0$-$0.99$    &$10$     &$10$  \\
%   $1$-$1.99$    &$10$     &$10$ \\
%   $2$-$2.99$  &$10$       &$10$ \\
%   $3$-$3.99$  &$10$       &$10$ \\
%   $4$-$9.99$  &$10$       &$10/6=1.67$ 
% \end{tabular} 
% \qquad
% \vcenteredhbox{\includegraphics[height=1.5in]{asy/histogram02.pdf}}
% \end{center}
% \end{enumerate}
% \end{frame}

% \begin{frame}
% \exm
% This is Census Bureau data on time to travel to work.
% \begin{center}\small
% \begin{tabular}{rrr}
%   time       &number   &$\text{number}/\text{minute}$  \\ \hline
%   $0$-$4$    &$4180$   &$836$  \\
%   $5$-$9$    &$13687$  &$2737$  \\
%   $10$-$14$  &$18618$  &$3723$  \\
%   $15$-$19$  &$19634$  &$3926$  \\
%   $20$-$24$  &$17981$  &$3596$  \\
%   $25$-$29$  &$7190$   &$1438$  \\
%   $30$-$34$  &$16369$  &$3273$  \\
%   $35$-$39$  &$3212$   &$642$  \\
%   $40$-$44$  &$4122$   &$824$  \\
%   $45$-$59$  &$9200$   &$613$  \\
%   $60$-$89$  &$6461$   &$215$  \\
%   $90+$      &$3435$   &$57$ 
% \end{tabular} 
% \end{center}
% \pause
% \centergraphic[width=3.5in]{asy/histogram01.pdf}
% \end{frame}


% \begin{frame}
%   \exm
%   \begin{center} \small
%     \begin{tabular}{r|rr}
%       Income         &Percent &$\text{Percent}/\text{Thousand}$\\  \hline
%       $0$-$999$      &$1$  &$1.0$  \\
%       $1000$-$1999$  &$2$  &$2.0$ \\
%       $2000$-$2999$  &$3$  &$3.0$ \\
%       $3000$-$3999$  &$4$  &$4.0$ \\
%       $4000$-$4999$  &$5$  &$5.0$ \\
%       $5000$-$5999$  &$5$  &$5.0$ \\
%       $6000$-$6999$  &$5$  &$5.0$ \\
%       $7000$-$9999$  &$15$ &$5.0$ \\
%       $10000$-$14999$  &$26$  &$5.2$ \\
%       $15000$-$24999$  &$26$  &$2.6$ \\
%       $25999$-$49999$  &$8$  &$0.3$ \\
%       $50000+$  &$1$   &--
%     \end{tabular}  \\[1ex]
%     Census data, 1990
%   \end{center}\pause
% \begin{center}
%   \includegraphics[width=4in]{asy/histogram00.pdf}    
% \end{center}
% \end{frame}


% \begin{frame}
% \frametitle{Histogram homework}
% In addition to the book exercises, 
% draw the histogram for this.
%   \begin{center} \small
%     \begin{tabular}{r|rr}
%       Grade completed   &Percent \\  \hline
%       $0$-$4.9$  &$2$   \\
%       $5$-$7.9$  &$4$  \\
%       $8$-$8.9$  &$4$  \\
%       $9$-$11.9$  &$11$ \\
%       $12$-$12.9$  &$39$  \\
%       $13$-$15.9$  &$18$  \\
%       $16+$        &$21$ 
%     \end{tabular}  \\[1ex]
%     Census data, 1991
%   \end{center}
% \end{frame}



% ====================================================

\section{Center and spread}
\begin{frame}{Measures of center for single quantative variable}
The \alert{mean} is the average.
Denote the mean of a population as $\mu$, read ``mu''.
Denote the mean of a sample as $\bar{x}$, read ``x bar''.

The \alert{median} is the number in the middle.
% : at least half the 
% data values are below that number and at least half are above.
% (If more than one such number exists then there will be an interval of
% such numbers and the median is the midpoint.)
To find the median, write the numbers in
ascending order and find the middle one.
If it is between two numbers, average them.

\exm
This data set has seven numbers.
\begin{center}
  1,2,4,5,6,7,7
\end{center}
The median is the fourth number, the 5.
\end{frame}
\begin{frame}
\exm
\begin{center} \small
  \begin{tabular}{ccc}
    \textit{data set}  &\textit{how many?}  &\textit{median}  \\ \hline
      1,3,5,6,9,11,11  &7   &6  \\
      1,3,5,6,7,9,11,11 &8   &6.5 (halfway between 4th and 5th)  \\
      1,3,4,4,4,5,6,8   &8   &4 (halfway between 4th and 5th)  
  \end{tabular}
\end{center}

\pause
\smallskip
When to use mean and when to use median?
The mean is easy to calculate and familiar.
What advantage does the median have?
\exm
Here are the grades on a math exam.
\begin{center}\small
\begin{tabular}{cccccccccc}
  Asa   &Barb &Cal  &Dora &Ed  &Francis \\ \hline
  $20$  &$75$  &$80$ &$85$ &$90$ &$95$    
\end{tabular}
\end{center}
The median is $(80+85)/2=82.5$.
The mean is $74.2$.

The mean is strongly pulled in the direction of extreme values.
But the median is \alert{resistant} to extreme values.
\end{frame}


\begin{frame} % http://www.slate.com/articles/life/weddings/2013/06/average_wedding_cost_published_numbers_on_the_price_of_a_wedding_are_totally.html
  % \title{Let the fleecing begin}
\exm
If you ask Americans, ``How many books did you read last year'' then the
mean is $10$.
But the median is~$4$. 
A small number of people read a lot of books, which pulls the mean way 
to the right.

\smallskip
\exm
In 2012 the average wedding cost was \$27,427
while the median was \$18,086.
\pause
In Manhattan, the average is \$76,687,
the median is \$55,104.
And in Alaska, where the average is \$15,504, the median is \$8,440.
\end{frame}


\begin{frame}{Why is the spreadedness of data important?}
To describe data we need to say more than just the center.
We need to describe how the data is distributed, and in 
particular we need to say how spread out it is.
Is it tightly clustered, or all over?

I have two ways to work.
Going by Five Corners averages 17 minutes.
Going by the connector averages 18 minutes.
But I go by the connector.

\pause
The reason is that the Five Corners data set is more
spread out (one day it took me 45 minutes).
% \begin{center} 
% \pgfplotsset{no markers}
% \begin{tikzpicture}
% \begin{axis}[
% ymin=0,
% xmin=14.5,xmax=19.5,
% %xlabel=$x$,
% %ylabel={$N(x)$},
% domain=14.5:19.5
% ]
% \addplot {((2*3.14159)^(-1/2))*(2.718)^(-1*((x-17)^2)/2)};
% \end{axis}
% \end{tikzpicture}\hspace*{3em}
% \begin{tikzpicture}
% \begin{axis}[
% ymin=0,
% xmin=14.5,xmax=19.5,
% %xlabel=$x$,
% %ylabel={$N(x)$},
% domain=14.5:19.5
% ]
% \addplot {((2*3.14159*(.5)^2)^(-1/2))*(2.718)^(-1*((x-17)^2)/(2*(.5)^2))};
% \end{axis}
% \end{tikzpicture}
% \end{center}

\pause\medskip
There are two ways to measure center and two ways to measure spread.
When measuring center with the mean, measure spreadedness
with the \alert{standard deviation}.
When measuring center with the median, measure spreadedness
with the \alert{five-number summary}.
We'll describe them next.
\end{frame}


\begin{frame}
\frametitle{Standard Deviation}
These two people shoot ten basketball free throws, repeating five times.
\begin{center}
  \begin{tabular}{r|rrrrr}
    Able  &$4$ &$5$ &$6$ &$7$ &$8$  \\ \hline
    Baker &$2$ &$4$ &$6$ &$8$ &$10$
  \end{tabular}
\end{center}
Both have a mean of $\bar{x}=6$.
But Baker's numbers are further from the mean.
\begin{center}
  \begin{tabular}{r|rrrrr}
    Able, distance from $\bar{x}=6$   &$2$ &$1$ &$0$ &$1$ &$2$  \\ \hline
    Baker, distance from $\bar{x}=6$  &$4$ &$2$ &$0$ &$2$ &$4$
  \end{tabular}
\end{center}
\pause
The \alert{standard deviation} is, roughly,
the average distance from the mean.
This is the formula for the standard deviation of a sample.
\begin{equation*}
  \alert{s}=\sqrt{\frac{\sum (x_i-\bar{x})^2}{n-1}}
  =\sqrt{\frac{(x_0-\bar{x})^2+(x_1-\bar{x})^2+\cdots}{n-1}}
\end{equation*}
\end{frame}


\begin{frame}{Points about standard deviation}
\begin{itemize}
\item It is greater than or equal to $0$.

\item The $n-1$ is the \alert{degrees of freedom}.
  (The standard deviation of a population uses $n$.)

\item Denote the standard deviation of a sample as \alert{$s$} while the 
  standard deviation of a population is \alert{$\sigma$} (read ``sigma'').

\item The square of the standard deviation is the \alert{variance}.
    It is useful in the theory.
    But we use standard deviation because it has the correct units:
    if the data is in inches then the standard deviation is also in inches
    (the variance is in inches squared).
\end{itemize}
%% The \alert{calculational formula} is algebraically equivalent.
%% \begin{equation*}
%%     s=\sqrt{\frac{\sum x_i^2 - (1/n)(\sum x_i)^2}{n-1}}
%% \end{equation*}
\end{frame}




\begin{frame}{Using \textit{StatKey} to find standard deviation}
Here is the number of traffic citations in Beaufort County,
South Carolina, in a recent year.
\begin{center} \small
  \begin{tabular}{*{6}{c}}
   Jan &Feb &Mar &Apr &May &Jun    \\
   \hline
   19 &17   &22  &18  &28  &34 
  \end{tabular}                                       \\
  \qquad\begin{tabular}{*{6}{c}}
   Jul &Aug &Sep &Oct &Nov &Dec   \\
   \hline
   45 &39 &38 &44 &34 &10
  \end{tabular}
\end{center}
\begin{center}
  \includegraphics[height=0.5\textheight]{citations.png}
\end{center}
  
\end{frame}
\begin{frame}
\begin{center}
  \includegraphics[height=0.5\textheight]{citations2.png}
\end{center}
The mean is $\bar{x}=29$ and the standard deviation is
$s=11.631$
\end{frame}




\begin{frame}{Finding standard deviation on a Normal curve}
Here is \textit{StatKey}'s picture of a Normal curve
\begin{center}
  \includegraphics[height=0.6\textheight]{normal_curve.png}
\end{center}
In the middle the curve is concave down, umbrella-shaped.
On both edges it is concave up, bowl-shaped.
The transition happens one standard deviation from the mean. 
\end{frame}



\begin{frame}{Standard units}
Locate data in a distribution by \alert{standard deviation units}
\begin{equation*}
  \frac{\text{data value}-\text{mean}}{\text{standard deviation}}
  \qquad
  z=\frac{x-\mu}{\sigma}
\end{equation*}

\exm
The SAT Math test has $\mu=500$ and $\sigma=100$.
A person who got $470$ is
\begin{equation*}
  z=\frac{x-\mu}{\sigma}=\frac{470-500}{100}=-0.30
\end{equation*}
standard deviation units from the mean.
We say
``$z=\mu-0.30\sigma$'' or maybe ``their \alert{$z$-score} is $-0.30$.''

\exm
IQ tests have a mean of $\mu=100$ and a standard deviation
of $\sigma=15$.
A person with a raw score of $120$ is located at 
$(120-100)/15=1.33$~standard deviation units from the mean.  
We write $z=\mu+1.33\sigma$,
and say their $z$-score is $1.33$.
\end{frame}


\begin{frame}{Two sigma}
  Suppose that you give a reading test to eight grade students.
  Some students do a little worse than average, some do even worse than that.
  At what point do you start considering a student's performance to be
  getting far out from the average, so that they are of special concern?

  \pause
    A rule of thumb for when a data point changes from 
    typical to atypical is the two standard deviation
    unit mark.
    % (This is based on 
    % Chebyshev's Theorem, that no matter how the data is distributed,
    % the proportion of data within
    % $k$ standard deviation units of the mean is at least $1-(1/k^2)$.
    % With $k=2$ we have $1-(1/2^2)=0.75$, seventy  five percent, 
    % of the data is
    % within two standard deviations of the mean.)
  % \pause
  %   For some distibutions the proportion is even higher.
    If the distribution is bell-shaped and approximately symmetric
    then about $95$\% of the data is within two standard 
    deviation units of the mean.

\exm
Elderly patients take a balance test with mean $\mu=40$ and standard deviation
$\sigma=8$.
One patient gets $x=27$.
Is that in the special concern range?
\begin{equation*}
  z=\frac{27-40}{8}=-1.625
\end{equation*}
That's below average, but it is not below $\mu-2\sigma=40-2\cdot 8=24$.
\end{frame}



\begin{frame}
  \frametitle{The median: summarizing data}
Here are the grades on a math exam.
The median is $(61+73)/2=67$.
\begin{center}\small
\begin{tabular}{cccccccccc}
  Asa   &Barb &Cal  &Dora &Ed   &Francis &Greg &Hortia &Ian  &Jane  \\ \hline
  $34$  &$42$ &$47$ &$59$ &$61$ &$73$    &$75$ &$86$   &$88$ &$93$   
\end{tabular}
\end{center}
\pause
A $p$-th \alert{percentile} value is a number that puts at least 
$p$ percent of the data at or below that number.

There are various ways 
used to calculate percentiles.
They all give the same values for large
data sets.
This method is common and easy to use 
by hand:~the \alert{percentile} of an observation in a data set is the
percentage of 
pieces of data with smaller value plus half the percentage of pieces of data
with equal value.

\exm
Cal's score is at the $25$-th percentile. 
\begin{equation*}
  \frac{2}{10}+\frac{1}{2}\cdot\frac{1}{10}=0.25
\end{equation*}
The median is at the $50$-th percentile.
\end{frame}



\begin{frame}{Speed limits}
Every year, traffic engineers review the speed limit on thousands of 
stretches of road and highway. 
Most are reviewed by a member of a state Department of Transportation 
along with a member of the state police.
The team follows the national standard of  
setting the speed limit at the 
$85$-th percentile of the speed that drivers actually use.

So they use video to see at what speed $85\%$ of drivers would
be OK but $15\%$ would be speeding, and that's the speed limit. 
\end{frame}


\begin{frame}{Quartiles}
For any data set 
the \alert{first quartile~$Q_1$} is a $25$-th percentile number,
and the \alert{third quartile~$Q_3$} is a $75$-th percentile number.

\pause
If we are summarizing data using the median
then we describe the spreadedness by giving 
the \alert{five number summary}: the minimum, and $Q_1$, and the median, 
and $Q_3$, and the maximum.

\pause
The \alert{interquartile range~IQR} is $Q_3-Q_1$.
A common
rule of thumb test for an outlier is that it is more than 
$3/2$ times the IQR above $Q_3$, or below $Q_1$.
\begin{itemize}
\item It a data point $x$ is less than $Q_1-1.5\cdot \text{IQR}$
  then it is a low outlier.
\item It a data point $x$ is more than $Q_3+1.5\cdot \text{IQR}$
  then it is a high outlier.
\end{itemize}
\end{frame}


\begin{frame}{Finding the five-number summary}
\exm
Here are sales numbers for the district salespeople in this quarter.
\begin{center}
  15, 21, 13, 19, 15, 20, 22, 28, 23, 14, 17, 24
\end{center}
\begin{center}
  \includegraphics[height=0.5\textheight]{fivenumber.png}
\end{center}
\end{frame}
\begin{frame}
\begin{center}
  \includegraphics[height=0.5\textheight]{fivenumber2.png}
\end{center}  
The five number summary is 13, 15, 19.5, 22.5, 28.

\pause
Also, the interquartile range is $\text{IQR}=22.5-15=7.5$.
To use the rule of thumb to check for low outliers we would look for data
points below $Q_1-(1.5)\cdot \text{IQR}=15-(1.5)(7.5)=3.75$.
There is no data point less than that so we say there are no low outliers.
To check for high outliers, look for data points above 
$Q_3+(1.5)\cdot\text{IQR}=22.5+(1.5)(7.5)=33.75$, but there aren't any. 

\end{frame}



\begin{frame}{Calculating the quartiles}
\noindent \textit{Comment:}
We will use \textit{StatKey} to find quartiles but here are the rules. 
Given a data set with $n$~observations, 
divide the data set into halves.
As to whether to include or exclude the median in the halves:
if $n$~is odd then include or exclude it so as to make each half have
an odd number of elements.
(If $n$~is even then leave the median out.)
Then $Q_1$ is the median of the lower half and $Q_3$ is the median of the 
upper.

% \exm For the math exam, there are $n=10$ numbers so we get a 
% bottom half of $34, 42, 47, 59, 61$ and a top half of
% $73,75,86,88,93$. 
% The five number summary is $34$, $47$, $67$, $86$, $93$.

% \exm
% Again, if $n$ is odd then include or exclude the median as needed to
% make the lower and upper halves have an odd number of observations.
% If $n$ is even, just split.
% \begin{center} \small
%   \begin{tabular}{cccc}
%     \textit{data}  &\textit{number obs} &$Q_1$ &$Q_3$ \\ \hline
%     1,2,3,4        &$n=4$                   &$1.5$ &$3.5$ \\
%     1,2,3,4,5      &5                   &$2$ &$4$ \\
%     1,2,3,4,5,6    &6                   &$2$ &$5$ \\
%     1,2,3,4,5,6,7  &7                   &$2$ &$6$ \\
%   \end{tabular}
% \end{center}
\end{frame}


% \begin{frame}{StatKey}
% Here is a screen shot of editing data on one quantitative variable.
% \begin{center}
%   \includegraphics[height=0.8\textheight]{statkey.png}
% \end{center}
% \end{frame}


\begin{frame}{Graphing the quartiles: Box plots}
Graph a $5$-number summary with a \alert{box plot}
(or \alert{box and whiskers plot}).

\exm The five-number summary of the number of pushups
that people in the class can do is $2,4,5,8,15$.
\begin{enumerate}
\item Draw the $x$-axis. Make it evenly spaced.
\vspace*{2ex}
\centergraphic{asy/boxplot02.pdf}
\item Fill in the bars at the five numbers, 
and put a box around the middle half.
\centergraphic{asy/boxplot00.pdf}
The point of the box plot is to graphically show
the uneven spacing, that the lower half of the data 
is compressed and the upper half
is stretched out.

\end{enumerate}
% So it is wrong to draw a boxplot by making it evenly spaced
% and then labelling the bars. 
% \centergraphic{asy/boxplot01.pdf}
\end{frame}

\begin{frame}{Side by side boxplots}
When we want to compare one categorical variable and one
quantative variable we may draw side-by-side box plots.
They must go on the same graph.

\exm
Here is the pushup data again, broken out by gender.
\centergraphic{asy/boxplot03.pdf}
  
\exm
Here are the boxplots of the ages of Oscar winners, by gender.
\vspace*{-1ex}
\centergraphic[height=0.25\textheight]{side-by-side.png}
\end{frame}

\end{document}

\begin{frame}
%\begin{center}\small
%\begin{tabular}{cccccccccc}
%  Asa   &Barb &Cal  &Dora &Ed   &Francis &Greg &Hortia &Ian  &Jane  \\ \hline
%  $34$  &$42$ &$47$ &$59$ &$61$ &$73$    &$75$ &$76$   &$88$ &$93$   
%\end{tabular}
%\end{center}
Graph the five number summary with a \alert{box plot}.
\vspace*{1.5ex}

% 34,47,67,76,93
\begin{tikzpicture}[thick]
    \filldraw[fill=green!20] (4.7,-0.25) rectangle (7.6,0.25);% draw the box
    \draw (6.7,-0.25) -- (6.7,.25);% draw the median
    \draw (7.6,0.0) -- (9.3,0.0);% draw right whisker
    \draw (3.4,0.0) -- (4.7,0.0);% draw left whisker
    \draw (3.4,-0.11) -- (3.4,0.11);% draw vertical tab
    \draw (9.3,-0.11) -- (9.3,0.11);% draw vertical tab
    % \node[below] at (2,0) {$\textsc{Q1}$};% label the hinge
    %\node[below] at (5,0) {$\textsc{Q3}$};% label the hinge
    %\filldraw[ball color=red!80,shading=ball] (4,0.5) circle
    %    (0.06cm) node[above]{$\bar{x}$};% the mean
    %\draw[<->] (2.3, -0.3) -- (4.7, -0.3)
    %    node[pos=0.5,below]{$\textsc{IQR}$}; % mark the IQR fences
    %\draw[<->] (2, -0.8) -- (0,-0.8)
    %    node[pos=0.5,below]{$\textsc{1.5*IQR}$}; % left inner fence
    %\draw[<->] (2,-1.4) -- (-2, -1.4)
    %    node[pos=0.5,below]{$\textsc{3*IQR}$};% left outer fence
    %\draw[<->] (5, -0.8) -- (8,-0.8)
    %    node[midway,below]{$\textsc{1.5*IQR}$}; % right inner fence
    %\draw[<->] (5,-1.4) -- (10, -1.4)
    %    node[pos=0.5,below]{$\textsc{3*IQR}$};% right outer fence
    %
    % \node[below] at (9,0.7) {$\textbf{*}$}; % mild outlier on the right
    % \node[below] at (-2.4,0.7) {$o$}; % extreme outlier on the left
    % Title
    %\draw (3,2) node[above,xshift=0.7cm]{$ \textsc{Box and Whisker
    %    Plot}$};%
    % Axis
    \draw (-.2,-0.5) -- (10.2,-0.5);
    % Note that the snaked line is drawn to 11.1 to force
    % TikZ to draw the final tick.
    \draw[snake=ticks,segment length=1cm] (0,-0.5) -- (10.1,-0.5);
    \node[below] at (0,-0.55) {\footnotesize $0$};
    \node[below] at (5,-0.55) {\footnotesize $50$};
    \node[below] at (10,-0.55) {\footnotesize $100$};
\end{tikzpicture}
\pause
Box plots are often drawn side by side.
\centergraphic[height=2.25in]{births.png}
\end{frame}


\begin{frame}
\frametitle{Median vs. Mean}
The mean is familiar and mathematically tractable.
The median is less influenced by outliers.

\pause
Here are grades on a class exam.
\begin{equation*}
  21,56,64,67,73,73,76,78,84,85,92,95,96
\end{equation*}
\begin{center}
\begin{tikzpicture}
\begin{axis}[ymin=0,ymax=3,
  xmin=0,xmax=100,
  xtick=\empty
  ]
\addplot+[ycomb] plot coordinates
{
(21,1) (56,1) (64,1) (67,1) (73,2) (76,1)
  (78,1) (84,1) (85,1) (92,1) (95,1) (96,1) 
};
\node[below] at (50,0) {\footnotesize $50$};
\node[below] at (100,0) {\footnotesize $100$};
\end{axis}
\end{tikzpicture}
\end{center}
The mean $\bar{x}\approx 73.85$ is pulled toward the extreme value
$21$ more than is the median $76$.
Distributions with a left tail (\alert{left-skewed}) will 
have a mean that is to the left of the median.
\end{frame}




\begin{frame}
\frametitle{Regression to the mean}
A variable that is extreme on its first measurement will tend to be 
closer to the centre of the distribution on a later measurement.

\pause
Suppose a class takes a $100$-item true/false test and all students choose 
randomly on all questions. 
The mean would be close to $50$. 
But some students score well above $50$ and if we take only the top  
$10\%$ and give them a second test then the mean score would again be 
close to $50$. 
The students have \alert{regressed to the mean}.

\pause
In practice many circumstaces involve some luck, and so will show some
regression to the mean.
One example is taking the top $10\%$ of scores on a standardized test
in a high school.
Give them the test again and you'll see some regression to the mean.
\end{frame}


\end{document}




