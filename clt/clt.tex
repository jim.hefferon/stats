\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
% \usepackage{present,jh}
\usepackage{../../present}
\usepackage{../stats}
\usepackage{listings}
\lstset{language=Python,
        basicstyle=\small\ttfamily}

\usepackage{pgfplots}
\pgfplotsset{width=5cm}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=red} 

% \usepackage[english]{babel}
% or whatever

% \usepackage[latin1]{inputenc}
% or whatever


\title[Central Limit Theorem] % (optional, use only with long paper titles)
{Central Limit Theorem}
\subtitle{}
\author{J Hef{}feron}
\institute{
  Mathematics\\
  Saint Michael's College\\
  Colchester, VT\\[1ex]
  \texttt{jhefferon@smcvt.edu.edu}
}
\date{}

\subject{Central Limit Theorem}
% This is only inserted into the PDF information catalog. Can be left
% out. 


%\setlength{\parskip}{1ex}



\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................

\begin{frame}
\frametitle{Sampling distribution}

We draw a number of samples of size $n=5$.
For every sample we report the mean $\bar{x}$. 

\pause
This is the underlying distribution.
\begin{center}
\begin{tikzpicture}
\begin{axis}
\addplot+[ybar] plot coordinates
{(1,1) (2,2) (3,3) (4,4) (5,5)};
\end{axis}
\end{tikzpicture}
\end{center}
\end{frame}



\begin{frame}
\frametitle{Sampling distributions}
We expect statistics to vary from sample to sample.

\pause The \alert{sampling distribution}
is the distribution of a statistic over all possible samples.
We will focus on the distribution of $\bar{x}$'s.  

\pause
To explore sample distributions 
we can use \alert{simulation}, including pulling numbers from a hat or
computer experiments.

\begin{itemize}
\pause\item
An \alert{unbiased statistic} is one whose mean value equals the true
value of the parameter.
(For instance, we use $n-1$ in the denominator of the definition of $s$
to make it an unbiased statistic.)

\pause\item
If the
population is $100$~times or more bigger than 
the size of the sample then the variability of the sampling distribution
does not change significantly with the sample size.
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Simulation}
We can draw samples from this continuous distribution
(known as the exponential distribution with $\lambda=1$).
\begin{center}
\pgfplotsset{no markers}
\begin{tikzpicture}
\begin{axis}[domain=0:10,
xlabel=$x$,
ylabel={$e^{-x}$}
]
% use TeX as calculator:
\addplot {exp(-x)}; % geometric dist, lambda=1
\end{axis}
\end{tikzpicture}
\end{center} 
\end{frame}



\begin{frame}[fragile]
\frametitle{Simulation}
\begin{lstlisting}
import math, random

N=10  # sample size
NUMBER_SAMPLES=50

def mean(lst):
    return sum(lst)/len(lst)

def generate_sample():
    lst=[]
    for i in range(N):
        lst.append(-math.log(random.random()))
    return mean(lst)

for i in range(NUMBER_SAMPLES):
    print generate_sample()
\end{lstlisting}
\end{frame}



\begin{frame}[fragile]
\noindent Here are the first five samples.
\begin{lstlisting}
1.68 2.30 0.70 0.66 2.15 1.50 2.42 1.04 0.25 0.51
1.82 1.62 0.27 1.73 0.32 2.71 1.31 6.19 1.59 0.67
0.04 0.34 0.78 0.22 0.08 0.18 3.46 1.10 1.06 4.14
1.58 0.03 1.80 0.67 0.45 0.18 0.39 2.43 0.80 1.59
1.40 0.65 0.58 1.68 0.45 2.74 1.09 0.09 3.33 0.68
\end{lstlisting}
\pause
Here are the first ten sample averages.
\begin{equation*}
1.32,\,
1.82,\,
1.14,\,
0.99,\,
1.27,\,
2.06,\,
1.06,\,
0.75,\,
1.09,\,
\ldots
\end{equation*}
% >  z=read.table(file='geometric_sample.averages',header=T)
% > png(file='geometric_sample.png')
% > plot(z,pch=20,xlab='',ylab='')
% > dev.off()
\centergraphic[width=1.75in]{geometric_sample.png}  
\end{frame}


\begin{frame}
\noindent Notes.
\begin{itemize}
  \item Each line consists of $N=10$ observations.
    These are distributed as the underlying distribution
    (the exponential distribution).
  \pause\item For each line sample there is one average $\bar{x}$.
  \pause\item The sample observations vary a bit 
     but the sample averages vary much less.
     That is, the sample averages are distributed differently 
     than the underlying distribution.
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Central Limit Theorem}

In any population, no matter the distribution (Normal or not), the distribution
of the samples must be Normal.
\pause
Specifically, where the sample size $n$ is large, 
the $\bar{x}$ are distributed Normally
with mean $\mu$ and standard deviation $\sigma/\sqrt{n}$.  
\end{frame}



\begin{frame}
\frametitle{Examples}
\begin{itemize}
\item 
Suppose that if we asked every person in the county
to rate the
president one to ten
then it would show an
average of $7.1$ with a standard deviation of $0.6$.
We will each go out and ask $n=25$ people, and report our $\bar{x}$'s. 
How will our $\bar{x}$'s be distributed?

\pause
\item Suppose that, if we could take a census, the first year class 
would score this way on the Mathematics readiness test: mean $75.4$
standard deviation $6.9$.
Instead of taking a census, we take a sample of size $15$ and report 
the average $\bar{x}$.
How are these sample averages distributed?
What is the chance of a sample average of $80$?
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{CLT in everyday life}

The Central Limit Theorem appears in popular culture.  

\pause
\textit{The law of large numbers:} after a large number of 
observations, the average will be pretty much the expected value,
and with more observations the average becomes even more
average.

This is correct but often misinterpreted.
\begin{itemize}
\pause\item Hot hand
\pause\item Due
\pause\item Pyramid power, horoscopes, other crap.
\end{itemize}

\end{frame}

\end{document}




