import math, random

N=10  # sample size
NUMBER_SAMPLES=50

def mean(lst):
    return sum(lst)/len(lst)

def generate_sample():
    lst=[]
    for i in range(N):
        lst.append(-math.log(random.random()))
    print " ".join(["%0.02f" % f for f in lst])
    return mean(lst)

f=open('geometric_sample.averages','w')
f.write('averages')
for i in range(NUMBER_SAMPLES):
    f.write("%0.02f\n" % generate_sample())
f.close()
