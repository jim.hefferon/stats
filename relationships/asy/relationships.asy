// histogram.asy
//  Draw histograms
import settings;
settings.outformat="pdf";
settings.render=0;

// unitsize(0.1cm);

include "/usr/local/share/asymptote/jh.asy";
// picture size
import stats;
include graph;

real height = 4cm;  // height of each picture


// ============== 
int picnum = 0;
picture p;
size(p,3*height,height,IgnoreAspect);

real[] avg={244, 250, 252, 239, 252, 242, 246, 272};
real[] place={1, 2, 3, 4, 5, 6, 7, 8};
real[] tabhi;
for(int i=0; i < avg.length; ++i)
  dot(p,(avg[i],place[i]));
xaxis(p,"Batting average",
      axis=YZero,
      xmin=225,xmax=280,
      RightTicks(Step=10,step=5),above=true); 
yaxis(p,"Place in league",axis=XEquals(225),
       ymin=0,ymax=10,
       Ticks(Step=5,step=1,
             pTick=blue, ptick=linetype("4 4")+grey,
             extend=false));

shipout(format("relationships%02d",picnum),p,format="pdf");


// ============== 
int picnum = 1;
picture p;
size(p,3*height,height,IgnoreAspect);

real[] avg={3.18, 3.72, 3.60, 2.89, 4.24, 4.05, 4.06, 5.45};
real[] place={1, 2, 3, 4, 5, 6, 7, 8};
real[] tabhi;
for(int i=0; i < avg.length; ++i)
  dot(p,(avg[i],place[i]));
// size(p,10cm,5cm,point(SW),point(NE));
xaxis(p,"ERA",axis=YZero,
      xmin=2.5,xmax=5.5,
      RightTicks(Step=0.5,step=0.25),above=true); 
yaxis(p,"Place in league",Left,
      ymin=0,ymax=10,
       Ticks(Step=5,step=1,
             pTick=blue, ptick=linetype("4 4")+grey,
             extend=false));

shipout(format("relationships%02d",picnum),p,format="pdf");


// // ============== 
// int picnum = 1;
// picture p;
// size(p,0,height);

// // Tableau des valeurs définissant les classes
// real[] tabxi={0,5,10,15,20,25,30,35,40,45,60,90,91};
// // Tableau des effectifs (ou fréquences) des classes
// real[] tabni={4180,13687,18618,19634,17981,7190,16369,3212,4122,9200,6461,3435};
// // Calcul des hauteurs
// real[] tabhi;
// for(int i=0; i < tabni.length; ++i)
//   tabhi[i]=tabni[i]/(300*(tabxi[i+1]-tabxi[i])); 
// // Tracé de l'histogramme
// histogram(p,tabxi,tabhi,low=0,bars=true);
// xaxis(p,"Minutes",Bottom,
//       RightTicks(Step=10,step=5),above=true); 
// yaxis(p,"Number/minute",Left,
//        Ticks(Step=5,step=5, 
//              pTick=blue, ptick=linetype("4 4")+grey,
//              extend=false));

// shipout(format("histogram%02d",picnum),p,format="pdf");


// // ============== 
// int picnum = 2;
// picture p;
// size(p,0,height);


// // Tableau des valeurs définissant les classes
// real[] tabxi={0,1,2,3,4,9};
// // Tableau des effectifs (ou fréquences) des classes
// real[] tabni={10,10,10,10,10};
// // Calcul des hauteurs
// real[] tabhi;
// for(int i=0; i < tabni.length; ++i)
//   tabhi[i]=tabni[i]/(tabxi[i+1]-tabxi[i]); 
// // Tracé de l'histogramme
// histogram(p,tabxi,tabhi,low=0,bars=true);
// xaxis(p,"$x$ quantity",Bottom,
//       RightTicks(Step=10,step=1),above=true); 
// yaxis(p,"$y$ quantity/$x$ quantity",Left,
//        Ticks(Step=5,step=1, 
//              pTick=blue, ptick=linetype("4 4")+grey,
//              extend=false));

// shipout(format("histogram%02d",picnum),p,format="pdf");
