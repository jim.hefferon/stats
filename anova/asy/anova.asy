// anova.asy
//  Draw anova graphics, including box plots
import settings;
settings.outformat="pdf";
settings.render=0;

// unitsize(0.1cm);

include "/usr/local/share/asymptote/jh.asy";
// picture size
import stats;
include graph;

real height = 4cm;  // height of each picture

void boxplot(picture p, real[] fivenumbers, real boxheight=5pt, real whiskerbarwidth=3pt, real barwidth=4pt, pen bppen=defaultpen) {
  path whiskerbar=(0,-0.5*whiskerbarwidth)--(0,0.5*whiskerbarwidth);
  path bar=(0,-0.5*barwidth)--(0,0.5*barwidth);
  pair min_pt=(fivenumbers[0],boxheight);
  pair q1_pt=(fivenumbers[1],boxheight);
  pair med_pt=(fivenumbers[2],boxheight);
  pair q3_pt=(fivenumbers[3],boxheight);
  pair max_pt=(fivenumbers[4],boxheight);
  draw(p,shift(min_pt)*whiskerbar); 
  draw(p,shift(q1_pt)*bar); 
  draw(p,shift(med_pt)*bar); 
  draw(p,shift(q3_pt)*bar); 
  draw(p,shift(max_pt)*whiskerbar); 
  // make the connections
  draw(p,min_pt--q1_pt,bppen); // whiskers
  draw(p,q3_pt--max_pt,bppen);
  draw(p,shift(0,-0.5*barwidth)*(q1_pt--q3_pt),bppen);
  draw(p,shift(0,0.5*barwidth)*(q1_pt--q3_pt),bppen);
}


// ============== 
int picnum = 0;
picture p;
size(p,3cm,0);
// scale(p,Linear(1),Linear(0.5));

real[] fivenumbers_t={3,5,6.5,7,8};
real[] fivenumbers_m={3.5,5.5,6,7,7.5};
real[] fivenumbers_b={2.2,4.5,5.75,6.5,7.75};
boxplot(p,fivenumbers_t,
	boxheight=6pt, whiskerbarwidth=0.75pt,barwidth=1pt);
boxplot(p,fivenumbers_m,
	boxheight=4pt, whiskerbarwidth=0.75pt,barwidth=1pt);
boxplot(p,fivenumbers_b,
	boxheight=2pt, whiskerbarwidth=0.75pt,barwidth=1pt);
xaxis(p,"Data set $A$",YZero,
      xmin=0,xmax=10,
      RightTicks(Step=5,step=1),above=true); 
yaxis(p,"",XZero,
      ymin=0,ymax=6.5);
ytick(p,"3",(0,6));
ytick(p,"2",(0,4));
ytick(p,"1",(0,2));
shipout(format("anova%02d",picnum),p,format="pdf");


// ============== Data set B
int picnum = picnum+1;
picture p;
size(p,3cm,0);

real[] fivenumbers_t={6,6.25,6.5,6.6,6.75};
real[] fivenumbers_m={4.5,5,5.25,5.5,6};
real[] fivenumbers_b={7,7.25,7.5,7.8,8};
boxplot(p,fivenumbers_t,
	boxheight=6pt, whiskerbarwidth=0.75pt,barwidth=1pt);
boxplot(p,fivenumbers_m,
	boxheight=4pt, whiskerbarwidth=0.75pt,barwidth=1pt);
boxplot(p,fivenumbers_b,
	boxheight=2pt, whiskerbarwidth=0.75pt,barwidth=1pt);
xaxis(p,"Data set $B$",YZero,
      xmin=0,xmax=10,
      RightTicks(Step=5,step=1),above=true); 
yaxis(p,"",XZero,
      ymin=0,ymax=6.5);
ytick(p,"3",(0,6));
ytick(p,"2",(0,4));
ytick(p,"1",(0,2));
shipout(format("anova%02d",picnum),p,format="pdf");




// ============== Data set C
int picnum = picnum+1;
picture p;
size(p,3cm,0);

real[] fivenumbers_t={4,4.5,5.5,6.5,6.75};
real[] fivenumbers_m={1.5,2.5,3.25,3.75,4.25};
real[] fivenumbers_b={6.5,7,8.5,9,9.5};
boxplot(p,fivenumbers_t,
	boxheight=6pt, whiskerbarwidth=0.75pt,barwidth=1pt);
boxplot(p,fivenumbers_m,
	boxheight=4pt, whiskerbarwidth=0.75pt,barwidth=1pt);
boxplot(p,fivenumbers_b,
	boxheight=2pt, whiskerbarwidth=0.75pt,barwidth=1pt);
xaxis(p,"Data set $C$",YZero,
      xmin=0,xmax=10,
      RightTicks(Step=5,step=1),above=true); 
yaxis(p,"",XZero,
      ymin=0,ymax=6.5);
ytick(p,"3",(0,6));
ytick(p,"2",(0,4));
ytick(p,"1",(0,2));
shipout(format("anova%02d",picnum),p,format="pdf");



// ============== Dosage levels
int picnum = picnum+1;
picture p;
size(p,3cm,0);

// Data: 
// 9,8,7,8,8,9,8
// 7,6,6,7,8,7,6
// 4,3,2,3,4,3,2

real[] fivenumbers_t={7,8,8,8.5,9};
real[] fivenumbers_m={6,6,7,7,8};
real[] fivenumbers_b={2,2.5,3,3.5,4};
boxplot(p,fivenumbers_t,
	boxheight=6pt, whiskerbarwidth=0.75pt,barwidth=1pt);
boxplot(p,fivenumbers_m,
	boxheight=4pt, whiskerbarwidth=0.75pt,barwidth=1pt);
boxplot(p,fivenumbers_b,
	boxheight=2pt, whiskerbarwidth=0.75pt,barwidth=1pt);
xaxis(p,"Data set $C$",YZero,
      xmin=0,xmax=10,
      RightTicks(Step=5,step=1),above=true); 
yaxis(p,"",XZero,
      ymin=0,ymax=6.5);
ytick(p,"3",(0,6));
ytick(p,"2",(0,4));
ytick(p,"1",(0,2));
shipout(format("anova%02d",picnum),p,format="pdf");

