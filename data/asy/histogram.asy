// histogram.asy
//  Draw histograms
import settings;
settings.outformat="pdf";
settings.render=0;

// unitsize(0.1cm);

// include "/usr/local/share/asymptote/jh.asy";
// picture size
import stats;
include graph;

real height = 4cm;  // height of each picture

// http://asy.marris.fr/asymptote/Statistiques/index.html
// ============== 
int picnum = 0;
picture p;
size(p,0,height);


// Tableau des valeurs définissant les classes
real[] tabxi={0,1,2,3,4,5,6,7,10,15,25,50,51};
// Tableau des effectifs (ou fréquences) des classes
//real[] tabni={836,2737,3723,3926,3596,1438,3273,642,824,613,215,57};
real[] tabni={1,2,3,4,5,5,5,15,26,26,8,1};
// Calcul des hauteurs
real[] tabhi;
for(int i=0; i < tabni.length; ++i)
  tabhi[i]=tabni[i]/(tabxi[i+1]-tabxi[i]); 
// Tracé de l'histogramme
histogram(p,tabxi,tabhi,low=0,bars=true);
xaxis(p,"Income in thousands",Bottom,
      RightTicks(Step=10,step=5),above=true); 
yaxis(p,"Percent per thousand",Left,
       Ticks(Step=5,step=5, 
             pTick=blue, ptick=linetype("4 4")+grey,
             extend=false));

shipout(format("histogram%02d",picnum),p,format="pdf");


// ============== 
int picnum = 1;
picture p;
size(p,0,height);

// Tableau des valeurs définissant les classes
real[] tabxi={0,5,10,15,20,25,30,35,40,45,60,90,91};
// Tableau des effectifs (ou fréquences) des classes
real[] tabni={4180,13687,18618,19634,17981,7190,16369,3212,4122,9200,6461,3435};
// Calcul des hauteurs
real[] tabhi;
for(int i=0; i < tabni.length; ++i)
  tabhi[i]=tabni[i]/(300*(tabxi[i+1]-tabxi[i])); 
// Tracé de l'histogramme
histogram(p,tabxi,tabhi,low=0,bars=true);
xaxis(p,"Minutes",Bottom,
      RightTicks(Step=10,step=5),above=true); 
yaxis(p,"Number/minute",Left,
       Ticks(Step=5,step=5, 
             pTick=blue, ptick=linetype("4 4")+grey,
             extend=false));

shipout(format("histogram%02d",picnum),p,format="pdf");


// ============== 
int picnum = 2;
picture p;
size(p,0,height);


// Tableau des valeurs définissant les classes
real[] tabxi={0,1,2,3,4,9};
// Tableau des effectifs (ou fréquences) des classes
real[] tabni={10,10,10,10,10};
// Calcul des hauteurs
real[] tabhi;
for(int i=0; i < tabni.length; ++i)
  tabhi[i]=tabni[i]/(tabxi[i+1]-tabxi[i]); 
// Tracé de l'histogramme
histogram(p,tabxi,tabhi,low=0,bars=true);
xaxis(p,"$x$ quantity",Bottom,
      RightTicks(Step=10,step=1),above=true); 
yaxis(p,"$y$ quantity/$x$ quantity",Left,
       Ticks(Step=5,step=1, 
             pTick=blue, ptick=linetype("4 4")+grey,
             extend=false));

shipout(format("histogram%02d",picnum),p,format="pdf");




// ======= Histogram SMC data ======= 
int picnum = 3;
picture p;
size(p,0,8cm);
scale(p,Linear(15),Linear);

// Tableau des valeurs définissant les classes
real[] tabxi={16,17,18,19,
	      20,21,22,23,24,25,26,27,28,29,
	      30,31,32,33,34,35,36,37,38,39,
	      40,41,42,43,44,45,46,47,48,49,
	      50,51,52,53,54,55
              };
// Tableau des effectifs (ou fréquences) des classes
real[] tabni={1,1,342,486,
              469,431,196,41,18,6,4,1,0,1,
              3,3,3,1,1,0,0,2,2,
              2,2,3,1,3,1,1,0,1,3,
              1,2,0,0,1};
// Calcul des hauteurs
real[] tabhi;
for(int i=0; i < tabni.length; ++i)
  tabhi[i]=tabni[i]/(tabxi[i+1]-tabxi[i]); 
// Tracé de l'histogramme
histogram(p,tabxi,tabhi,low=0,bars=true);
xaxis(p,"{\small Age}",Bottom,
      RightTicks(Step=10,step=10),above=true); 
yaxis(p,"{\small Number}",Left,
       Ticks(Step=100,step=100, 
             pTick=blue, ptick=linetype("4 4")+grey,
             extend=false));

shipout(format("histogram%02d",picnum),p,format="pdf");




// ======= Bar chart SMC data ======= 
int picnum = 4;
picture p;
size(p,0,8cm);
scale(p,Linear(50),Linear);

string jhticklabel(real x){
  string t="--";
  if (x==0) {
    t="";
  } else if(x==1) {
    t="18";
  } else if(x==2) {
    t="";
  } else if(x==3) {
    t="20";
  } else if(x==4) {
    t="";
  } else if(x==5) {
    t="22";
  } else if(x==6) {
    t="";
  } else if(x==7) {
    t="";
  } else if(x==8) {
    t="";
  } else if(x==9) {
    t="26-29";
  } else if(x==10) {
    t="";
  }
  return t;
}

// Tableau des valeurs définissant les classes
real[] tabxi={0,1,2,3,4,5,6,7,8,9,10,11};
// Tableau des effectifs (ou fréquences) des classes
real[] tabni={2,  // < 18
	      342,  // 18
	      486,  // 19
	      469,  // 20
	      431,   // 21
	      196,  // 22
	      41,  // 23
	      18,  // 24 
	      6,  // 25
	      6,  // 26-29
	      38};  // 30+
// Calcul des hauteurs
real[] tabhi;
for(int i=0; i < tabni.length; ++i)
  tabhi[i]=tabni[i]/(tabxi[i+1]-tabxi[i]); 
// Tracé de l'histogramme
histogram(p,tabxi,tabhi,low=0,bars=true);
xaxis(p,"{\small Age}",Bottom,
      // RightTicks(step=1,tickmodifier=jhticklabel),above=true); 
      NoTicks,above=true); 
yaxis(p,"{\small Number}",Left,
       Ticks(Step=100,step=100, 
             pTick=blue, ptick=linetype("4 4")+grey,
             extend=false));

shipout(format("histogram%02d",picnum),p,format="pdf");
