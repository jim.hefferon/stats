// histogram.asy
//  Draw histograms
import settings;
settings.outformat="pdf";
settings.render=0;

unitsize(0.1cm);

include "/usr/local/share/asymptote/jh.asy";
// picture size
import stats;
include graph;

// real height = 4cm;  // height of each picture

import fontsize;
pen tickpen = Gray+fontsize(7pt);

// normal curve x in std dev units
// real f(real x) {return (1/sqrt(2*3.14159*sigma*sigma))*exp(-1*(((x*sigma)-mu)*((x*sigma)-mu))/(2*sigma*sigma));};



// http://asy.marris.fr/asymptote/Statistiques/index.html
// ============== 
int picnum = 0;
picture p;
// size(p,0,height);
unitsize(p, 1.25cm);
scale(p,Linear(0.75),Linear(1));

// Note that the x-axis array has an extra entry, for the end of the last
real[] tabxi={-0.5,0.5,1.5,2.5};
real[] tabni={1/4,1/2,1/4};
real[] tabhi;
for(int i=0; i < tabni.length; ++i)
  tabhi[i]=tabni[i]/(tabxi[i+1]-tabxi[i]); 
// Tracé de l'histogramme
histogram(p,tabxi,tabhi,low=0,bars=true,SkyBlue,Blue);

xaxis(p,"Number of sons",YZero,
      xmin=-1,xmax=3,
      RightTicks(Label("$%.2g$",tickpen),
		 new real[]{0,1,2}),  // Step=1,step=1),
                 above=false); 
yaxis(p,"Probability",XEquals(-1),ymin=0,ymax=1.1,
      LeftTicks(Label("$%.2g$",tickpen),
		new real[]{0,1/4,1/2,1}, // Step=1,step=1,
	    pTick=black, ptick=linetype("4 4")+grey,
             extend=false));

shipout(format("binomial%02d",picnum),p,format="pdf");



// ============== 
int picnum = 1;
picture p;
// size(p,0,height);
unitsize(p, 1.25cm);
scale(p,Linear(0.75),Linear(1));

// Note that the x-axis array has an extra entry, for the end of the last
real[] tabxi={-0.5,0.5,1.5,2.5,3.5};
real[] tabni={1/8,3/8,3/8,1/8};
real[] tabhi;
for(int i=0; i < tabni.length; ++i)
  tabhi[i]=tabni[i]/(tabxi[i+1]-tabxi[i]); 
// Tracé de l'histogramme
histogram(p,tabxi,tabhi,low=0,bars=true,SkyBlue,Blue);

xaxis(p,"Number of sons",YZero,
      xmin=-1,xmax=4,
      RightTicks(Label("$%.2g$",tickpen),
		 new real[]{0,1,2,3}),  // Step=1,step=1),
                 above=false); 
yaxis(p,"Probability",XEquals(-1),ymin=0,ymax=1.1,
      LeftTicks(Label("$%.2g$",tickpen),
		new real[]{0,1/8,3/8,1}, // Step=1,step=1,
	    pTick=black, ptick=linetype("4 4")+grey,
             extend=false));

shipout(format("binomial%02d",picnum),p,format="pdf");



// ============== four-child family
int picnum = 2;
picture p;
// size(p,0,height);
unitsize(p, 1.25cm);
scale(p,Linear(0.75),Linear(1));

// Note that the x-axis array has an extra entry, for the end of the last
real[] tabxi={-0.5,0.5,1.5,2.5,3.5,4.5};
real[] tabni={1/16,4/16,6/16,4/16,1/16};
real[] tabhi;
for(int i=0; i < tabni.length; ++i)
  tabhi[i]=tabni[i]/(tabxi[i+1]-tabxi[i]); 
// Tracé de l'histogramme
histogram(p,tabxi,tabhi,low=0,bars=true,SkyBlue,Blue);

xaxis(p,"Number of sons",YZero,
      xmin=-1,xmax=5,
      RightTicks(Label("$%.2g$",tickpen),
		 new real[]{0,1,2,3,4}),  // Step=1,step=1),
                 above=false); 
yaxis(p,"Probability",XEquals(-1),ymin=0,ymax=1.1,
      LeftTicks(Label("$%.2g$",tickpen),
		new real[]{0,1}, // Step=1,step=1,
	    pTick=black, ptick=linetype("4 4")+grey,
             extend=false));

shipout(format("binomial%02d",picnum),p,format="pdf");




// ============== five-child family
int picnum = 3;
picture p;
// size(p,0,height);
unitsize(p, 1.25cm);
scale(p,Linear(0.75),Linear(1));

// Note that the x-axis array has an extra entry, for the end of the last
real[] tabxi={-0.5,0.5,1.5,2.5,3.5,4.5,5.5};
real[] tabni={1/32,5/32,10/32,10/32,5/32,1/32};
real[] tabhi;
for(int i=0; i < tabni.length; ++i)
  tabhi[i]=tabni[i]/(tabxi[i+1]-tabxi[i]); 
// Tracé de l'histogramme
histogram(p,tabxi,tabhi,low=0,bars=true,SkyBlue,Blue);

xaxis(p,"Number of sons",YZero,
      xmin=-1,xmax=6,
      RightTicks(Label("$%.2g$",tickpen),
		 new real[]{0,1,2,3,4,5}),  // Step=1,step=1),
                 above=false); 
yaxis(p,"Probability",XEquals(-1),ymin=0,ymax=1.1,
      LeftTicks(Label("$%.2g$",tickpen),
		new real[]{0,1}, // Step=1,step=1,
	    pTick=black, ptick=linetype("4 4")+grey,
             extend=false));

shipout(format("binomial%02d",picnum),p,format="pdf");





// ============== five-question test
int picnum = 4;
picture p;
// size(p,0,height);
unitsize(p, 1.25cm);
scale(p,Linear(0.75),Linear(1));

// Note that the x-axis array has an extra entry, for the end of the last
real[] tabxi={-0.5,0.5,1.5,2.5,3.5,4.5,5.5};
real[] tabni={1*(1/4)^(0)*(3/4)^(5),
	      5*(1/4)^(1)*(3/4)^(4),
	      10*(1/4)^(2)*(3/4)^(3),
	      10*(1/4)^(3)*(3/4)^(2),
	      5*(1/4)^(4)*(3/4)^(1),
	      1*(1/4)^(5)*(3/4)^(5)};
real[] tabhi;
for(int i=0; i < tabni.length; ++i)
  tabhi[i]=tabni[i]/(tabxi[i+1]-tabxi[i]); 
// Tracé de l'histogramme
histogram(p,tabxi,tabhi,low=0,bars=true,SkyBlue,Blue);

ScaleX(p,0.5);
xaxis(p,"Number right",YZero,
      xmin=-1,xmax=6,
      RightTicks(Label("$%.2g$",tickpen),
		 new real[]{0,1,2,3,4,5}),  // Step=1,step=1),
                 above=false); 
yaxis(p,"Probability",XEquals(-1),ymin=0,ymax=1.1,
      LeftTicks(Label("$%.2g$",tickpen),
		new real[]{0,1}, // Step=1,step=1,
	    pTick=black, ptick=linetype("4 4")+grey,
             extend=false));

shipout(format("binomial%02d",picnum),p,format="pdf");





// // ============== 
// int picnum = 1;
// picture p;
// unitsize(p, 1.25cm);

// // draw(p,(0,0)--(0,1/4),FCNPEN+blue);
// // draw(p,(1,0)--(1,1/2),FCNPEN+blue);
// // draw(p,(2,0)--(2,1/4),FCNPEN+blue);
// real[] tabxi={-0.5,0.5,1.5,2.5};
// real[] tabni={1/4,1/2,1/4};
// real[] tabhi;
// for(int i=0; i < tabni.length; ++i)
//   tabhi[i]=tabni[i]/(tabxi[i+1]-tabxi[i]); 
// // Tracé de l'histogramme
// histogram(p,tabxi,tabhi,low=0,bars=true,SkyBlue,Blue);

// xaxis(p,"Number heads",YZero,
//       xmin=-1,xmax=3,
//       RightTicks(Label("$%.4g$",tickpen),new real[]{0,1,2},
// 		 pTick=Gray),
//                  above=false);
// yaxis(p,"Probability",XEquals(-1),ymin=0,ymax=1.1,
//       LeftTicks(Label("$%.4g$",tickpen),
// 	    new real[]{0,1/4,1/2,1}, // Step=1,step=1,
// 	    pTick=Gray, ptick=linetype("4 4")+Gray,
//              extend=false));

// shipout(format("dists%02d",picnum),p,format="pdf");


// // ============== 
// int picnum = 2;
// picture p;
// unitsize(p, 1.25cm);

// // size(p, 3cm);

// real[] tabxi={-2.0,-1.9,-1.8,-1.7,-1.6,-1.5,-1.4,-1.3,-1.2,-1.1,
// 	      -1.0,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1,
// 	      0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,
// 	      1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0};
// real[] tabni={0.01,0.02,0.02,0.03,0.04,0.035,0.045,0.04,0.05,0.06,
// 	      0.07,0.075,0.09,0.09,0.10,0.12,0.135,0.15,0.14,0.15,
//               0.15,0.155,0.16,0.15,0.155,0.14,0.135,0.12,0.10,0.09,
//      0.095,0.085,0.07,0.065,0.045,0.03,0.025,0.01,0.01,0.0075};
// real[] tabhi;
// for(int i=0; i < tabni.length; ++i)
//   tabhi[i]=tabni[i]/(tabxi[i+1]-tabxi[i]); 
// // Tracé de l'histogramme
// histogram(p,tabxi,tabhi,low=0,bars=true,SkyBlue,Blue);

// xaxis(p,"Input $x$",YZero,
//       xmin=-2.5,xmax=3,
//       RightTicks(Label("$%.2g$",tickpen),new real[]{-2,-1,0,1,2},
//       	 pTick=Gray),
//       above=false);
// yaxis(p,"Probability",XEquals(-2.5),ymin=0,ymax=1.1,
//       LeftTicks(Label("$%.4g$",tickpen),
// 		new real[]{0,1}, // Step=1,step=1,
// 		pTick=Gray, ptick=linetype("4 4")+Gray,
// 		extend=false));

// shipout(format("dists%02d",picnum),p,format="pdf");




// // ============== 
// int picnum = 3;
// picture p;
// unitsize(p, 1.25cm);

// // size(p, 3cm);
// real f(real x) {return 0.5*exp(-0.5*x);};

// // real[] x={0.01,0.1,0.25,0.5,0.75,1,2,3,4,5};
// // real[] y=0.5*exp(-0.5*x);
// draw(p,graph(f,0.01,5),FCNPEN+Blue);


// xaxis(p,"Input $x$",YZero,
//       xmin=0,xmax=5.5,
//       RightTicks(Label("$%.2g$",tickpen),new real[]{1,2,3,4,5},
//       	 pTick=Gray),
//       above=false);
// yaxis(p,"",XEquals(0),ymin=0,ymax=1.1,
//       LeftTicks(Label("$%.4g$",tickpen),
// 		new real[]{0,1}, // Step=1,step=1,
// 		pTick=Gray, ptick=linetype("4 4")+Gray,
// 		extend=false));

// shipout(format("dists%02d",picnum),p,format="pdf");



// // ============== 
// int picnum = 4;
// picture p;
// unitsize(p, 1.25cm);

// // size(p, 3cm);
// real f(real x) {return 0.5*exp(-0.5*x);};

// // real[] x={0.01,0.1,0.25,0.5,0.75,1,2,3,4,5};
// // real[] y=0.5*exp(-0.5*x);
// path s = (1,f(1))..(1.25,f(1.25))..(1.5,f(1.5))..(1.75,f(1.75))..(2,f(2))
//   --(2,0)--(1,0)--(1,f(1))--cycle;
// fill(p,s,SkyBlue);
// draw(p,graph(f,0.01,5),FCNPEN+Blue);


// xaxis(p,"Input $x$",YZero,
//       xmin=0,xmax=5.5,
//       RightTicks(Label("$%.2g$",tickpen),new real[]{1,2,3,4,5},
//       	 pTick=Gray),
//       above=false);
// yaxis(p,"",XEquals(0),ymin=0,ymax=1.1,
//       LeftTicks(Label("$%.4g$",tickpen),
// 		new real[]{0,1}, // Step=1,step=1,
// 		pTick=Gray, ptick=linetype("4 4")+Gray,
// 		extend=false));

// shipout(format("dists%02d",picnum),p,format="pdf");




// // ============== 
// int picnum = 5;
// picture p;
// unitsize(p, 1.25cm);

// // size(p, 3cm);
// real f(real x) {return (1/sqrt(2*3.14159))*exp(-(1/2)*x*x);};

// // real[] x={0.01,0.1,0.25,0.5,0.75,1,2,3,4,5};
// // real[] y=0.5*exp(-0.5*x);
// draw(p,graph(f,-3,3),FCNPEN+Blue);


// xaxis(p,"Input $z$",YZero,
//       xmin=-3.2,xmax=3.2,
//       RightTicks(Label("$%.2g$",tickpen),new real[]{-3,-2,-1,0,1,2,3},
//       	 pTick=Gray),
//       above=false);
// yaxis(p,"",XEquals(0),ymin=0,ymax=1.1,
//       LeftTicks(Label("$%.2g$",tickpen),
// 		new real[]{0,1}, // Step=1,step=1,
// 		pTick=Gray, ptick=linetype("4 4")+Gray,
// 		extend=false));

// shipout(format("dists%02d",picnum),p,format="pdf");




// // ============== 
// int picnum = 6;
// picture p;
// unitsize(p, 1.25cm);

// // size(p, 3cm);
// real sigma = 1/2;
// real mu = 0;
// real f(real x) {return (1/sqrt(2*3.14159*sigma*sigma))*exp(-1*((x-mu)*(x-mu))/(2*sigma*sigma));};

// // real[] x={0.01,0.1,0.25,0.5,0.75,1,2,3,4,5};
// // real[] y=0.5*exp(-0.5*x);
// draw(p,graph(f,-1.5,1.5),FCNPEN+Blue);

// xaxis(p,"Input $z$",YZero,
//       xmin=-1.7,xmax=1.7,
//       RightTicks(Label("$\mu$",tickpen),new real[]{mu},
//       	 pTick=Gray),
//       above=false);
// xtick(p,"$\mu+\sigma$",0.5,tickpen);
// // labelx(p,"$\mu+\sigma$",0.5,tickpen,align=S);
// yaxis(p,"",XEquals(-1.7),ymin=0,ymax=1.1,
//       LeftTicks(Label("$%.2g$",tickpen),
// 		new real[]{0,1}, // Step=1,step=1,
// 		pTick=Gray, ptick=linetype("4 4")+Gray,
// 		extend=false));

// shipout(format("dists%02d",picnum),p,format="pdf");




// // ============== IQ test
// int picnum = 7;
// picture p;
// unitsize(p, 1.25cm);

// // size(p, 3cm);
// real mu = 0;
// real sigma = 0.75;
// real f(real x) {return (1/sqrt(2*3.14159*sigma*sigma))*exp(-1*((x*sigma-mu)*(x*sigma-mu))/(2*sigma*sigma));};

// // real[] x={0.01,0.1,0.25,0.5,0.75,1,2,3,4,5};
// // real[] y=0.5*exp(-0.5*x);
// draw(p,graph(f,-1.7,1.7),FCNPEN+Blue);

// xaxis(p,"IQ $z$",YZero,
//       xmin=mu-2.1*sigma,xmax=mu+2.1*sigma,
//       RightTicks(Label("$100$",tickpen),new real[]{mu},
//       	 pTick=Gray),
//       above=false);
// xtick(p,"$70$",mu-2*sigma,tickpen);
// xtick(p,"$85$",mu-sigma,tickpen);
// xtick(p,"$115$",mu+sigma,tickpen);
// xtick(p,"$130$",mu+2*sigma,tickpen);
// // labelx(p,"$\mu+\sigma$",0.5,tickpen,align=S);
// yaxis(p,"",XEquals(-2.1*sigma),ymin=0,ymax=1.1,
//       LeftTicks(Label("$%.2g$",tickpen),
// 		new real[]{0,1}, // Step=1,step=1,
// 		pTick=Gray, ptick=linetype("4 4")+Gray,
// 		extend=false));

// shipout(format("dists%02d",picnum),p,format="pdf");




// // ============== Math SAT
// int picnum = 8;
// picture p;
// unitsize(p, 1.25cm);

// // size(p, 3cm);
// real sigma = 0.75;
// real mu = 0;
// // real f(real x) {return (1/sqrt(2*3.14159*sigma*sigma))*exp(-1*((x-mu)*(x-mu))/(2*sigma*sigma));};

// // real[] x={0.01,0.1,0.25,0.5,0.75,1,2,3,4,5};
// // real[] y=0.5*exp(-0.5*x);
// draw(p,graph(f,-1.7,1.7),FCNPEN+Blue);

// xaxis(p,"SAT score $z$",YZero,
//       xmin=-1.9,xmax=1.9,
//       RightTicks(Label("$500$",tickpen),new real[]{mu},
//       	 pTick=Gray),
//       above=false);
// xtick(p,"$300$",mu-2*sigma,tickpen);
// xtick(p,"$400$",mu-sigma,tickpen);
// xtick(p,"$600$",mu+sigma,tickpen);
// xtick(p,"$700$",mu+2*sigma,tickpen);
// // labelx(p,"$\mu+\sigma$",0.5,tickpen,align=S);
// yaxis(p,"",XEquals(-1.9),ymin=0,ymax=1.1,
//       LeftTicks(Label("$%.2g$",tickpen),
// 		new real[]{0,1}, // Step=1,step=1,
// 		pTick=Gray, ptick=linetype("4 4")+Gray,
// 		extend=false));

// shipout(format("dists%02d",picnum),p,format="pdf");




// // ============== Women's heights
// int picnum = 9;
// picture p;
// unitsize(p, 1.25cm);

// real sigma = 0.75;
// real mu = 0;
// real f(real x) {return (1/sqrt(2*3.14159*sigma*sigma))*exp(-1*((x-mu)*(x-mu))/(2*sigma*sigma));};

// draw(p,graph(f,-1.7,1.7),FCNPEN+Blue);

// xaxis(p,"Height $z$",YZero,
//       xmin=-1.9,xmax=1.9,
//       RightTicks(Label("$65$",tickpen),new real[]{mu},
//       	 pTick=Gray),
//       above=false);
// xtick(p,"$58$",mu-2*sigma,tickpen);
// xtick(p,"$61.5$",mu-sigma,tickpen);
// xtick(p,"$68.5$",mu+sigma,tickpen);
// xtick(p,"$72$",mu+2*sigma,tickpen);
// // labelx(p,"$\mu+\sigma$",0.5,tickpen,align=S);
// yaxis(p,"",XEquals(-1.9),ymin=0,ymax=1.1,
//       LeftTicks(Label("$%.2g$",tickpen),
// 		new real[]{0,1}, // Step=1,step=1,
// 		pTick=Gray, ptick=linetype("4 4")+Gray,
// 		extend=false));

// shipout(format("dists%02d",picnum),p,format="pdf");



// // ============== paper bag population 
// int picnum = 10;
// picture p;
// // size(p,0,height);
// unitsize(p, 0.75cm);

// // Note that the x-axis array has an extra entry, for the end of the last
// real[] tabxi={0.5,1.5,2.5,3.5,4.5,5.5};
// real[] tabni={1,2,3,4,5};
// real[] tabhi;
// for(int i=0; i < tabni.length; ++i)
//   tabhi[i]=tabni[i]/(tabxi[i+1]-tabxi[i]); 
// // Tracé de l'histogramme
// histogram(p,tabxi,tabhi,low=0,bars=true,SkyBlue,Blue);

// xaxis(p,"Number on paper $x$",YZero,
//       xmin=0,xmax=6,
//       RightTicks(Label("$%.2g$",tickpen),
// 		 new real[]{1,2,3,4,5}),  // Step=1,step=1),
//                  above=false); 
// yaxis(p,"How many are in the bag",XEquals(0),ymin=0,ymax=5.1,
//       LeftTicks(Label("$%.2g$",tickpen),
// 	    Step=1,step=1,
// 	    pTick=black, ptick=linetype("4 4")+grey,
//              extend=false));

// shipout(format("dists%02d",picnum),p,format="pdf");




// // ============== stanines
// // int picnum = 11;
// // picture p;
// // size(p,0,height);
// // unitsize(p, 1.25cm);

// real sigma = 0.75;
// real mu = 0;
// real f(real x) {return (1/sqrt(2*3.14159*sigma*sigma))*exp(-1*(((x*sigma)-mu)*((x*sigma)-mu))/(2*sigma*sigma));};

// // real[] x={0.01,0.1,0.25,0.5,0.75,1,2,3,4,5};
// // real[] y=0.5*exp(-0.5*x);

// xaxis(p,"",YZero,
//       xmin=-1.9,xmax=1.9,
//       RightTicks(Label("$500$",tickpen),new real[]{mu},
//       	 pTick=Gray),
//       above=false);
// // xtick(p,"$300$",mu-2*sigma,tickpen);
// // xtick(p,"$400$",mu-sigma,tickpen);
// // xtick(p,"$600$",mu+sigma,tickpen);
// // xtick(p,"$700$",mu+2*sigma,tickpen);
// // labelx(p,"$\mu+\sigma$",0.5,tickpen,align=S);
// yaxis(p,"",XEquals(-1.9),ymin=0,ymax=1.1,
//       LeftTicks(Label("$%.2g$",tickpen),
// 		new real[]{0,1}, // Step=1,step=1,
// 		pTick=Gray, ptick=linetype("4 4")+Gray,
// 		extend=false));

// for(int i=0; i < 9; ++i) {
//   int picnum = 11+i;
//   picture p;
//   unitsize(p,1.25cm);

//   draw(p,graph(f,-1.7,1.7),FCNPEN+Blue);

//   real xmin=-1.9;
//   real xmax=1.9;
//   xaxis(p,"",YZero,
//       xmin=xmin,xmax=xmax,
//       RightTicks(Label("",tickpen),new real[]{},
//       	 pTick=Gray),
//       above=false);
//   yaxis(p,"",XEquals(-1.9),ymin=0,ymax=1.1,
//       LeftTicks(Label("$%.2g$",tickpen),
// 		new real[]{0,1}, // Step=1,step=1,
// 		pTick=Gray, ptick=linetype("4 4")+Gray,
// 		extend=false));
//   real barhgt = -0.35;
//   if (i==0){
//     real leftend = mu-2.25*sigma;
//     real rightend = mu-1.75*sigma;
//     // make the area to be filled in below the curve
//     path bottompath=(leftend,2)--(leftend,0)--(rightend,0)--(rightend,2);
//     path area = buildcycle(bottompath,graph(f,-1.7,1.7));
//     fill(p,area,SkyBlue);
//     // interval
//     draw(p,(leftend,barhgt)--(mu-1.75*sigma,barhgt),arrow=BeginArrow(2pt),bar=EndBar(2pt),red);
//     label(p,"\makebox[0em][l]{$-1.75\sigma$}",(mu-1.75*sigma,barhgt),align=N,tickpen);
//     label(p,"stanine~$1$",((0.5)*(leftend+rightend),barhgt),align=S,tickpen);
//   }
//   if (i==8) {
//     real leftend = mu+1.75*sigma;
//     real rightend = mu+2.25*sigma;
//     // make the area to be filled in below the curve
//     path bottompath=(leftend,2)--(leftend,0)--(rightend,0)--(rightend,2);
//     path area = buildcycle(bottompath,graph(f,-1.7,1.7));
//     fill(p,area,SkyBlue);
//     // interval
//     draw(p,(leftend,barhgt)--(rightend,barhgt),arrow=EndArrow(2pt),bar=BeginBar(2pt),red);
//     label(p,"\makebox[0em][r]{$1.75\sigma$}",(leftend,barhgt),align=N,tickpen);
//     label(p,"stanine $9$",((0.5)*(leftend+rightend),barhgt),align=S,tickpen);
//   }
//   if ((i!=0) && (i!=8)) {
//     real leftend = mu+(-2.25+0.50*i)*sigma;
//     real rightend = leftend+0.50*sigma;
//     // make the area to be filled in below the curve
//     path bottompath=(leftend,2)--(leftend,0)--(rightend,0)--(rightend,2);
//     path area = buildcycle(bottompath,graph(f,-1.7,1.7));
//     fill(p,area,SkyBlue);
//     // draw the interval
//     draw(p,(mu+leftend,barhgt)--(mu+rightend,barhgt),bar=Bars(2pt),red);
//     label(p,format("\makebox[0em][r]{$%.02f\sigma$}",-2.25+i*(0.5)),(leftend,barhgt),align=N,tickpen);
//     label(p,format("\makebox[0em][l]{$%.02f\sigma$}",-1.75+i*(0.5)),(rightend,barhgt),align=N,tickpen);
//     label(p,format("\makebox[0em][c]{stanine $%d$}",i+1),(0.5*(leftend+rightend),barhgt),align=S,tickpen);      
//   }
//   // draw the graph over the shaded area
//   draw(p,graph(f,-1.7,1.7),FCNPEN+Blue);
//   shipout(format("dists%02d",picnum),p,format="pdf");
// }





// // Tableau des valeurs définissant les classes
// real[] tabxi={0,1,2};
// // Tableau des effectifs (ou fréquences) des classes
// real[] tabni={0.51,0.49};
// // Calcul des hauteurs
// real[] tabhi;
// for(int i=0; i < tabni.length; ++i)
//   tabhi[i]=tabni[i]/(tabxi[i+1]-tabxi[i]); 
// // Tracé de l'histogramme
// histogram(p,tabxi,tabhi,low=0,bars=true);
// xaxis(p,"H/T",Bottom,
//       RightTicks(Step=1,step=1),above=true); 
// yaxis(p,"Percent",Left,
//        Ticks(Step=1,step=1, 
//              pTick=blue, ptick=linetype("4 4")+grey,
//              extend=false));

// shipout(format("dists%02d",picnum),p,format="pdf");


// // ============== 
// int picnum = 1;
// picture p;
// size(p,0,height);

// // Tableau des valeurs définissant les classes
// real[] tabxi={0,5,10,15,20,25,30,35,40,45,60,90,91};
// // Tableau des effectifs (ou fréquences) des classes
// real[] tabni={4180,13687,18618,19634,17981,7190,16369,3212,4122,9200,6461,3435};
// // Calcul des hauteurs
// real[] tabhi;
// for(int i=0; i < tabni.length; ++i)
//   tabhi[i]=tabni[i]/(300*(tabxi[i+1]-tabxi[i])); 
// // Tracé de l'histogramme
// histogram(p,tabxi,tabhi,low=0,bars=true);
// xaxis(p,"Minutes",Bottom,
//       RightTicks(Step=10,step=5),above=true); 
// yaxis(p,"Number/minute",Left,
//        Ticks(Step=5,step=5, 
//              pTick=blue, ptick=linetype("4 4")+grey,
//              extend=false));

// shipout(format("histogram%02d",picnum),p,format="pdf");


// // ============== 
// int picnum = 2;
// picture p;
// size(p,0,height);


// // Tableau des valeurs définissant les classes
// real[] tabxi={0,1,2,3,4,9};
// // Tableau des effectifs (ou fréquences) des classes
// real[] tabni={10,10,10,10,10};
// // Calcul des hauteurs
// real[] tabhi;
// for(int i=0; i < tabni.length; ++i)
//   tabhi[i]=tabni[i]/(tabxi[i+1]-tabxi[i]); 
// // Tracé de l'histogramme
// histogram(p,tabxi,tabhi,low=0,bars=true);
// xaxis(p,"$x$ quantity",Bottom,
//       RightTicks(Step=10,step=1),above=true); 
// yaxis(p,"$y$ quantity/$x$ quantity",Left,
//        Ticks(Step=5,step=1, 
//              pTick=blue, ptick=linetype("4 4")+grey,
//              extend=false));

// shipout(format("histogram%02d",picnum),p,format="pdf");
