\documentclass[12pt]{article}
\usepackage{overhead}
\renewcommand{\footnote}[1]{\relax}

\begin{document}
\begin{slide}
  \slidetitle{Before Starting \\
              a \\
              STATISTICAL INVESTIGATION}
\begin{slidepoints}
  \item[Where does the idea come from?]
    \begin{description}
      \item[Part of an ongoing program]
        Exploratory data analysis.
        \textit{A researcher who has established an effect on test scores 
          in elementary school
          students who do not receive breakfast notices in the data 
          that boys appear to be affected more than girls.}
       The question can be a next step of some research program.  
        \textit{The researcher decides to study whether
          students with a history of bad scores after no breakfast 
          can have their grades improved by a
          school breakfast program.}
      \item[Current controversy]  
        A person may want to settle some hot question.
        \textit{Are radio waves linked to cancer?}
      \item[Anecdotal evidence]
        An investigator wonders if there is a larger pattern in a 
        small set of occurrences.
        \textit{A doctor investigates headaches among people 
          taking the new cold pill.} 
      \item[Ordered up]
        An investigator gets money to do a study from someone 
        (company, political activist, lawyer) who 
        believes that the outcome will strengthen their position.
        \textit{A drug company, about to market a new pain pill,
          underwrites a study on side effects of the current treatment.}
    \end{description}
  \item[Deciding to proceed]
    \begin{description}
      \item[Is the question worth answering?]
        Is it worth some fraction of your life?
      \item[Is the answer already known?]
        What is the history of work in this area?
        By reputable people?
        Is there some new feature or idea?
      \item[Can what we are asking be determined from data?] 
        Can it be measured?
        How?   When?
      \item[About what population are we asking?]
        Can we identify members of that population?
        Is the population too big to survey, and we must sample? 
      \item[Cost?]
        How will we pay for it, in time and money?
    \end{description}
  \item[Remember]              
    \textbf{Most studies are not neutral---we typically gather data to
            support some point of view.}
\end{slidepoints}
\end{slide}


\begin{slide}
  \slidetitle{Kinds of Studies}

Studies classify according to two dimensions.
\begin{itemize}
  \item A \definend{census} takes data from the entire population.
    \textit{We get a vote from every senator on the Act of War.}

    A \definend{sample} takes data from some small part.
    \textit{We select a thousand people at randome and ask them about
      the Act of War.}

  \item In an \definend{observational study}, the investigator observes
    the variation of interest,  passively gathering data.
    \textit{We tabulate at what age the kings and queens of England ascended
    to the throne.}

    An \definend{experiment} is where the observer acts to affect the 
    data set. 
    The investigator institutes controls; 
    a treatment is deliberately 
    imposed on some individuals and witheld from others.
    The investigator acts in the desire that the two groups differ only
    in whether or not the treatment is applied
    (various strategies are employed to keep all other differences constant).
    \textit{We get a hundred people with arthritis and give half of 
    them a new pill, while the other half get a similar-looking, but
    inactive pill.} 

    (There is a gray area between observational studies and experiments; 
    many studies are a mixture of the two.)
\end{itemize}
\end{slide}



\begin{slide}
  \slidetitle{Census vs.~Sample}

The idea of gathering information by asking everyone is obvious, 
and old.
\begin{quotation}
  \small In those days a decree went out from Caesar Augustus for a census of 
  the whole world. 
  \ldots
  So all went to be enrolled, each to his own town.  
  \textit{from:~Gospel according to Luke} 
\end{quotation}
The US Constitution has a provision that a census be conducted every ten
years.

In cases where the population is small, it is the best thing to do.

\begin{example}
There are six known skeletons of \textit{Archeopteryx}, a creature often seen
as a link between dinosaurs and birds.
A sutdy of beak length that fails to consider some of the six
skeletons is dubious.
\end{example}

\begin{example}
There have not been too many presidents.
A study of their heights can easily be done by
considering them all.  
\end{example}

But a census is a tremendous operation in a population of 
even moderate size.
It is costly in time, effort, and money.

\begin{example}
Getting the CEO's of all of the Fortune 500 to tell their height would be 
quite hard.  
\end{example}

\begin{example}
Sending a questionaire to every pair of identical twins in the US
is a very large undertaking.  
\end{example}

\begin{example} \textsc{Fisher's Corn Plants}
To test the effect of a new fertilizer on corn plants, we cannot 
subject them to every possible combination of temperature range,
sunlight, pests, soil acidity, etc. 
\end{example}

Sometimes a census is not even possible.

\begin{example}
We cannot flip a coin infinitely many times to test if it is fair.  
\end{example}

\begin{example}
To discover the air pressure at which a brand of auto tires blow out, we
cannot blow them all out.
\end{example}
\end{slide}



\begin{slide}
%  \slidetitle{Census versus Sample (continued)}
To a person first approaching statistics, sampling seems dubious.
Do we sample only because a census is too expensive?
If we are willing to do the extra work, will it pay?
There is historical reason to doubt the usefulness of such a large
operation as a census, anyway.

\begin{example}
  \famouscase{Literary Digest Poll}
In 1936, after FDR's first term, A.~Landon ran against him.
The \textit{Literary Digest} mailed questionaires to ten million
people.
That's one voter out of four, which is an attempt more at a census 
than a sample.  
They added up all of the $2.4$~million returned questionares and 
predicted that FDR would get $43\%$ of the vote.
Instead of counting all of those returned responses, George Gallup sampled 
three thousand of them, and knew in advance the \textit{Digest}'s 
predicted $43\%$.
Not only was the great effort unnecessary, but it also gave the wrong 
answer---Roosevelt didn't get $43\%$ of the vote, he got $62\%$.   
\end{example}

\begin{center}
\fbox{\begin{minipage}{.9\textwidth}
         We can draw accurate conclusions about a population from only a small
         sample, if the sample is taken correctly. 
  \end{minipage}}
\end{center}

\begin{example}
  \famouscase{Fishers Corn Plants}
  In a test of what factors affect corn yield, researchers had developed an
  elaborate design that was suppossed to balance all of the
  factors---temperature range, soil moisture, pest exposure, etc.---so that
  the effect of a fertilizer could be deduced.
  But every design was subject to new objections.
  R.A.~Fisher's solution:~randomize.
  For each plant, to decide whether to fertilize or not, flip a coin.
  To answer, say,
  ``How do you know that there is a balance for soil acidity?''
  \begin{center}
    \begin{tabular}{r|cc|}
       \multicolumn{1}{c}{\ }
           &\multicolumn{1}{c}{\textit{soil acid}}   
           &\multicolumn{1}{c}{\textit{soil basic}}              \\
      \cline{2-3}
      \textit{fertilized}      &$25\%$  &$25\%$  \\
      \textit{unfertilized}    &$25\%$  &$25\%$  \\
      \cline{2-3}
    \end{tabular}
  \end{center}
  Fisher replies, ``Because I flipped a coin.''
\end{example}
\end{slide}



\begin{slide}
  \slidetitle{Observational Studies}

In an \emph{observational study} the investigator observes the variation 
but does not act to sharpen the data.

\begin{example}
Jane Goodall
goes with the chimp troop, waits until she believes that they are used to her,
and then records the diet that she sees them eating.
\end{example}

\begin{example}
To see the effect of unemployment on profitability, we draw up a table
comparing the national unemployment rate against the price/earnings ratio
for companies in the Fortune 200.   
\end{example}

\begin{example}
We are interested in whether having an older mother is, in part, 
a cause of troubled pregnancies.
We look at the files of every birth in the local hospital over the past year
and draw up a table whose rows give the age of the mother and whose columns 
list the percentage of pregnacies that are `routine', `mildly troubled',
or `quite troubled'.
\end{example}

\begin{example}
To help see if smoking causes cancer, we ask a thousand people with cancer if
they are long-time smokers.
\end{example}

\begin{example}
To help doctors choose which of two treatments for blocked arteries is better,
a study looks at 250 patients who have choosen one method or the other, and 
sees how they have turned out. 
\end{example}

\begin{example}
A doctor trying out a new pill that the drug company rep gave out gives it 
to all the patients who came in this week with mild pain.
A week later, the patients are called and $60\%$ say that they feel better.   
\end{example}

Problems with the last case.
\begin{itemize}
  \item How do we know that there is any effect at all?
    Maybe without treatment $60\%$ get better
    (maybe $70\%$).
  \item How do we know the people even took the pills?
  \item Maybe people say they feel better because the doctor's office took
     the trouble to call back.
  \item Even if the pill does have an effect, maybe it is not as good, or much
     more expensive, than the current treatment.
  \item Maybe some aspect of this doctor's patients predisposes them to 
     do well with this pill (older, etc; maybe these patients are mostly
     working class and this pill is good at body aches).
  \item Maybe, just by happenstance, this doctor got a high sample.
     Perhaps next week's patients would give give a low sample.
\end{itemize}
\end{slide}




\begin{slide}
  \slidetitle{Experiment}
In an \emph{experiment} the observer acts to make the data as useful as
possible.
The investigator imposes some change in order to see its effect.

\begin{example}
\famouscase{Salk Polio Vaccine}
Polio hit the US in 1916 and until the 1950's killed many people.
The vaccine developed by Salk was known to cause the production of antibodies
against polio; does it prevent the disease?
If the treatment were simply given to all children in the country, a drop in 
the disease rate might be because of the vaccine.
But it might be simply because the rate varies from year to year
(e.g., the 1953 rate was half of the 1952 rate).

In one study, researchers took children (whose parents had given permission 
for them to be in the experiment), and split them into two groups.
Both groups got shots; the first group got the vaccine while the second got 
a shot of salt water.
\begin{center}
  \begin{tabular}{|r|c|}
    \multicolumn{1}{c}{\textit{group}}      
     &\multicolumn{1}{c}{\textit{rate of polio per 100,000}} \\
    \hline
    vaccine    &28                        \\
    salt water &71                        \\ \hline
  \end{tabular}                   \\
%    \textit{results of Salk 
%                trial}\footnote{\emph{An evaluation of the 1954 poliomyelitis 
%               vaccine trial}, Thomas Francis, Jr., American Journal of Public 
%               Health, 1954, p.~1-53} 
\end{center}
\end{example}

\end{slide}




\begin{slide}
  \slidetitle{Back at that Holiday Inn \\
              in Kansas City}

\begin{example}
Recall the hypothetical example from the first day of a doctor whose
patients try a new cold pill and some report headaches.               
The study, if conducted, must be made up to deflect or answer questions that
the audience will express.

The doctor is making an argument.
Two kinds of questions need to be addressed.
\begin{description}
  \item[Concerns unique to this topic]
     On the face of it, a claim that a pill causes headaches in some of
     the people who take it seems possible.
     The claim that to some users, it confers immortality, would
     require stronger evidence.
  \item[Concerns in general]
     Professionals have a historically-based sensitivity in some directions.
     They know where others have stumbled, and are not likely to let
     another such error slide by.
\end{description}
\end{example}
\end{slide}


\begin{slide}
  \slidetitle{Observational Study versus Experiment}


\begin{example}
California evaluates a new program to rehabilitate prisoners before their 
release.
They get several months of ``boot camp'', military-style training with 
strict discipline.
Admission to the program is voluntary (most prisoners are promised a
reduction in time served in return for volunteering).
A prison spokesman says, ``Those who complete boot camp are less likely
to return to prison than other inmates.''\footnote{hypothetical, from
David Freedman, et.\ al.}  
\end{example}

\begin{example}
Many observational studies have found that people who get many extra
vitamins by eating five or more 
servings of fresh fruit and vegtables each day (especially those of a type
including broccoli) have much lower death rates from colon cancer and 
lung cancer.
Some large randomized controlled experiments were done;\footnote{\emph{A 
clinical trial of antioxidant vitamins to prevent colorectal adenoma}, 
E.R.~Greenburg et.\ al., New England Journal of Medicine, 1994, p.~141-147, 
and \emph{Effect of Vitamin E and Beta Carotene on the Incidence of Lung 
Cancer and Other Cancers in Male Smokers}, O.P.~Heinonen, New England 
Journal of Medicine, 1994, p.1029-1035} 
a treatment group go extra vitamins and the control group ate their usual diet.
There was found to be no difference between the treatment and 
control groups.   
\end{example}

\begin{example}
The disease Pellagra was first observed in Europe in the
1700's, especially in the peoples of northern Italy.
It spread in the 1800's into southwestern France, Austria, Romania,
and parts of the Turkish Empire.
It was recognized in Egypt, South Africa, and was prevelant in the south 
of the US.
It seemed to hit some villages more than others, and even some households more
than others.
One blood-sucking fly was had roughly the same geographical range as
the area of greatest disease, and further, the fly was most active in the
spring which was when the disease was most prevalent.
Many epidemiologists figured that this was another insect-borne disease
like malaria or Lyme. 

Around 1914, the American epidemiologist Joseph Goldberger showed
(in a combination of observational studies and experiments) that Pellagra
is caused by bad diet, not an insect.
Milk, meat, and eggs contain a nutritional component that prevents 
pallegra but corn contains little of this component.
Poor people ate corn, mostly.
Since 1940 most of the flour sold in the US is enriched with a vitamin
which prevents the disease (niacin). 
\end{example}


\begin{example}
Researchers in several different countries found that cervical cancer was 
quite rare among Jews.
It was also found to be rare among Moslems.
In the 1950's several investigators announced that they were satisfied  
that circumcision of males was the protective 
factor.\footnote{\emph{A study
of environmental factors in carcinoma of the cervix}, E.L.~Wynder et.\ al.,
American Journal of Obstetrics and Gynecology, 1954, p.~1016-1052}
But there are other differencs between Jews and Moslems, 
and other well-studied
groups.
Current research suggests that cervical cancer is a sexually transmitted 
disease (the human papiloma virus is implicated); early researchers did
not pay attention to this confounding variable (in part because 
cancer takes
so long to develop and so sexual behavior in the 1930's and 1940's is the
variable at issue).\footnote{c.f., \emph{A Review of Problems of Bias and 
Confounding in Epidemologic Studies of Cervical Neoplasia and Oral 
Contraceptive Use}, S.H.~Swan and D.B.~Pettiti, American Journal of 
Epidemology, 1982, p.~10-18}  
\end{example}

We must split the subjects into two groups, control and treatment.
We act to have, on average, the only difference between
the two groups be that the treatment group gets the treatment.
\clearpage

\begin{slidepoints}
  \item[Other effects requiring use of a control group]
    \begin{description}
      \item[Placebo effect]
         Subjects respond to an inactive treatment.
      \item[Hawthorn effect]
         Experimenters may not understand what subjects are responding to.   
      \item[Factors you haven't discovered yet]
        Isn't the point of doing research that you don't entirely understand
        the question?

        \begin{example}
         According to a study at Kaiser Permanente, 
         users of oral contraceptive have
         a higher rate of cervical cancer than nonusers 
         (even after adjusting for
         factors such as age, education, and marital status).
         They conclude that the Pill causes 
         cancer.\footnote{\emph{The Incidence of 
         Cervical Cancer and Duration of Oral Contraceptive Use}, 
         E.~Peritz et.\ al.,
         American Journal of Epidemiology, 1977, p.~462-469}
        \end{example}
    \end{description}
  \item[The control group must be randomized]

    \begin{example}
    With cirrhosis of the liver, a patient may hemorrhage.
    One treatment is to redirect the blood through a `portacaval shunt'.
    This summarizes the studies on the effectiveness of this 
    treatment.\footnote{\textit{The Present Status of Shunts for Portal 
    Hypertension in Cirrhosis}, N.D.~Grace, et.\ al., Gastroenterology, 1966, 
    p.~684-691}
    \begin{center}
      \begin{tabular}{l|rrr|c}
         \multicolumn{1}{c}{\ }
                    &\multicolumn{3}{c}{\textit{degree of enthusiasm}} & \\
         \cline{2-4}
         \textit{methodology} 
             &\multicolumn{1}{|c}{\textit{strong}}  
             &\multicolumn{1}{c}{\textit{moderate}}  
             &\multicolumn{1}{c|}{\textit{none}}
             &\textit{percentage strong}                        \\
          \hline
          No controls               &$24$      &$7$    &$1$  &$75\%$  \\
          Controls, not randomized  &$10$      &$3$    &$2$  &$67\%$  \\
          Randomized controls       &$0$       &$1$    &$3$  &$0\%$  %\\ \hline
      \end{tabular}
    \end{center}
    \end{example}
  \item[The control group cannot be historical]
   \begin{example}
    These are the three-year survival rates from studies on the
    portacaval shunt.
    \begin{center}
      \begin{tabular}{l|cc}
          \multicolumn{1}{c}{\ }
             &\multicolumn{1}{c}{\textit{randomized control}}  
             &\multicolumn{1}{c}{\textit{historical control}}  \\
          \cline{2-3}
        Treatment (surgery) group   &$60\%$  &$60\%$  \\
        Control group               &$60\%$  &$45\%$  %\\ \cline{2-3}
      \end{tabular}
    \end{center}
    \end{example}

    \begin{example}
    Coronary bypass surgery is widely used.
    Does it help people live longer?
    These are the three-year survival rates reported in two types 
    of studies (totalling $9\,290$ patients and $18\,861$ patients). 
%    The first type randomly assigns patients to control or treatment group
%    (there were six studies totaling $9,290$ patients).
%    The second type uses historical controls (nine studies with $18,861$
%    patients).\footnote{\textit{Randomized versus Historical Controls for 
%    Clinical Trials}, H.~Sacks, et.\ al., American Journal of Medicine, 
%    1982, p.~233-240}
    \begin{center}
      \begin{tabular}{l|cc}
          \multicolumn{1}{c}{\ }
             &\multicolumn{1}{c}{\textit{randomized control}}  
             &\multicolumn{1}{c}{\textit{historical control}}  \\
          \cline{2-3}
        Treatment (surgery) group   &$87.6\%$  &$90.9\%$  \\
        Control group               &$83.2\%$  &$71.1\%$
      \end{tabular}
    \end{center}
    A few studies did not report three-year rates.
    Overall, the summary is this. 
    \begin{center}
      \begin{tabular}{l|cc|cc|}
           \multicolumn{1}{c}{\ }
                           &\multicolumn{2}{c}{\textit{randomized control}}
                           &\multicolumn{2}{c}{\textit{historical control}}     \\
         \cline{2-5}                

        \textit{therapy}   &\textit{num positive} &\textit{num negative}
                           &\textit{num positive} &\textit{num negative} \\
        \hline
        Coronary bypass    &$1$  &$7$   &$16$  &$5$   %\\ \hline
      \end{tabular}
    \end{center}
    %The less-carefully done studies have a greater tendency to be positive.
    \end{example}

    \begin{example}
    Here is the same effect with three other treatments.  
    \begin{center}
      \begin{tabular}{l|cc|cc|}
          \multicolumn{1}{c}{\ }
                  &\multicolumn{2}{c}{\textit{randomized control}}
                  &\multicolumn{2}{c}{\textit{historical control}}     \\
         \cline{2-5}                

        \textit{therapy}   &\textit{num positive} &\textit{num negative}
                           &\textit{num positive} &\textit{num negative} \\
        \hline
         5-FU   &$0$  &$5$   &$2$  &$0$   \\
         BCG   &$2$  &$2$   &$4$  &$0$   \\
         DES   &$0$  &$3$   &$5$  &$0$   %\\ \hline
      \end{tabular}
    \end{center}
    The first treatment is used in chemotherapy for colon cancer.
    The second is used for melanoma.
    The third DES is an artificial hormone used to prevent spontaneous 
    abortion.
    The randomized controlled experiments seemed to persuade few doctors to
    be cautious.
    Even in the late 1960's, fifty thousand women a year were being given the 
    drug.
    We now know that if administered to the mother during pregnancy, 
    the drug can 
    cause a daughter to develop, twenty years later, a cancer (clear-cell
    adencarcinoma of the vagina); it was banned on pregnant women in 1971.
    \end{example}
  \item[The controls must be double-blinded]
        Clever Hans: the person gathering data should not know which is
        treatment and which is control (and neither should the subject,
        either).
\end{slidepoints}
\end{slide}


\begin{slide}
  \slidetitle{The SECOND deep sentence of this course}

A statistical investigation whose methodology is a little off can easily
yield results that are totally off.
\end{slide}
%
%\begin{example}
%  A study of young children found that those with more body fat tended to have
%  more ``controlling'' mothers.\footnote{\emph{Parents and Children's Adioposity and Eating Style}, S.L.~Johnson, L.L.~Birch, Pediatrics, 1994, p.~1150-1155} 
%\end{example}


\end{document}


