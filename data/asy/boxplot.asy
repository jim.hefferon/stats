// boxplot.asy
//  Draw box plotss
import settings;
settings.outformat="pdf";
settings.render=0;

// unitsize(0.1cm);

include "/usr/local/share/asymptote/jh.asy";
// picture size
import stats;
include graph;

real height = 4cm;  // height of each picture

void boxplot(picture p, real[] fivenumbers, real boxheight=5pt, real whiskerbarwidth=3pt, real barwidth=4pt, pen bppen=defaultpen) {
  path whiskerbar=(0,-0.5*whiskerbarwidth)--(0,0.5*whiskerbarwidth);
  path bar=(0,-0.5*barwidth)--(0,0.5*barwidth);
  pair min_pt=(fivenumbers[0],boxheight);
  pair q1_pt=(fivenumbers[1],boxheight);
  pair med_pt=(fivenumbers[2],boxheight);
  pair q3_pt=(fivenumbers[3],boxheight);
  pair max_pt=(fivenumbers[4],boxheight);
  draw(p,shift(min_pt)*whiskerbar); 
  draw(p,shift(q1_pt)*bar); 
  draw(p,shift(med_pt)*bar); 
  draw(p,shift(q3_pt)*bar); 
  draw(p,shift(max_pt)*whiskerbar); 
  // make the connections
  draw(p,min_pt--q1_pt,bppen); // whiskers
  draw(p,q3_pt--max_pt,bppen);
  draw(p,shift(0,-0.5*barwidth)*(q1_pt--q3_pt),bppen);
  draw(p,shift(0,0.5*barwidth)*(q1_pt--q3_pt),bppen);
}


// ============== 
int picnum = 0;
picture p;
size(p,5cm,0);

real[] fivenumbers={2,4,5,8,15};
boxplot(p,fivenumbers,
	boxheight=2pt, whiskerbarwidth=1pt,barwidth=1.5pt);
xaxis(p,"number pushups",YZero,
      xmin=0,xmax=20,
      RightTicks(Step=5,step=1),above=true); 
shipout(format("boxplot%02d",picnum),p,format="pdf");



// ============== 
int picnum = 1;
picture p;
size(p,3cm,0);

real[] fivenumbers={1,2,3,4,5};
boxplot(p,fivenumbers,
	boxheight=0.6pt, whiskerbarwidth=0.4pt,barwidth=0.6pt);
label(p,"2",(1,0));
label(p,"4",(2,0));
label(p,"5",(3,0));
label(p,"8",(4,0));
label(p,"15",(5,0));
// xaxis(p,"number pushups",YZero,
//       xmin=0,xmax=20,
//       RightTicks(Step=5,step=1),above=true); 
shipout(format("boxplot%02d",picnum),p,format="pdf");



// ============== 
int picnum = 2;
picture p;
size(p,5cm,0);

real[] fivenumbers={2,4,5,8,15};
// boxplot(p,fivenumbers,
// 	boxheight=2pt, whiskerbarwidth=1pt,barwidth=1.5pt);
xaxis(p,"number pushups",YZero,
      xmin=0,xmax=20,
      RightTicks(Step=5,step=1),above=true); 
shipout(format("boxplot%02d",picnum),p,format="pdf");


// ============== 
int picnum = 3;
picture p;
size(p,5cm,0);

real[] fivenumbers_m={4,5,7,12,15};
real[] fivenumbers_f={2,3,4,5,7};
boxplot(p,fivenumbers_f,
	boxheight=4pt, whiskerbarwidth=1pt,barwidth=1.5pt);
boxplot(p,fivenumbers_m,
	boxheight=2pt, whiskerbarwidth=1pt,barwidth=1.5pt);
xaxis(p,"number pushups",YZero,
      xmin=0,xmax=20,
      RightTicks(Step=5,step=1),above=true); 
yaxis(p,"",XZero,
      ymin=0,ymax=4.5);
ytick(p,"F",(0,4));
ytick(p,"M",(0,2));
shipout(format("boxplot%02d",picnum),p,format="pdf");

