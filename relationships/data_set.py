#! /usr/bin/env python
# -*- encoding: utf-8 -*-
__version__="0.9.0"
__author__='Jim Hefferon ftpmaint at tug.ctan.org'
__date__='2010-Aug-03'
__notes__="""
"""
import os, os.path, sys, re
import optparse
import random

FN='data_set.data'
VERBOSE=False

def generate(mean,std_dev):
    return random.gauss(mean,std_dev)

def open_file(fn=FN):
    f=open(fn,'w')
    f.write("%s %s\n" % ('x','y'))
    return f

def do(std_dev,fn=FN,verbose=False):
    f=open_file(fn=fn)
    for x in range(1,100):
        f.write("%0.2f  %0.2f\n" % (x,generate(x,std_dev)))
    f.close()

#......................................................................
def main(argv=None):
    """The main logic if called from the command line
      argv=None  The arguments to the routine"""
    # Set the defaults
    if argv is None:
        argv=sys.argv
    # Parse the arguments
    #   First, define all options
    usage="""Generate x-y data with various std devs.
  %prog  [options]"""
    oP=optparse.OptionParser(usage=usage,version=__version__)
    oP.add_option('--filename','-f',action='store',default=FN,dest='fn',help='Name of file for output (default %s)' % (repr(FN),))
    oP.add_option('--std_dev','-s',action='store',default=1.0,type='float',dest='std_dev',help='Standard deviation away from the line y=x (default %d)' % (1.0,))
    oP.add_option('--VERBOSE','-V',action='store_true',default=VERBOSE,dest='verbose',help='talk a lot (default %s)' % (VERBOSE,))
    opts, args=oP.parse_args(argv[1:])
    # Establish the infrastructure
    # Handle positional arguments
    # Handle the options
    # Go
    do(opts.std_dev,fn=opts.fn,verbose=opts.verbose)
    rc=0
    # Done
    return rc

#------------------------------------------------------------------
if __name__=='__main__':
    if __notes__.strip():
       print "Notes for "+__file__+":\n"+__notes__
    try:
        rc=main(argv=sys.argv)
    except KeyboardInterrupt:
        mesg=sys.argv[0]+u": Keyboard interrupt"
        print mesg
        sys.exit(1)
    sys.exit(rc)
