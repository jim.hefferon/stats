\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
% \usepackage{present,jh}
\usepackage{../present}
\usepackage{../stats}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=red} 

% \usepackage[english]{babel}
% or whatever

% \usepackage[latin1]{inputenc}
% or whatever


\title[Controversy] % (optional, use only with long paper titles)
{Controversy}
\subtitle{}
\author{J Hef{}feron}
\institute{
  Mathematics\\
  Saint Michael's College\\
  Colchester, VT\\[1ex]
  \texttt{jhefferon@smcvt.edu.edu}
}
\date{}

\subject{Controversy}
% This is only inserted into the PDF information catalog. Can be left
% out. 


%\setlength{\parskip}{1ex}



\begin{document}
\begin{frame}
  \titlepage
\end{frame}

\section{$P$ values are easy to get wrong}
\begin{frame}{What $P$ values are}
% Recall what a $P$-value is.
\begin{center}
  \framebox{\begin{minipage}{0.8\textwidth}\color{red}
            The $P$~value is the probability, 
            assuming that the null hypothesis is true,
            of obtaining a sample as extreme or more extreme than the
            sample that was observed.
            \end{minipage}}
\end{center}

Suppose that you give a drug to a hundred patients and 
find that their colds are on average a day shorter.  
The $P$~value is the chance that, 
if the medication didn't do anything at all, 
your sample's patients would have averaged day-shorter colds 
just by happenstance.

\pause
A $P$~value does not measure whether the question at issue is true,
how right you are, 
or how significant is the difference made by the treatment.
It instead measures how surprised you should be if there is no actual difference 
between the groups but your sample data suggests there is.

It's not easy to translate this into an answer to, ``Is there 
a real difference?'' 
In practice, 
if $P <0.05$ then most researchers call 
the difference ``significant.'' 
But ``significant'' is a technical term and does not mean that the treatment
makes any practical difference.
\end{frame}


\begin{frame}{What you what you want to know vs what it tells you}
Imagine you are testing a new cancer drug.
You will test to see if, on average, people live longer than five years.
\begin{center}
  \begin{tabular}{l}
    $H_0$: $\mu=5$  \\
    $H_a$: $\mu>5$
  \end{tabular}
\end{center}

\begin{itemize}
\item
What you want to know is: given the data, what is the probability that the 
null hypothesis is true?
\item
What the $P$~value tells you is: given that the null hypothesis is true, 
what is the
probability of this (or more extreme) data?
\end{itemize}
They are not the same.
\end{frame}


\begin{frame}{Effect size and sample size}
Usually the goal is to get a small $P$~value.
You can get the same $P$ value either by measuring a huge effect
or by measuring a tiny effect with great certainty.

Suppose that you are testing a medicine to lower blood pressure.
Imagine that this medicine actually lowers pressure by three points
(so if you gave it to every person in the world thent the
worldwide average would be three points lower).
If you test it on thousands of 
people, you may succeed in getting the small $P$~value that you desire.
But typical healthy blood pressure is about $100$ and lowering it
by three points will not help patients.
Statistical significance does not mean your result has any practical 
significance.

\pause
Similarly, statistical insignificance is hard to interpret. 
Even if your medicine actually lowers blood pressure by $20$ points, 
if you only test it on a sample of ten people then 
you would be hard-pressed to tell the difference between a real improvement 
in the patients and good luck. 
A statistically insignificant difference does not mean there is no 
practical difference.
\end{frame}


\begin{frame}{Confusions about $P$ values}
Recall:
            the $P$~value is the probability, 
            assuming that the null hypothesis is true,
            of obtaining a sample as extreme or more extreme than the
            sample that was observed.

All the statements below are mistakes that people, 
including experienced and often-published professionals,
make about $P$-values.
{\color{red}These are all wrong.}
\begin{itemize}
\item \alert{Inverse probability error.}
  The $P$-value is the probability that the null hypothesis is true, 
  given the data.

\pause\item If the null hypothesis is rejected, the $P$~value is the probability 
  that this decision is wrong (i.e., 
  the chance that the decision to reject is a Type~I error).

\pause\item The complement of the $P$-value, $1-P$, is the probability that 
  $H_a$ is right.

\pause\item \alert{Repeatability error.}
  The complement of the $P$-value, $1-P$, is the probability
  that the result will replicate.
  (A variant is that the complement is the chance that a replication
  of the experiment will yield a statistically significant result.)
\end{itemize}
\end{frame}



\begin{frame}{Math was never my best subject}
Seventy academic psycologists were surveyed about
their go-to interpretation of ``$P<0.01$.''
(People were allowed to answer more than one; 
other surveys give similar results.)
These are people who do this for a living.
\vspace*{-2ex}
\begin{center} \small
  \begin{tabular}{l|r@{$.$}l}
    \multicolumn{1}{c}{\textit{Statement}}
      &\multicolumn{2}{c}{\textit{Percent}}           \\ 
    \hline
    1. The null hypothesis is absolutely disproved.           &$1$ &$4$ \\
    2. The probability of the null hypothesis has been found. &$45$ &$7$ \\
    3. The experimental hypothesis is absolutely proved.      &$2$ &$9$ \\
    4. The probability of the experimental hypothesis can be deduced. &$42$ &$9$ \\
    5. The probability that the decision taken is wrong is known. &$68$ &$6$ \\
    6. A replication has a $0.99$ probability of being significant. &$34$ &$3$ \\
    7. The probability of the data given the null hypothesis is known. &$11$ &$3$ \\
  \end{tabular}
\end{center}
Only number~$7$ is right.
\end{frame}




\begin{frame}{Fallacy of Proof of the Null Hypothesis}
In a test the null hypothesis $H_0$ is considered the best guess 
until there is enough data to reject it.  
We can compare this to a trial, where the accused is considered innocent 
($H_0$) until proven guilty ($H_a$) beyond reasonable doubt ($\alpha$).

But if data does not give us enough proof to reject $H_0$, 
this does not mean that $H_0$ is correct. 
We can compare this with freeing a defendant who is guilty
because there was not enough proof to convict.
Lack of proof does not prove lack of guilt.

% \medskip
% \begin{quotation}\small
% \noindent "\ldots{} the null hypothesis is never proved or established, 
% but it is possibly disproved, in the course of experimentation. 
% Every experiment may be said to exist only in order to give the facts 
% a chance of disproving the null hypothesis." 
% \ \textrm{-- RA~Fisher} 
% \end{quotation}
\end{frame}




\section{What hypothesis testing gets right}
\begin{frame}{Counter-argument: what the method of hypothesis testing gets right}
Hypothesis testing is the current standard.
Professionals have used it for sound reasons.

\begin{itemize}
\item \alert{It addresses sampling error.}
We have talked about the medical researcher who is afraid that,
just because of the happenstance of a way-off sample, 
they will recommend a treatement option that is in fact worse, 
and people will die.

\pause\item \alert{Misinterpretations are not the fault of the method.}

\pause\item \alert{Some research questions require a dichotomy.}
Doctors need to prescribe a drug, and want to know: is this
drug better than a placebo?

\pause\item \alert{Sometimes null hypotheses are appropriate.}
One example is quality control.
\end{itemize}
\end{frame}



\begin{frame}{More points: hypothesis tests overly automate data reasoning}
\begin{itemize}
\item They encourage dichotomous (two-choice) thinking.
  They either reject, or fail to reject, $H_0$.
  A $P$-value of $0.05$ is not all that different than one of 
  $0.06$ but the first study will be accepted by a journal
  and appear in the literature
  while the second may well not.

\pause
\item They result in an emphasis on statistical significance 
to the exclusion of estimation and confirmation by repeated experiments.
They divert attention away from the data and the measurement process.
  One place they do this is in Elementary Statistics classes.

\pause
\item They may aid studies that clutter the literature but have no
  scientific value.
  Their objective appearance and mechanical application may lend an air
  of credibility to studies that have otherwise weak conceptual foundations.
  \pause
  ESP is crap.
\end{itemize}
\end{frame}



\section{Publication bias}

\begin{frame}{Only publishing significant results biases against the null hypothesis}
  Say you study flipping coins.
  The null hypothesis is that the coin is half and half.
  A standard experiement in this field is to flip $5$ times and report the
  number of heads.
  This is the probabilities.
  \begin{center}  \small\renewcommand{\arraystretch}{1.5}
    \begin{tabular}{p{.55\linewidth}lc}
      \multicolumn{1}{l}{\textit{Cases (out of $32$)}}
        &\multicolumn{1}{c}{\textit{Report}}
        &\multicolumn{1}{c}{\textit{Chance}}     \\  \hline
      $TTTTT$  &$0$ heads  &$1/32=0.03125$      \\
      $HTTTT$, 
      $THTTT$,
      $TTHTT$,
      $TTTHT$,
      $TTTTH$
        &$1$ head  &$5/32 = 0.15625$     \\
    $HHTTT$,
    $HTHTT$,
    $HTTHT$,
    $HTTTH$,
    $THHTT$, 
    $THTHT$,
    $THTTH$,
    $TTHHT$,
    $TTHTH$,
    $TTTHH$
      &$2$ heads  &$10/32 = 0.3125$  \\
    $HHHTT$,
    $HHTHT$,
    $HHTTH$,
    $HTHHT$,
    $HTHTH$,
    $HTTHH$,
    $THHHT$,
    $THHTH$,
    $THTHH$,
    $TTHHH$,
        &$3$ heads &$10/32 = 0.3125$  \\
    $HHHHT$,
    $HHHTH$,
    $HHTHH$,
    $HTHHH$,
    $THHHH$,
       &$4$ heads  &$5/32 = 0.15625$   \\
    $HHHHH$ &$5$ heads  &$1/32 = 0.03125$
    \end{tabular}
  \end{center}
\end{frame}

\begin{frame}
  Say that the major journals of coin flipping only publish results with
  the significance $P<0.05$.
  Alice flips a quarter $5$ times and gets $3$ heads and $2$ tails,
  and nobody will publish her study because it has $P=.3125$.
  Bob flips a quarter $5$ times and gets $2$ heads and $3$ tails and as with
  Alice, no journal will publish his results.
  Catherine, David, Ellen, Frank, and Geri all perform the same experiment,
  most reporting $2$~heads or $3$~heads,
  but with some $4$'s and $1$'s.
  The journal editors tirelessly send them all rejection letters.

  \pause
  Now, Robert flips a coin $5$ times and gets $5$ heads.
  This result has $P=.03125$, which meets the requirement of $P<.05$.
  He sends it to the American Journal of Coin Flipping Studies
  and they are very excited to publish his results.
  General audience nature and science magazines do front page pieces
  with headlines ``Quarters Found Heavy-headed'' and
  ``Washington Shows His Face'' respectively.
  His quarter-flipping study is cited in the abstracts of two
  dime-flipping studies and a half-dollar study and he notes that in his
  grant applications.

  \pause
  Later, Sally flips a quarter $5$ times and gets $2$ heads and $3$
  tails.
  She sends her results to the AJCFS noting a failure to
  reproduce Robert's result, but it is rejected because it has
  $P=.3125$.
\end{frame}

\begin{frame}
  If you are a coin-flipping researcher and you do a literature search,
  coin flipping then you'd conclude that quarters are significantly weighted
  towards heads.
  But in fact the null hypothesis is true:~quarters are evenly weighted.
  Robert's low-$P$ result is just what happens eventually
  if you have enough people perform the $5$-flip experiment---if a lot of
  people are studying coin flips, the probability is just about a
  hundred percent that you will see low-$P$ results.
  The problem is that the AJCFS requires $P=.05$,
  so they've created a bias against
  the null hypothesis appearing in the literature,
  People are deceived into thinking that
  flipped quarters are more likely to come up heads than tails.

  This is one reason why so many fields, most notably psychology and medicine,
  are having a replication crisis.
  % Asimilar effect can be used in
  % $P$-hacking to bolster results that are essentially fake.
  % 
  Unlike coin-flipping, these fields have real
  effects on the lives of real people.
  It's irresponsible and unethical for journals to have a bias against
  the null hypothesis.
  Note that adjusting the $P$-value requirements to
  another significance requirement, even a less arbitrary one, doesn't fix
  the issue.

  % \pause
  % One thing that would help is for journals to commit to publish studies
  % before the study has been performed, based on the methodology, previous
  % studies on the subject, and qualifications of the researcher.
  % This would mean that many, many studies would be published with null results,
  % and that would be a good thing.
\end{frame}




\begin{frame}{Publication bias}
If ten researchers do studies and find no significant effect, so their
$P$-value does not reach the $\alpha$ critereon, and one researcher does
find a significant effect, then the only paper published is the last one.

This is \alert{publication bias} (or the \alert{file drawer problem}), that
by selecting only papers with a significant $P$-value, 
negative studies are selected against.

\pause
\exm 
You have a medical condition. 
Your doctor tells you about a new procedure so
you get on the interwebs and read some literature.
You find a number of studies showing that people were cured. 
These studies met the $\alpha=0.05$ criteria.
But maybe there are lots of other studies that failed to do so;
journals won't accept those studies so they just sit in file cabinets. 

\pause\medskip
A related case is that a drug company may pay an independent testing 
lab to run an experiment.
The contract says that if they find a significant effect showing the 
drug is good, the lab can publish.
But if they do not find an effect, or if they find a bad effect, 
their contract says that the lab cannot publish.

This games the system for commercial gain.
\end{frame}


\section{$P$-hacking}
\begin{frame}
Today we can exhaustively search huge data sets (this is Big Data)
and can find combinations of variables that show a correlation,
or differences in means, etc. 

Conventional tests of statistical significance are based on the 
probability that a particular result would arise if chance alone were at work, 
and necessarily accept some risk of mistaken conclusions. 
This level of risk is the significance. 

When you perform large numbers of tests, some produce spurrious results: 
$5\%$ of randomly chosen hypotheses will be significant at 
$\alpha=0.05$. 
Test enough and you will find something that is  
statistically significant, but wrong.
\pause
\begin{center}
  \includegraphics[height=0.35\textheight]{spurious.png}
\end{center}
\end{frame}

\begin{frame}
\alert{Data dredging} (or \alert{data fishing}, or \alert{$P$-hacking}) 
is the use of data mining to uncover patterns that compute to be 
statistically significant, without first devising a specific hypothesis 
of the underlying truth.

Hypothesis testing was invented with the model that you 
first formulate a research hypothesis, typically based on prior work. 
Only then do you collect relevant data, 
and finish by carrying out a statistical significance test to see how 
likely were such results if chance alone were at work.
\end{frame}
\begin{frame}
\centering
\includegraphics[height=\textheight]{xkcd-significant.png}  
\end{frame}

\begin{frame}{Use different data}
In particular, if you are sifting through data to form hypotheses you 
\emph{must} test each one with data other than that used to 
construct it. 
\begin{itemize}
\item
Suppose 
you give headache suffers a drug and they report getting better $15$ times
out of $20$.
You might hypothesize that the probabilty of a headache is $0.75$.
Fair enough, but to test your hypothesis you must gather a new data set.

\pause \item
This explains how $H_a$'s can use~$\neq$.
If a homework problem says, \textit{you are a quality control inspector
and you test $H_0:\;\mu=12$ against $H_a:\;\mu\neq 12$, with
$\bar{x}=11.94$ and $\sigma=0.10$}
students will ask, ``since $\bar{x}$ is smaller, why are we not 
testing $H_a:\;\mu<12$?''
The answer is that we must state $H_a$ before we gather the
data, so before we know $\bar{x}$.
\end{itemize}
\pause
Looking for patterns in data is legitimate. 
Applying a statistical test of significance to the same data where 
the pattern was found is not. 

There are strategies for this,
even in data mining.
For instance, the
researcher can collects data and randomly split it into two subsets. 
They use the first for creating hypotheses, which they
test on the second. 
\end{frame}



\section{The replication crisis}
\begin{frame}{The crisis}
The \alert{replication crisis} (or \alert{replicability crisis}) 
is that many 
scientific experiments are difficult or impossible to replicate, 
either by independent researchers or by the original researchers themselves. 
Reproducibility of experiments is an essential part of the 
scientific method so the inability to replicate studies has 
potentially grave consequences for many fields.

John Ioannidis, 
Professor of Medicine and of Health Research and Policy at Stanford 
University School of Medicine and a Professor of Statistics at 
Stanford, 
published a stunning paper in 
2005 called \textit{Why Most Published Research Findings Are False}
that brought this issue to general attention.  

It has been particularly widely discussed in the 
psychology (in particular, in social psychology) and in medicine.

Statistics and statistical methods are certainly involved, and
reforming them will certainly be part of any solution.
\end{frame}


\begin{frame}{Some numbers}
According to a 2016 poll reported in 
\textit{Nature}, of 1,500 scientists, $70\%$ have been unable to reproduce 
another scientist's experiments 
($50\%$ have been unable to reproduce their own experiment). 
These numbers differ among disciplines, but are discouragingly large in 
each:
    chemistry $90\%$ ($60\%$),
    biology $80\%$ ($60\%$),
    physics and engineering $70\%$ ($50\%$),
    medicine $70\%$ ($60\%$),
    Earth and environment science $60\%$ ($40\%$).
% In 2009, $2\%$ of scientists admitted to falsifying studies at least once 
% and $14\%$ admitted to personally knowing someone who did. 
% Misconducts were reported more frequently by medical researchers than others. 

\pause
If we look at the medical studies from 1990-2003 with more than 
a thousand citations, there are $49$ of them. 
The great majority, $45$, claimed that studied therapy was effective. 
Out of these, $16\%$ were contradicted by subsequent studies, 
$16\%$ inflated effectiveness of therapy, 
and $24\%$ were not replicated (some went in more than one category). 

\pause
The Food and Drug Administration in 1977-90 found flaws in 
$10$-$20\%$ of medical studies. 

\pause
In a paper published in 2012, Glenn Begley and Lee Ellis
argued that only $11\%$ of the pre-clinical cancer studies could be replicated.
\end{frame}


\begin{frame}{Replication take-aways}
Begley and Ioannidis cite these points.
\begin{itemize}
\item 
    One major cause is the generation of new data and publications 
    at an unprecedented rate.
\item
    There is compelling evidence that the majority of these discoveries will 
    not stand the test of time.
\item
    Key among the causes: failure to adhere to good scientific practice, 
    and the desperation to publish or perish.
    People respond to rewards and researchers are rewarded for
    getting grants and for publishing papers. 
\item
    This is a multifaceted, multi-stakeholder problem.
    No single party is solely responsible and no single solution will suffice.
\end{itemize}
\end{frame}


\begin{frame}{Start of reforms}

In a 2016 article, Ioannidis 
covered \textit{Why Most Clinical Research Is Not Useful}.
He laid out some the problems and called for reform, 
characterizing certain points for medical research to be useful again.
One point was the need for medicine to be ``Patient Centered'' 
instead of the current practice that mainly takes care of 
``the needs of physicians, investigators, or sponsors.'' 

For example, he talks about the value of negative findings, 
ones that fail to meet the $\alpha=0.05$ criteria.
There needs to be a way for them to get into the public record,
out of file cabinets.
\end{frame}


\begin{frame}{Some proposed steps}
\begin{itemize}
\item Tackling publication bias with pre-registration of studies
\item Emphasizing replication attempts in teaching
\item Encouraging use of larger sample sizes
\item Sharing raw data in online repositories
\item Funding for replication studies
\end{itemize}  
Some of these fall under the name \textit{Open Science}.
\end{frame}



\section{The role of Statistics}

\begin{frame}{Role of the field of Statistics in all this}
The field is comparatively young ($P$~values date from the 1920's
whereas high school science dates from the 1600's and 1700's).
It is dynamic, under development, responding to what is happening today, 
including Big Data.

This course gives a solid foundation.
What we have seen is all current best practice.
However, there is reason to suppose that best practice may continue 
to evolve.

% \href{Veritasium video}{https://www.youtube.com/watch?v=42QuXLucH3Q&t=479s}
\end{frame}




\section{More reading}
\begin{frame}{Additional readings}
  \begin{enumerate}
  \item \href{http://ist-socrates.berkeley.edu/~maccoun/PP279_Cohen1.pdf}{Cohen, \textit{The Earth is Round ($p<.05$)}}
  \item \href{https://www.statisticsdonewrong.com/}{Reinhart, \textit{Statistics Done Wrong}}
  \item \href{https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1182327/}{Ioannidis, \textit{Why Most Published Research Findings Are False}}
  \end{enumerate}
\end{frame}
\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 




